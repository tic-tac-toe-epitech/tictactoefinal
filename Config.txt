// Full path to the Unreal engine source directory
UE4_ROOT=""
// Full path to the TicTacToe.uproject file
GAME_UPROJECT=""
// Full path to the server package destination
ARCHIVED_SERVER_DIR=""
// Full path to the client package destination
ARCHIVED_CLIENT_DIR=""
// Package mode can be Development or Production
PACKAGE_CONFIG="Development"
