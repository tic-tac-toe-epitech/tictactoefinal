#include "CoreMinimal.h"
#include "UnitTest.h"
#include "TicTacToe/Tests/Unit/CustomUnitTest.h"
#include "TicTacToe/GameData/Configuration/ConfigurationLoader.h"

IMPLEMENT_CUSTOM_SIMPLE_AUTOMATION_TEST(GetMapConfig, FCustomUnitTest, "UnitTests.UConfigurationLoaderTest.GetMapConfig", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool GetMapConfig::RunTest(const FString& Parameters)
{
	UConfigurationLoader	*configurationLoader = NewObject<UConfigurationLoader>();
	FMapStruct				map;

	MessageInfo(TEXT("Checking all default values for map configuration."));
	map = configurationLoader->GetMapConfig();
	return (map._Id == "Map" && map._DefaultMaxPopulationByPlayer == 20 && map._VictoryTerms._BAllowDiagonals == 1 &&
			map._VictoryTerms._BAllowHorizontal == 1 && map._VictoryTerms._BAllowVertical == 1 && map._VictoryTerms._LineGoal == 3);
}