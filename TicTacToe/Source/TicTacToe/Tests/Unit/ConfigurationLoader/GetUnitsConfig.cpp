#include "CoreMinimal.h"
#include "UnitTest.h"
#include "TicTacToe/Tests/Unit/CustomUnitTest.h"
#include "TicTacToe/GameData/Configuration/ConfigurationLoader.h"

IMPLEMENT_CUSTOM_SIMPLE_AUTOMATION_TEST(GetUnitsConfig, FCustomUnitTest, "UnitTests.UConfigurationLoaderTest.GetUnitsConfig", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool GetUnitsConfig::RunTest(const FString& Parameters)
{
	UConfigurationLoader	*configurationLoader = NewObject<UConfigurationLoader>();
	FUnitsStruct			units;

	MessageInfo(TEXT("Checking fews default values for units configuration."));
	units = configurationLoader->GetUnitsConfig();
	return (units._GlobalId == "Units"
		&& FUnitsStruct::GetUnitData(units, "FactionEntity.Unit.Artillery")._Damage._Value == 30
		&& FUnitsStruct::GetUnitData(units, "FactionEntity.Unit.Cyborg")._Hp == 200
		&& FUnitsStruct::GetUnitData(units, "FactionEntity.Unit.Scout")._Name == "Scout");
}