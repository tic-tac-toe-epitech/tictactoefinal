#include "CoreMinimal.h"
#include "UnitTest.h"
#include "TicTacToe/Tests/Unit/CustomUnitTest.h"
#include "TicTacToe/GameData/Configuration/ConfigurationLoader.h"

IMPLEMENT_CUSTOM_SIMPLE_AUTOMATION_TEST(GetResourcesConfig, FCustomUnitTest, "UnitTests.UConfigurationLoaderTest.GetResourcesConfig", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool GetResourcesConfig::RunTest(const FString& Parameters)
{
	UConfigurationLoader	*configurationLoader = NewObject<UConfigurationLoader>();
	FResourcesStruct		resources;

	MessageInfo(TEXT("Checking all default values for resources configuration."));
	resources = configurationLoader->GetResourcesConfig();
	return (resources._Id == "Resources" && resources._Steel._Id == "Resources.Steel" && resources._Steel._Name == "Steel" &&
			resources._Gold._Id == "Resources.Gold" && resources._Gold._Name == "Gold" &&
			resources._Wood._Id == "Resources.Wood" && resources._Wood._Name == "Wood");
}