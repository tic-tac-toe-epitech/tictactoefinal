// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalTest.h"
#include "EngineGlobals.h"
#include "Logging/MessageLog.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "CustomFunctionalTest.generated.h"

/**
 *
 */
UCLASS()
class TICTACTOE_API ACustomFunctionalTest : public AFunctionalTest
{
	GENERATED_BODY()

private:
	FMessageLog logger = FMessageLog("AutomationTestingLog");

public:
	UFUNCTION(BlueprintCallable, Category = "Functional Testing Message Log")
	void MessageInfo(FString Message, bool onScreen = true);

	UFUNCTION(BlueprintCallable, Category = "Functional Testing Message Log")
	void MessageWarning(FString Message, bool onScreen = true);

	UFUNCTION(BlueprintCallable, Category = "Functional Testing Message Log")
	void MessageError(FString Message, bool onScreen = true);
};
