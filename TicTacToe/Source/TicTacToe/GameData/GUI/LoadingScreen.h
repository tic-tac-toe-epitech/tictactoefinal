// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TextBlock.h"
#include "Blueprint/UserWidget.h"
#include "LoadingScreen.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API ULoadingScreen : public UUserWidget
{
	GENERATED_BODY()
	

public:
	UFUNCTION()
	void SetLoadingText(FString text);

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* transition_text;

};
