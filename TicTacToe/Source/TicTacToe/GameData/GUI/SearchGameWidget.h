// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Styling/SlateColor.h"
#include "Components/Image.h"
#include "Math/Color.h"
#include "SearchGameWidget.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API USearchGameWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	enum Status {
		PROCESSING = 0,
		SUCCEED = 1,
		FAILED = 3
	};

	virtual void NativeConstruct() override;

	void SetStatus(Status status) {
		if (status == PROCESSING) {
			const FLinearColor color(1.0, 0.6444799, 0, 1.0);
			this->status_icon->SetBrushTintColor(FSlateColor(color));
		}
		else if (status == SUCCEED) {
			const FLinearColor color(0, 1.0, 0.2303133, 1.0);
			this->status_icon->SetBrushTintColor(FSlateColor(color));
		}
		else if (status == FAILED) {
			const FLinearColor color(1.0, 0.0363607, 0, 1.0);
			this->status_icon->SetBrushTintColor(FSlateColor(color));
		}
	}

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UButton* Button_Play;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UImage* status_icon;

};
