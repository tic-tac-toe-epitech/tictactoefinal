// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GameFramework/PlayerController.h"
#include "GameplayTags.h"
#include "InGame/AI/OrderManager/OrderManagerInclude.h"
#include "InGame/Building/GhostBuilding.h"
#include "UObject/SoftObjectPtr.h"
#include "InGamePlayerController.generated.h"


class ISelectableInterface;
class AUnit;
class ABuilding;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_InGamePlayerController_GameplayTag, AInGamePlayerController*, Controller, FGameplayTag, BuildingType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_InGamePlayerController_ActorArray, AInGamePlayerController*, Controller, const TArray<AActor*>&, Actors);

/**
 * 
 */
UCLASS()
class TICTACTOE_API AInGamePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorChannelFailure(UActorChannel* ActorChan) override;

	// Called on client when the server notifies the player the match has started.
	UFUNCTION(BlueprintNativeEvent)
		void HandletMatchIsWaitingToStart();
	void HandletMatchIsWaitingToStart_Implementation();

	// Called on client when the server notifies the player the match has started.
	UFUNCTION(BlueprintNativeEvent)
		void HandleStartMatch();
	void HandleStartMatch_Implementation();

	UFUNCTION(BlueprintNativeEvent)
		void HandleMatchHasEnded();
	void HandleMatchHasEnded_Implementation();

protected:
	// Always executed AFTER the tick function.
	UFUNCTION(BlueprintNativeEvent)
		void ServerTick(float DeltaTime);
	virtual void ServerTick_Implementation(float DeltaTime);

	// Always executed AFTER the tick function.
	UFUNCTION(BlueprintNativeEvent)
		void ClientTick(float DeltaTime);
	virtual void ClientTick_Implementation(float DeltaTime);

protected:
	UPROPERTY(BlueprintReadWrite)
	AActor* ActorUnderCursor;

	UPROPERTY(BlueprintReadWrite)
	TArray<AActor*> SelectedActors;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Construction")
	UDataTable*		BuildingsTable;

	// Construction mode
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Construction")
		bool isBuildMode = false;
	// Current Ghost Building
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Construction")
		AGhostBuilding* GhostBuilding;

	UFUNCTION(BlueprintCallable)
	void TryToGiveOrderToActors(const TArray<AActor*>& Actors, FOrderStruct Order);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void StartBuildingConstruction(FGameplayTag BuildingType, FTransform Transform, const TArray<AActor*>& InSelectedActors);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool QueueOrder;



private:
	UFUNCTION(Server, Reliable, WithValidation)
	void GiveOrderToActorsRPCS(const TArray<AActor*>& Actors, FOrderStruct Order);
	void GiveOrderToActorsRPCS_Implementation(const TArray<AActor*>& Actors, FOrderStruct Order);
	bool GiveOrderToActorsRPCS_Validate(const TArray<AActor*>& Actors, FOrderStruct Order) { return true; }

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
	void OrderBuildingConstruction(FGameplayTag BuildingType, FTransform Transform, const TArray<AActor*>& InSelectedActors);
	void OrderBuildingConstruction_Implementation(FGameplayTag BuildingType, FTransform Transform, const TArray<AActor*>& InSelectedActors);
	bool OrderBuildingConstruction_Validate(FGameplayTag BuildingType, FTransform Transform, const TArray<AActor*>& InSelectedActors) { return true; }

public:
	UFUNCTION(BlueprintCallable)
		void SelectActors(TArray<AActor*> ActorsToSelect, bool bClearCurrentSelection);

	UFUNCTION(BlueprintCallable)
		void UnselectActors(TArray<AActor*> ActorsToUnselect);

	UFUNCTION(BlueprintCallable)
		void ClearSelection();

	UFUNCTION(BlueprintPure)
		AActor* GetActorUnderCursor() { return ActorUnderCursor; }

	UFUNCTION(BlueprintPure)
		TArray<AActor*> GetSelectedActors() { return SelectedActors; }

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
	void DestroySelectedActorsRCPS(const TArray<AActor*>& actors);
	void DestroySelectedActorsRCPS_Implementation(const TArray<AActor*>& actors);
	bool DestroySelectedActorsRCPS_Validate(const TArray<AActor*>& actors) { return true; }

	UFUNCTION(BlueprintPure)
		TArray<AUnit*> GetSelectedUnits();

	UFUNCTION(BlueprintPure)
		TArray<ABuilding*> GetSelectedBuildings();

	UFUNCTION(BlueprintPure)
		TArray<AUnit*> GetSelectedUnitsOfType(FGameplayTag Type);

	UFUNCTION(BlueprintPure)
		TArray<ABuilding*> GetSelectedBuildingsOfType(FGameplayTag Type);

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerController_ActorArray OnSelectionChanged;

	UFUNCTION(BlueprintCallable)
		void MakeOrderStructForActorUnderCursor();

	UFUNCTION(BlueprintCallable)
		void ActivateConstructionMode(const FGameplayTag& BuildingTag);

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerController_GameplayTag OnStartBuild;

	UFUNCTION()
		bool GetOrderFromActorUnderCursor(FOrderStruct & OS);

	UFUNCTION(BlueprintCallable)
		void ForceDisconnect();
};

