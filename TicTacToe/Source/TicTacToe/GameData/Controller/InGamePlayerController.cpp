// Fill out your copyright notice in the Description page of Project Settings.


#include "InGamePlayerController.h"
#include "InGame/Selectable/SelectableInterface.h"
#include "InGame/Unit/Unit.h"
#include "InGame/Building/Building.h"
#include "Engine/ActorChannel.h"
#include "Libraries/DiplomacyLibrary.h"
#include "InGame/AI/OrderManager/OrderManagerComponent.h"
#include "TicTacToe.h"
#include "Engine/World.h"
#include "InGame/AI/Formation/Formation.h"
#include "GameData/GameMode/InGameGameMode.h"
#include "InGame/Building/BuildingTableRow.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "InGame/Resources/ResourcePoint.h"

// TICK
void AInGamePlayerController::HandletMatchIsWaitingToStart_Implementation()
{
	UE_LOG(LogController, Log,
		TEXT("AInGamePlayerController::HandletMatchIsWaitingToStart_Implementation."));
}

void AInGamePlayerController::HandleStartMatch_Implementation()
{
	UE_LOG(LogController, Log,
		TEXT("AInGamePlayerController::HandleStartMatch_Implementation."));
}

void AInGamePlayerController::HandleMatchHasEnded_Implementation()
{
	UE_LOG(LogController, Log,
		TEXT("AInGamePlayerController::HandleMatchHasEnded_Implementation."));
}

void AInGamePlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		ServerTick(DeltaTime);
	}
	else
	{
		ClientTick(DeltaTime);
	}
}

void AInGamePlayerController::NotifyActorChannelFailure(UActorChannel* ActorChan)
{
	UE_LOG(LogController,
		Error,
		TEXT("Actor channel failure actor : %s."),
		*GetNameSafe(ActorChan->GetActor()));
}

void AInGamePlayerController::ServerTick_Implementation(float DeltaTime)
{

}

void AInGamePlayerController::ClientTick_Implementation(float DeltaTime)
{

}

// END TICK


void AInGamePlayerController::SelectActors(TArray<AActor*> ActorsToSelect, bool bClearCurrentSelection)
{
	if (bClearCurrentSelection)
	{
		ClearSelection();
	}
	for (auto actor : ActorsToSelect)
	{
		ISelectableInterface* asSelectable = Cast<ISelectableInterface>(actor);
		if (asSelectable)
		{
			asSelectable->Execute_Select(actor);
		}
		SelectedActors.AddUnique(actor);
	}
	OnSelectionChanged.Broadcast(this, SelectedActors);
}

void AInGamePlayerController::UnselectActors(TArray<AActor*> ActorsToUnselect)
{
	for (auto actor : ActorsToUnselect)
	{
		ISelectableInterface* asSelectable = Cast<ISelectableInterface>(actor);
		if (asSelectable)
		{
			asSelectable->Execute_Unselect(actor);
		}
		SelectedActors.Remove(actor);
	}
	OnSelectionChanged.Broadcast(this, SelectedActors);
}

void AInGamePlayerController::ClearSelection()
{
	UnselectActors(SelectedActors);
}

void AInGamePlayerController::TryToGiveOrderToActors(const TArray<AActor*>& Actors, FOrderStruct Order)
{
	if (!Actors.Num())
	{
		return;
	}
	for (auto actor : Actors)
	{
		if (actor && UDiplomacyLibrary::GetDiplomacyWithPlayerController(actor, this) != EDiplomacy::OWNED)
		{
			// We don't do anything if one of the given actors isn't possessed by the player.
			// Player controllers shall only give orders to owned entities.
			return;
		}
	}
	GiveOrderToActorsRPCS(Actors, Order);
}

void AInGamePlayerController::GiveOrderToActorsRPCS_Implementation(const TArray<AActor*>& Actors, FOrderStruct Order)
{
	if (Actors.Num())
	{
		TArray<AUnit*> unitArray;

		for (auto actor : Actors)
		{
			if (actor && UDiplomacyLibrary::GetDiplomacyWithPlayerController(actor, this) == EDiplomacy::OWNED)
			{
				if (Order.OrderType == EOrderType::Move)
				{
					UE_LOG(TicTacLogOrderManagement, Warning,
						TEXT("AInGamePlayerController::GiveOrderToActorsRPCS_Implementation: Order is of type MOVE."))
						AUnit* asUnit = Cast<AUnit>(actor);

					if (asUnit)
					{
						UE_LOG(TicTacLogOrderManagement, Log,
							TEXT("AInGamePlayerController::GiveOrderToActorsRPCS_Implementation: Actor %s is a unit."),
							*GetNameSafe(actor));
						unitArray.Add(asUnit);
					}
				}

				UOrderManagerComponent* orderManager = actor->FindComponentByClass<UOrderManagerComponent>();
				if (orderManager)
				{
					if (HasAuthority())
					{
						orderManager->AddOrder(Order);
						UE_LOG(TicTacLogOrderManagement, Log,
							TEXT("AInGamePlayerController::GiveOrderToActorsRPCS_Implementation: Order given to '%s' server-side."),
							*GetNameSafe(actor));
					}
					else
					{
						UE_LOG(TicTacLogOrderManagement, Log,
							TEXT("AInGamePlayerController::GiveOrderToActorsRPCS_Implementation: Called client-side, should not happen."));
					}
				}
			}
		}
		if (unitArray.Num())
		{
			FActorSpawnParameters params;

			params.Owner = this;
			AFormation* formation = GetWorld()->SpawnActor<AFormation>(params);

			if (formation)
			{
				formation->SetMembers(unitArray);
				formation->MoveToLocation(Order.TargetLocation);
			}
		}

	}
}

void AInGamePlayerController::DestroySelectedActorsRCPS_Implementation(const TArray<AActor*>& actors)
{
	for (auto actor : actors)
	{
		actor->Destroy();
	}
	UnselectActors(actors);
}

TArray<AUnit*> AInGamePlayerController::GetSelectedUnits()
{
	TArray<AUnit*> ret;

	for (auto actor : GetSelectedActors())
	{
		AUnit* asUnit = Cast<AUnit>(actor);
		if (asUnit)
		{
			ret.AddUnique(asUnit);
		}
	}
	return ret;
}

TArray<ABuilding*> AInGamePlayerController::GetSelectedBuildings()
{
	TArray<ABuilding*> ret;

	for (auto actor : GetSelectedActors())
	{
		ABuilding* asBuilding = Cast<ABuilding>(actor);
		if (asBuilding)
		{
			ret.AddUnique(asBuilding);
		}
	}
	return ret;
}

TArray<AUnit*> AInGamePlayerController::GetSelectedUnitsOfType(FGameplayTag Type)
{
	return AUnit::GetUnitsOfType(GetSelectedUnits(), Type);
}

TArray<ABuilding*> AInGamePlayerController::GetSelectedBuildingsOfType(FGameplayTag Type)
{
	return ABuilding::GetBuildingsOfType(GetSelectedBuildings(), Type);
}

bool AInGamePlayerController::GetOrderFromActorUnderCursor(FOrderStruct & OS)
{
	FHitResult hitresult;

	if (!this->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, hitresult))
	{
		UE_LOG(TicTacLogOrderManagement, Log,
			TEXT("Nothing under the cursor"));
		OS.OrderType = EOrderType::Move;
		return true;
	}
	OS.TargetLocation = hitresult.Location;
	OS.bIsShift = QueueOrder;
	auto resourcePtr = Cast<AResourcePoint>(hitresult.Actor);
	if (resourcePtr != __nullptr)
	{
		OS.OrderType = EOrderType::Collect;
		OS.TargetActor = resourcePtr;
		return true;
	}
	auto buildingPtr = Cast<ABuilding>(hitresult.Actor);
	if (buildingPtr != __nullptr)
	{
		if (UDiplomacyLibrary::GetDiplomacyWithPlayerController(buildingPtr, this) == EDiplomacy::ENEMY) {
			OS.OrderType = EOrderType::Attack;
			OS.TargetActor = buildingPtr;
		}
		else if (buildingPtr->GetConstructionStatus() != EConstructionStatus::Constructed) {
			OS.OrderType = EOrderType::Build;
			OS.TargetActor = buildingPtr;
		}
		return true;
	}
	auto unitPtr = Cast<AUnit>(hitresult.Actor);
	if (unitPtr != __nullptr && UDiplomacyLibrary::GetDiplomacyWithPlayerController(unitPtr, this) == EDiplomacy::ENEMY)
	{
		OS.OrderType = EOrderType::Attack;
		OS.TargetActor = unitPtr;
		return true;
	}
	else
	{
		OS.OrderType = EOrderType::Move;
		return false;
	}
}

void AInGamePlayerController::MakeOrderStructForActorUnderCursor()
{
	auto targetActor = this->GetActorUnderCursor();

	FOrderStruct OS;
	if (!GetOrderFromActorUnderCursor(OS))
	{
		UE_LOG(TicTacLogOrderManagement, Log,
			TEXT("None orders are automatically set to Move"));
	}
	if (OS.OrderType == EOrderType::None)
	{
		OS.OrderType = EOrderType::Move;
	}
	this->TryToGiveOrderToActors(this->GetSelectedActors(), OS);
}

void AInGamePlayerController::ActivateConstructionMode(const FGameplayTag& BuildingTag)
{
	if (!HasAuthority())
	{
		if (BuildingTag.IsValid())
		{
			if (BuildingTag.MatchesTag(FGameplayTag::RequestGameplayTag(FName("FactionEntity.Building"))))
			{
				OnStartBuild.Broadcast(this, BuildingTag);
			}
		}
	}
}

void AInGamePlayerController::OrderBuildingConstruction_Implementation(FGameplayTag BuildingType, FTransform Transform, const TArray<AActor*>& InSelectedActors) {
	this->StartBuildingConstruction(BuildingType, Transform, InSelectedActors);
}

void AInGamePlayerController::StartBuildingConstruction_Implementation(FGameplayTag BuildingType, FTransform Transform, const TArray<AActor*>& InSelectedActors) {
	UE_LOG(TicTacLogOrderManagement, Log,
		TEXT("AInGamePlayerController::StartBuildingConstruction_Implementation: Function Not Implemented."));
}

void AInGamePlayerController::ForceDisconnect() {
	UGameplayStatics::GetGameInstance(this)->GetEngine()->HandleDisconnect(GetWorld(), GetWorld()->GetNetDriver());
}