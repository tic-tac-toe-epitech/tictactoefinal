// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObjectNetworkSyncronizer.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEvent_ObjectNetworkSyncronizer, AObjectNetworkSyncronizer*, Syncronizer);

UCLASS()
class TICTACTOE_API AObjectNetworkSyncronizer : public AActor
{
	GENERATED_BODY()
	
public:	
    // Sets default values for this actor's properties
    AObjectNetworkSyncronizer();

protected:
    // Is syncronizer is activated.
    UPROPERTY()
    bool bActivated = false;

    int32 NbNetworkObjects;
    
    UPROPERTY(Replicated)
    TArray<UObject*> replicatedObjects;

	UPROPERTY(Replicated)
	TArray<AActor*>	replicatedActors;
 
	// Notify server that all object are syncronized
    UFUNCTION(Server, Reliable, WithValidation)
    void NotifyServerActorsSyncronizedRPCS();
    void NotifyServerActorsSyncronizedRPCS_Implementation();
    bool NotifyServerActorsSyncronizedRPCS_Validate() { return true; }
    
	// Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:	
	virtual bool ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

    // Called every frame
    virtual void Tick(float DeltaTime) override;
    // Register object to syncronisation.
    UFUNCTION(BlueprintCallable)
    bool RegisterActor(AActor *actor);

	UFUNCTION(BlueprintCallable)
	bool RegisterObject(UObject *object);

    void Activate();

private:
	// Start syncronisation check.
    UFUNCTION(Client, Reliable)
    void ActivateInternalRPCC(int32 NbObjectsOnServer);
    void ActivateInternalRPCC_Implementation(int32 NbObjectsOnServer);

public:
    UFUNCTION()
        bool IsSynchronized() { return bIsSyncronized; }

    UPROPERTY(BlueprintAssignable)
        FEvent_ObjectNetworkSyncronizer OnClientSyncronized;

private:
    bool bIsSyncronized = false;
};
