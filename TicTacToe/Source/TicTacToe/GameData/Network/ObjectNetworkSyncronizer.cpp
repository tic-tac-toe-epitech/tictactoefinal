// Fill out your copyright notice in the Description page of Project Settings.


#include "ObjectNetworkSyncronizer.h"
#include "Misc/CoreMisc.h"
#include "UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "Logging/MessageLog.h"
#include "TicTacToe.h"


// Sets default values
AObjectNetworkSyncronizer::AObjectNetworkSyncronizer()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = IsRunningClientOnly();
    bReplicates = true;
    bOnlyRelevantToOwner = true;

	NbNetworkObjects = 0;
}

// Called when the game starts or when spawned
void AObjectNetworkSyncronizer::BeginPlay()
{
	Super::BeginPlay();
}

bool AObjectNetworkSyncronizer::ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	//for (auto& replicatedObject : replicatedObjects)
	//{
	//	if (replicatedObject != nullptr)
	//	{
	//		WroteSomething |= Channel->ReplicateSubobject(replicatedObject, *Bunch, *RepFlags);
	//	}
	//}
	return WroteSomething;
}

void AObjectNetworkSyncronizer::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Property replication sample. Replace ReplicatedProperty by your property and repeat this line for any properties that need to be replicated.
	DOREPLIFETIME(AObjectNetworkSyncronizer, replicatedObjects);
	DOREPLIFETIME(AObjectNetworkSyncronizer, replicatedActors);
}


// Called every frame
void AObjectNetworkSyncronizer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!HasAuthority())
	{
		if (bActivated && replicatedActors.Num() + replicatedObjects.Num() == NbNetworkObjects)
		{
			for (auto& actor : replicatedActors)
			{
				if (actor == nullptr)
					return;
			}
			for (auto& object : replicatedObjects)
			{
				if (object == nullptr)
					return;
			}
			NotifyServerActorsSyncronizedRPCS();
			SetActorTickEnabled(false);
		}
	}
}

void AObjectNetworkSyncronizer::NotifyServerActorsSyncronizedRPCS_Implementation() {
    bIsSyncronized = true;
	UE_LOG(TicTacLogMapGeneration, Log,
		TEXT("AObjectNetworkSyncronizer::NotifyServerActorsSyncronizedRPCS_Implementation: Synchronizer '%s' synchronized."),
		*GetNameSafe(this));
	this->SetActorTickEnabled(false);
	OnClientSyncronized.Broadcast(this);
}

bool AObjectNetworkSyncronizer::RegisterActor(AActor *actor) {
	if (actor != nullptr && actor->GetIsReplicated()) {
		replicatedActors.Add(actor);
		UE_LOG(TicTacLogMapGeneration, Log,
			TEXT("AObjectNetworkSyncronizer::RegisterActor: Actor '%s' registered."),
			*GetNameSafe(actor));
		return true;
	}
	return false;
}

bool AObjectNetworkSyncronizer::RegisterObject(UObject *object) {
	if (object != nullptr && object->IsSupportedForNetworking()) {
		replicatedObjects.Add(object);
		UE_LOG(TicTacLogMapGeneration, Log,
			TEXT("AObjectNetworkSyncronizer::RegisterObject: Object '%s' registered."),
			*GetNameSafe(object));
		return true;
	}
	return false;
}

void AObjectNetworkSyncronizer::Activate()
{
	if (HasAuthority())
	{
		ActivateInternalRPCC(replicatedActors.Num() + replicatedObjects.Num());
	}
}

void AObjectNetworkSyncronizer::ActivateInternalRPCC_Implementation(int32 NbObjectsOnServer)
{
	if (!HasAuthority())
	{
		NbNetworkObjects = NbObjectsOnServer;
		bActivated = true;
		UE_LOG(TicTacLogMapGeneration, Log,
			TEXT("AObjectNetworkSyncronizer::Activate: synchornizer '%s' activated."),
			*GetNameSafe(this));
		SetActorTickEnabled(true);
	}
}
