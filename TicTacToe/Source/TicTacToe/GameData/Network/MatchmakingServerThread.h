// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Messages/MessageUtils.h"
#include "TicTacToe.h"
#include "GameData/Network/ServerConnectionThread.h"
#include "CoreMinimal.h"
#include "Sockets.h"
#include "Networking.h"
#include "Engine/World.h"
#include "GameFramework/GameModeBase.h"
#include "HAL/Runnable.h"
#include "HAL/RunnableThread.h"
#include "GenericPlatform/GenericPlatformAffinity.h"
#include "GenericPlatform/GenericPlatformProcess.h"


class TICTACTOE_API FMatchmakingServerWorker : public FRunnable
{
	static FMatchmakingServerWorker* Runnable;

public:
	FMatchmakingServerWorker(UWorld*);
	virtual ~FMatchmakingServerWorker() {}

	virtual uint32 Run();
	virtual void Stop();

	static FMatchmakingServerWorker* JoyInit(UWorld* world);

	void receive(ServerConnectionThread* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json);

private:
	UWorld* World;

	FRunnableThread* Thread;
	FCriticalSection Mutex;
	FThreadSafeBool IsFinished;
};
