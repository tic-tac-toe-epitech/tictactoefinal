// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Messages/MessageUtils.h"
#include "Sockets.h"
#include "Networking.h"
#include "Serialization/JsonSerializer.h"
#include "HAL/Runnable.h"
#include "HAL/RunnableThread.h"
#include "GenericPlatform/GenericPlatformAffinity.h"
#include "GenericPlatform/GenericPlatformProcess.h"
#include "CoreMinimal.h"
#include <functional>

class TICTACTOE_API ServerConnectionThread : public FRunnable
{
public:
	typedef std::function<void(ServerConnectionThread* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json)> PacketHandler;

	~ServerConnectionThread();

	/*
	@brief Entry point of the thread
	*/
	virtual uint32 Run();

	/*
	@brief Stops the thread
	*/
	virtual void Stop();

	/*
	@brief Setup the thread
	*/
	void Setup(FSocket *socket, PacketHandler callback);

	/*
	@brief Get the underlying Socket that this connection currently reads on
	@return returns the socket
	*/
	FSocket* GetSocket() {
		return this->Connection;
	}

private:
	FRunnableThread* Thread;
	FSocket* Connection;

	FCriticalSection Mutex;
	FEvent* Semaphore;
	FThreadSafeBool IsFinished;
	PacketHandler Callback;
};
