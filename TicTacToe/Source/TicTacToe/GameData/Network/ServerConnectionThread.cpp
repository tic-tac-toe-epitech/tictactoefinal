// Fill out your copyright notice in the Description page of Project Settings.


#include "ServerConnectionThread.h"

ServerConnectionThread::~ServerConnectionThread()
{
	if (Semaphore)
	{
		// Cleanup the FEvent
		FGenericPlatformProcess::ReturnSynchEventToPool(Semaphore);
		Semaphore = nullptr;
	}

	if (Thread)
	{
		// Cleanup the worker thread
		delete Thread;
		Thread = nullptr;
	}
}

void ServerConnectionThread::Setup(FSocket* connectionSocket, PacketHandler callback) {
	this->Connection = connectionSocket;
	this->Callback = callback;
	this->IsFinished = false;

	Thread = FRunnableThread::Create(this, TEXT("ConnectionThread"), 0, TPri_BelowNormal);
}

uint32 ServerConnectionThread::Run() {
	while (!IsFinished)
	{
		uint32 size = 0;
		TArray<uint8> ReceivedData;

		while (this->Connection->HasPendingData(size)) {
			ReceivedData.Init(FMath::Min(size, 65507u), size);

			int32 Read = 0;
			this->Connection->Recv(ReceivedData.GetData(), ReceivedData.Num(), Read);
		}

		if (ReceivedData.Num() > 0) {
			MessageUtils::MessageHeader header;
			TSharedPtr<FJsonObject> body = MakeShareable(new FJsonObject());

			memcpy(&header, ReceivedData.GetData(), sizeof(MessageUtils::MessageHeader));

			if (header.length) {
				ReceivedData.RemoveAt(0, 4, true);
				FString jsonBody;

				for (auto& Str : ReceivedData)
				{
					jsonBody += (char) Str;
				}

				// Deserialize JSON
				TSharedRef<TJsonReader<>> jsonReader = TJsonReaderFactory<>::Create(*jsonBody);
				FJsonSerializer::Deserialize(jsonReader, body);
			}

			this->Callback(this, header, body);
		}

		// Finish the thread if the connection is closed
		if (this->GetSocket()->GetConnectionState() == SCS_ConnectionError ||
			this->GetSocket()->GetConnectionState() == SCS_NotConnected) {
			this->Stop();
		}
	}

	this->Connection->Close();
	return 0;
}

void ServerConnectionThread::Stop() {
	this->IsFinished = true;
	if (this->Semaphore)
	{
		Semaphore->Trigger();
	}
}
