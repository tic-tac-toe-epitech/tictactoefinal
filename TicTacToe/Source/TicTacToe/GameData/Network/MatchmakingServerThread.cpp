// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchmakingServerThread.h"

FMatchmakingServerWorker* FMatchmakingServerWorker::Runnable = NULL;

FMatchmakingServerWorker::FMatchmakingServerWorker(UWorld* world)
{
	this->World = world;
	this->IsFinished = false;

	Thread = FRunnableThread::Create(this, TEXT("FPrimeNumberWorker"), 0, TPri_BelowNormal);
}

uint32 FMatchmakingServerWorker::Run()
{
	FString portArg;
	int port = 8091;
	if (FParse::Value(FCommandLine::Get(), TEXT("MM_PORT"), portArg))
	{
		port  = FCString::Atoi(*portArg);
	}

	FIPv4Endpoint Endpoint(FIPv4Address(0, 0, 0, 0), port);
	FSocket* ListenSocket = FTcpSocketBuilder(FString("GameStatusListener"))
		.AsReusable()
		.BoundToEndpoint(Endpoint)
		.Listening(8);

	int32 NewSize = 0;
	ListenSocket->SetReceiveBufferSize(2 * 1024 * 1024, NewSize);

	TSharedRef<FInternetAddr> RemoteAddress = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	bool Pending = false;

	UE_LOG(MatchmakingService, Warning, TEXT("Listening for connections on %s"), *Endpoint.ToString());

	while (!Pending && !IsFinished) {
		ListenSocket->HasPendingConnection(Pending);

		if (Pending) {
			FSocket *ConnectionSocket = ListenSocket->Accept(*RemoteAddress, TEXT("GameStatus connection"));

			if (ConnectionSocket != NULL)
			{
				ServerConnectionThread* connection = new ServerConnectionThread();
				connection->Setup(ConnectionSocket, std::bind(&FMatchmakingServerWorker::receive, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
			}

			Pending = false;
		}
	}

	ListenSocket->Close();
	return 0;
}

void FMatchmakingServerWorker::receive(ServerConnectionThread* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json) {
	if (header.type == MessageUtils::GAME_STATUS_REQUEST) {
		TSharedPtr<FJsonObject> body = MakeShareable(new FJsonObject());

		int players = this->World->GetAuthGameMode()->GetNumPlayers();
		int listeningPort = this->World->URL.Port;

		body->SetNumberField("players", players);
		body->SetStringField("state", "WAITING");
		body->SetNumberField("port", listeningPort);

		MessageUtils::Send(connection->GetSocket(), MessageUtils::GAME_STATUS_RESPONSE, body);

		connection->Stop();
	}
}

void FMatchmakingServerWorker::Stop()
{
	this->IsFinished = true;
	this->Thread->WaitForCompletion();
}

FMatchmakingServerWorker* FMatchmakingServerWorker::JoyInit(UWorld *world)
{
	if (Runnable != NULL) {
		Runnable->Stop();
		Runnable = NULL;
	}

	if (!Runnable && FPlatformProcess::SupportsMultithreading())
	{
		Runnable = new FMatchmakingServerWorker(world);
	}
	return Runnable;
}