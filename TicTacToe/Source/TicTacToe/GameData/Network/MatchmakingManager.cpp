// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchmakingManager.h"

void UMatchmakingManager::Init(FIPv4Address mmHost, int mmPort, ServerConnectionThread::PacketHandler packetHandler) {
	this->State = ConnectionState::NOT_CONNECTED;

	this->MMHost = mmHost;
	this->MMPort = mmPort;
	this->Handler = packetHandler;
}

void UMatchmakingManager::Disconnect() {
	Socket->Close();

	this->State = ConnectionState::NOT_CONNECTED;
}

bool UMatchmakingManager::Connect() {
	this->Socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("default"), false);

	TSharedRef<FInternetAddr> addr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	addr->SetIp(this->MMHost.Value);
	addr->SetPort(this->MMPort);

	UE_LOG(MatchmakingService, Warning, TEXT("Connecting to %s"), *addr.Get().ToString(1));

	this->State = ConnectionState::CONNECTING;
	if (Socket->Connect(*addr))
	{
		ServerConnectionThread* proxyConnection = new ServerConnectionThread();
		proxyConnection->Setup(this->Socket, this->Handler);
		
		this->State = ConnectionState::CONNECTED;

		UE_LOG(MatchmakingService, Warning, TEXT("Connected"));
		return true;
	}

	UE_LOG(MatchmakingService, Error, TEXT("Connection failed"));
	this->State = ConnectionState::NOT_CONNECTED;
	return false;
}

void UMatchmakingManager::SendSearchMatch() {
	if (this->Socket->GetConnectionState() != ESocketConnectionState::SCS_Connected) {
		if (!this->Connect()) {
			return;
		}
	}

	if (MessageUtils::Send(this->Socket, MessageUtils::START_SEARCH_MATCH)) {
		this->State = ConnectionState::SEARCHING;
	}
}

void UMatchmakingManager::SendStopSearchMatch() {
	if (this->Socket->GetConnectionState() != ESocketConnectionState::SCS_Connected) {
		return;
	}

	MessageUtils::Send(this->Socket, MessageUtils::STOP_SEARCH_MATCH);
}

UMatchmakingManager::ConnectionState UMatchmakingManager::GetConnectionState() {
	return this->State;
}

bool UMatchmakingManager::IsConnected() {
	return (this->Socket->GetConnectionState() == ESocketConnectionState::SCS_Connected);
}