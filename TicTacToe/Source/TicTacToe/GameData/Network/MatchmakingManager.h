// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Messages/MessageUtils.h"
#include "TicTacToe.h"
#include "Networking.h"
#include "Sockets.h"
#include "EngineUtils.h"
#include "CoreMinimal.h"
#include "UObject/UObjectIterator.h"
#include "Containers/UnrealString.h"
#include "Kismet/GameplayStatics.h"
#include "GameData/Network/ServerConnectionThread.h"
#include "GameData/GUI/SearchGameWidget.h"
#include "MatchmakingManager.generated.h"

/**
 * @class class helper used to interact with the matchmaking service
 */
UCLASS()
class TICTACTOE_API UMatchmakingManager : public UObject
{
	GENERATED_BODY()
	
public:
	enum ConnectionState {
		NOT_CONNECTED = 0,
		CONNECTED = 1,
		CONNECTING = 2,
		SEARCHING = 3,
		MATCH_FOUND = 4
	};

	/*
	@brief Init the matchmaking service helper
	@param host address to connect to
	@param port to connect to
	@param callback used to read a new packet
	*/
	void Init(FIPv4Address host, int port, ServerConnectionThread::PacketHandler packetHandler);
	
	/*
	@brief Connect to the matchmaking service
	@return Return if connection succeeded or not
	*/
	bool Connect();

	/*
	@brief Disconnects from the matchmaking service
	*/
	void Disconnect();

	/*
	@brief Sends a packet to the matchmaking service to start a game search.
	*/
	void SendSearchMatch();

	/*
	@brief Sends a packet to the matchmaking service to start a game search.
	*/
	void SendStopSearchMatch();

	/*
	@brief Get the connection state
	@returns ConnectionState
	*/
	ConnectionState GetConnectionState();

	/*
	@brief Returns if it's connected to the matchmaking service
	@returns bool
	*/
	bool IsConnected();

private:
	const UWorld* World;
	
	FSocket* Socket;
	ConnectionState State;
	ServerConnectionThread::PacketHandler Handler;

	FIPv4Address MMHost;
	int MMPort;
};
