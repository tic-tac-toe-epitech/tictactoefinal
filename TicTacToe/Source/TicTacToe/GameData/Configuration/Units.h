#pragma once

#include "TicTacToe/GameData/Configuration/Units/UnitConfig.h"
#include "Units.generated.h"

//!
//! @struct FUnitsStruct
//! @brief Game units configuration.
//!
USTRUCT(BlueprintType)
struct FUnitsStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Units constructor.
		//!
		FUnitsStruct() : _GlobalId("FactionEntity.Unit")
		{
			_Units.Add({"FactionEntity.Unit.Soldier", "Soldier", 100, 0, FUnitDamageStruct(), 1.0, FPriceStruct(100, 0, 0), 15, 0 });
			_Units[0]._Damage._Value = 20;
			_Units[0]._Damage._Modifiers.Add(FUnitDamageModifierStruct("Building", 5.0));

			_Units.Add({"FactionEntity.Unit.Shooter", "Shooter", 80, 3, FUnitDamageStruct(), 1.0, FPriceStruct(100, 15, 0), 15, 0 });
			_Units[1]._Damage._Value = 25;

			_Units.Add({"FactionEntity.Unit.FlywheelMotor", "Flywheel motor", 150, 2, FUnitDamageStruct(), 2.0, FPriceStruct(0, 100, 0), 15, 0 });
			_Units[2]._Damage._Value = 25;

			_Units.Add({"FactionEntity.Unit.Artillery", "Artillery", 150, 10, FUnitDamageStruct(), 0.5, FPriceStruct(0, 150, 50), 15, 0 });
			_Units[3]._Damage._Value = 30;

			_Units.Add({"FactionEntity.Unit.Villager", "Villager", 50, 0, FUnitDamageStruct(), 1.0, FPriceStruct(50, 0, 0), 15, 0 });
			_Units[4]._Damage._Value = 5;

			_Units.Add({"FactionEntity.Unit.Cyborg", "Cyborg", 200, 0, FUnitDamageStruct(), 2.0, FPriceStruct(150, 30, 10), 15, 0 });
			_Units[5]._Damage._Value = 40;

			_Units.Add({"FactionEntity.Unit.FlameThrower", "Flame thrower", 150, 3, FUnitDamageStruct(), 1.0, FPriceStruct(150, 40, 15), 15, 60 });
			_Units[6]._Damage._Value = 40;

			_Units.Add({"FactionEntity.Unit.Aircraft", "Aircraft", 250, 2, FUnitDamageStruct(), 3.0, FPriceStruct(0, 150, 75), 15, 60 });
			_Units[7]._Damage._Value = 50;

			_Units.Add({"FactionEntity.Unit.MedicalEngineer", "Medical Engineer", 150, 3, FUnitDamageStruct(), 2.0, FPriceStruct(150, 50, 25), 15, 0 });
			_Units[8]._Damage._Value = 15;

			_Units.Add({"FactionEntity.Unit.Scout", "Scout", 100, 0, FUnitDamageStruct(), 4.0, FPriceStruct(50, 0, 0), 15, 60 });
		}

	public:
		// Units
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		TArray<FUnitStruct>	_Units;

		// Units ID.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FString						_GlobalId;

public:
		static FUnitStruct &GetUnitData(FUnitsStruct &UnitsStruct, const FString& UnitID)
		{
			for (auto& unit : UnitsStruct._Units)
			{
				if (unit._Id == UnitID)
				{
					return unit;
				}
			}
			return UnitsStruct._Units[0];
		}
};