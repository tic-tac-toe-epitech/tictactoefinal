#pragma once

#include "TicTacToe/GameData/Configuration/Shared/Price.h"
#include "BuildingConfig.generated.h"

//!
//! @struct FBuildingStruct
//! @brief Building configuration (contain one building information's).
//!
USTRUCT(BlueprintType)
struct FBuildingStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Defaut building structure constructor. With default value, the building is not valid.
		//!
		FBuildingStruct() : _Id(""), _Name(""), _Hp(0), _Price(), _Heal(0), _Range(0),
							_BuildAllow(true), _BuildTime(0), _ResearchTime(0), _UpgradeCost() {}
		//!
		//! @brief Building structure constructor.
		//! @param id Building ID.
		//! @param name Building name.
		//! @param hp Building Health Points.
		//! @param price Building price.
		//!
		FBuildingStruct(const FString &id, const FString &name,
						int32 hp, const FPriceStruct &price, int32 heal,
						int32 range, int8 buildAllow, int32 buildTime,
						int32 researchTime, const FPriceStruct &upgradeCost) :	_Id(id), _Name(name), _Hp(hp), _Price(price),
																				_Heal(heal), _Range(range), _BuildAllow(buildAllow),
																				_BuildTime(buildTime), _ResearchTime(researchTime),
																				_UpgradeCost(upgradeCost) {}

	public:
		// Building ID.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		FString			_Id;
		// Building name.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		FString			_Name;
		// Building Health Points.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		int32			_Hp;
		// Building price.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		FPriceStruct	_Price;
		// Building heal.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		int32			_Heal;
		// Building range.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		int32			_Range;
		// Tell if the building is allowed to be build.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		uint8			_BuildAllow;
		// Building build time.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		int32			_BuildTime;
		// Building research time.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		int32			_ResearchTime;
		// Building cost for upgrade.
		UPROPERTY(BlueprintReadOnly, Category = "Buildings.Building")
		FPriceStruct	_UpgradeCost;
};