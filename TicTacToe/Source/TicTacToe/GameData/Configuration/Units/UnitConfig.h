#pragma once

#include "TicTacToe/GameData/Configuration/Units/Damage.h"
#include "TicTacToe/GameData/Configuration/Shared/Price.h"
#include "UnitConfig.generated.h"

//!
//! @struct FUnitStruct
//! @brief Unit configuration (contain one unit information's).
//!
USTRUCT(BlueprintType)
struct FUnitStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Defaut unit structure constructor. With default value, the unit is not valid.
		//!
		FUnitStruct() : _Id(""), _Name(""), _Hp(0), _Scope(0), _Damage(), _Speed(0), _Price(), _HiringTime(0), _ResearchTime(0) {}
		//!
		//! @brief Unit structure constructor.
		//! @param id Unit ID.
		//! @param name Unit name.
		//! @param hp Unit Health Points.
		//! @param scope Unit map scope.
		//! @param damage Unit damage.
		//! @param speed Unit move speed.
		//! @param price Unit price.
		//! @param hiringTime Unit hiring time.
		//! @param researchTime Unit research time to unlock it.
		//!
		FUnitStruct(const FString &id, const FString &name, int32 hp, int32 scope,
					const FUnitDamageStruct &damage, float speed, const FPriceStruct &price,
					int32 hiringTime, int32 researchTime) : _Id(id), _Name(name), _Hp(hp), _Scope(scope), _Damage(damage),
															_Speed(speed), _Price(price), _HiringTime(hiringTime), _ResearchTime(researchTime) {}

	public:
		// Unit ID.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		FString				_Id;
		// Unit name.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		FString				_Name;
		// Unit Health Points.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		int32				_Hp;
		// Unit map scope.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		int32				_Scope;
		// Unit damage.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		FUnitDamageStruct	_Damage;
		// Unit move speed.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		float				_Speed;
		// Unit price.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		FPriceStruct		_Price;
		// Unit hiring time.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		int32				_HiringTime;
		// Unit research time to unlock it.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit")
		int32				_ResearchTime;
};