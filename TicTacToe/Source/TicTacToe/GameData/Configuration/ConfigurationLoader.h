// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TicTacToe/GameData/Configuration/Map.h"
#include "TicTacToe/GameData/Configuration/Resources.h"
#include "TicTacToe/GameData/Configuration/General.h"
#include "TicTacToe/GameData/Configuration/Units.h"
#include "TicTacToe/GameData/Configuration/Buildings.h"
#include "TicTacToe/TicTacToe.h"
#include "Paths.h"
#include "Json.h"
#include "ConfigurationLoader.generated.h"

//!
//! @class UConfigurationLoader
//! @brief Load and format all data used as game configuration.
//!
UCLASS(BlueprintType)
class TICTACTOE_API UConfigurationLoader : public UObject
{
	GENERATED_BODY()

	private:
		// Default configuration directory path, all json configuration file are stocked here.
		const FString	DefaultConfigurationDirectoryPath = FPaths::ConvertRelativePathToFull(FPaths::ProjectConfigDir() + FString("Game"));

	private:
		// General server game configuraiton.
		UPROPERTY()
		FBuildingsStruct	_Buildings;
		// General server game configuraiton.
		UPROPERTY()
		FGeneralStruct		_General;
		// Map configuration.
		UPROPERTY()
		FMapStruct			_Map;
		// Game resources.
		UPROPERTY()
		FResourcesStruct	_Resources;
		// Units configuraiton.
		UPROPERTY()
		FUnitsStruct		_Units;

	public:
		//!
		//! @brief Load the buildings configuration from the system json file and return the data previously loaded.
		//! @return Buildings configuration.
		//!
		UFUNCTION(BlueprintCallable)
		FBuildingsStruct		LoadAndGetBuildingsConfig();
		//!
		//! @brief Load the buildings configuration from the system json file.
		//!
		UFUNCTION(BlueprintCallable)
		void					LoadBuildingsConfig();
		//!
		//! @brief Extract the buildings configuration. The configuration must be loaded before.
		//! @return Buildings configuration.
		//!
		UFUNCTION(BlueprintGetter)
		inline FBuildingsStruct	GetBuildingsConfig() const { return (_Buildings); }

	public:
		//!
		//! @brief Load the map configuration from the system json file and return the data previously loaded.
		//! @return Map configuration.
		//!
		UFUNCTION(BlueprintCallable)
		FMapStruct			LoadAndGetMapConfig();
		//!
		//! @brief Load the map configuration from the system json file.
		//!
		UFUNCTION(BlueprintCallable)
		void				LoadMapConfig();
		//!
		//! @brief Extract the map configuration. The configuration must be loaded before.
		//! @return Map configuration.
		//!
		UFUNCTION(BlueprintGetter)
		inline FMapStruct	GetMapConfig() const { return (_Map); }

	public:
		//!
		//! @brief Load the resources configuration from the system json file and return the data previously loaded.
		//! @return Resources configuration.
		//!
		UFUNCTION(BlueprintCallable)
		FResourcesStruct		LoadAndGetResourcesConfig();
		//!
		//! @brief Load the resources configuration from the system json file.
		//!
		UFUNCTION(BlueprintCallable)
		void					LoadResourcesConfig();
		//!
		//! @brief Extract the resources configuration. The configuration must be loaded before.
		//! @return Resources configuration.
		//!
		UFUNCTION(BlueprintGetter)
		inline FResourcesStruct	GetResourcesConfig() const { return (_Resources); }
		
	public:
		//!
		//! @brief Load the general configuration from the system json file and return the data previously loaded.
		//! @return General configuration.
		//!
		UFUNCTION(BlueprintCallable)
		FGeneralStruct			LoadAndGetGeneralConfig();
		//!
		//! @brief Load the general configuration from the system json file.
		//!
		UFUNCTION(BlueprintCallable)
		void					LoadGeneralConfig();
		//!
		//! @brief Extract the general configuration. The configuration must be loaded before.
		//! @return General configuration.
		//!
		UFUNCTION(BlueprintGetter)
		inline FGeneralStruct	GetGeneralConfig() const { return (_General); }


	public:
		//!
		//! @brief Load all units configuration from the system json file and return the data previously loaded.
		//! @return Units configuration.
		//!
		UFUNCTION(BlueprintCallable)
		FUnitsStruct			LoadAndGetUnitsConfig();
		//!
		//! @brief Load the units configuration from the system json file.
		//!
		UFUNCTION(BlueprintCallable)
		void					LoadUnitsConfig();
		//!
		//! @brief Extract the units configuration. The configuration must be loaded before.
		//! @return Units configuration.
		//!
		UFUNCTION(BlueprintGetter)
		inline FUnitsStruct		GetUnitsConfig() const { return (_Units); }


	private:
		//!
		//! @brief Load a generic json object.
		//! @param fileName Json file name to load.
		//! @param jsonObject Json object (unloaded) for the file to load.
		//! @return Loaded json.
		//!
		const TSharedPtr<FJsonObject>	LoadJsonObject(const FString &fileName, TSharedPtr<FJsonObject> &jsonObject);
		//!
		//! @brief Extract all information from a json object (already deserialize) to own structure.
		//! @param jsonObject Json object containing all deserialized data.
		//!
		void							ExtractBuildingsStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject);
		//!
		//! @brief Extract all information from a json object (already deserialize) to own structure.
		//! @param jsonObject Json object containing all deserialized data.
		//!
		void							ExtractMapStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject);
		//!
		//! @brief Extract all information from a json object (already deserialize) to own structure.
		//! @param jsonObject Json object containing all deserialized data.
		//!
		void							ExtractResourcesStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject);
		//!
		//! @brief Extract all information from a json object (already deserialize) to own structure.
		//! @param jsonObject Json object containing all deserialized data.
		//!
		void							ExtractGeneralStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject);
		//!
		//! @brief Extract all information from a json object (already deserialize) to own structure.
		//! @param jsonObject Json object containing all deserialized data.
		//!
		void							ExtractUnitsStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject);
		//!
		//! @brief Extract all information from a specific unit (already deserialize) to own structure.
		//! @param jsonObject Json object containing all deserialized data.
		//! @param unit Custom structure how containing all data (out).
		//!
		void							ExtractSpecificUnitStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject, FUnitStruct &unit);
		//!
		//! @brief Extract all information from a specific unit (already deserialize) to own structure.
		//! @param jsonObject Json object containing all deserialized data.
		//! @param building Custom structure how containing all data (out).
		//!
		void							ExtractSpecificBuildingStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject, FBuildingStruct &building);
};
