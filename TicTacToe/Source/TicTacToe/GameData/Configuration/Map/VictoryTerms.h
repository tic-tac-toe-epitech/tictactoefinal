#pragma once

#include "CoreMinimal.h"
#include "VictoryTerms.generated.h"

//!
//! @struct VictoryTerms
//! @brief Define all victory conditions in a map context.
//!
USTRUCT(BlueprintType)
struct TICTACTOE_API FVictoryTermsStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Default constructor for victory terms. Default value define on game design documentation.
		//!
		FVictoryTermsStruct() : _LineGoal(3), _BAllowVertical(true), _BAllowHorizontal(true), _BAllowDiagonals(true) {}

	public:
		// Number of case needed to do a line (win condition).
		UPROPERTY(BlueprintReadOnly, Category = "Map.VictoryTerms")
		int32		_LineGoal;
		// Explicit if vertical line are allow as victory.
		UPROPERTY(BlueprintReadOnly, Category = "Map.VictoryTerms")
		uint8		_BAllowVertical;
		// Explicit if horizontal line are allow as victory.
		UPROPERTY(BlueprintReadOnly, Category = "Map.VictoryTerms")
		uint8		_BAllowHorizontal;
		// Explicit if diagonals line are allow as victory.
		UPROPERTY(BlueprintReadOnly, Category = "Map.VictoryTerms")
		uint8		_BAllowDiagonals;

};