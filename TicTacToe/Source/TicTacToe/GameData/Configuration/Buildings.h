#pragma once

#include "TicTacToe/GameData/Configuration/Building/BuildingConfig.h"
#include "Containers/Map.h"
#include "Buildings.generated.h"

//!
//! @struct FBuildingsStruct
//! @brief Game buildings configuration.
//!
USTRUCT(BlueprintType)
struct FBuildingsStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Buildings constructor.
		//!
		FBuildingsStruct() : _GlobalId("FactionEntity.Building")
		{
			_Buildings.Add({ "FactionEntity.Building.Heart", "Heart", 2500, FPriceStruct(0, 0, 0), 0, 0, false, 0, 0, FPriceStruct(10, 5, 2) });
			_Buildings.Add({ "FactionEntity.Building.CityHall", "City Hall", 2500, FPriceStruct(0, 0, 0), 0, 0, false, 0, 0, FPriceStruct(200, 150, 100) });
			_Buildings.Add({ "FactionEntity.Building.Laboratory", "Laboratory", 500, FPriceStruct(200, 150, 50), 0, 0, false, 30, 0, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Sawmill", "Sawmill", 500, FPriceStruct(200, 0, 0), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Quarry", "Quarry", 500, FPriceStruct(500, 0, 0), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Outpost", "Outpost", 2500, FPriceStruct(1000, 300, 100), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Forge", "Forge", 500, FPriceStruct(0, 200, 100), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Hospital", "Hospital", 500, FPriceStruct(400, 200, 100), 10, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Machinery", "Machinery", 500, FPriceStruct(500, 300, 100), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Barracks", "Barracks", 500, FPriceStruct(300, 50, 0), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.ShootingRange", "ShootingRange", 500, FPriceStruct(300, 100, 0), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.SpacePort", "SpacePort", 500, FPriceStruct(0, 400, 200), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Tower", "Tower", 1000, FPriceStruct(0, 100, 20), 0, 5, false, 30, 60, FPriceStruct(0, 0, 0) });
			_Buildings.Add({ "FactionEntity.Building.Wall", "Wall", 2000, FPriceStruct(0, 20, 0), 0, 0, true, 30, 0, FPriceStruct(0, 0, 0) });
		}

	public:
		// Buildings
		UPROPERTY(BlueprintReadOnly, Category = "Building")
		TArray<FBuildingStruct>	_Buildings;
		// Global Building Gameplay tag.
		UPROPERTY(BlueprintReadOnly, Category = "Building")
		FString				_GlobalId;

public:
	static FBuildingStruct &GetBuildingData(FBuildingsStruct &BuildingsStruct, const FString &BuildingID)
	{
		for (auto& building : BuildingsStruct._Buildings)
		{
			if (building._Id == BuildingID)
			{
				return building;
			}
		}
		return BuildingsStruct._Buildings[0];
	}
};