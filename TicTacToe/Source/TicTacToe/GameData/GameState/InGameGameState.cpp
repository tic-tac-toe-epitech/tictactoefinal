// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameGameState.h"
#include "GameData/Configuration/ConfigurationLoader.h"
#include "Kismet/GameplayStatics.h"
#include "GameData/Controller/InGamePlayerController.h"
#include "GameData/GameInstance/TicTacGameInstance.h"
#include "UnrealNetwork.h"


AInGameGameState::AInGameGameState()
{
	bReplicates = true;
}

void AInGameGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AInGameGameState, MapData);
	DOREPLIFETIME(AInGameGameState, UnitsData);
	DOREPLIFETIME(AInGameGameState, BuildingsData);
}

void AInGameGameState::PreInitializeComponents()
{
	if (HasAuthority())
	{
		UConfigurationLoader* loader = NewObject<UConfigurationLoader>(this);

		MapData = loader->LoadAndGetMapConfig();
		UnitsData = loader->LoadAndGetUnitsConfig();
		BuildingsData = loader->LoadAndGetBuildingsConfig();
	}
}

void AInGameGameState::BeginPlay()
{
	Super::BeginPlay();
}

void AInGameGameState::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchIsWaitingToStart();

	if (!HasAuthority())
	{
		AInGamePlayerController* localController = Cast<AInGamePlayerController>(UGameplayStatics::GetPlayerController(this, 0));

		if (localController)
		{
			localController->HandletMatchIsWaitingToStart();
		}
	}
}
void AInGameGameState::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	UTicTacGameInstance::RemoveLoadingScreenWithoutContext(this);

	if (!HasAuthority())
	{
		AInGamePlayerController* localController = Cast<AInGamePlayerController>(UGameplayStatics::GetPlayerController(this, 0));

		if (localController)
		{
			localController->HandleStartMatch();
		}
	}
}

void AInGameGameState::HandleMatchHasEnded()
{
	Super::HandleMatchHasEnded();

	//TODO : DO THAT THING -- UI mode
	//UTicTacGameInstance::RemoveLoadingScreenWithoutContext(this);

	if (!HasAuthority())
	{
		AInGamePlayerController* localController = Cast<AInGamePlayerController>(UGameplayStatics::GetPlayerController(this, 0));

		if (localController)
		{
			localController->HandleMatchHasEnded();
		}
	}
}

void AInGameGameState::DeclareWinner(int playerIndex)
{
	int i = 1;
}
//
//void AInGameGameState::DeclareWinner_Implementation(int playerIndex)
//{
//}
