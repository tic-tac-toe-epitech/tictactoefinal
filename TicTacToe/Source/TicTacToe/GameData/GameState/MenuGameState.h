// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <functional>
#include "GameData/GUI/SearchGameWidget.h"
#include "GameData/GUI/LoadingScreen.h"
#include "GameData/Network/MatchmakingManager.h"
#include "GameData/Configuration/ConfigurationLoader.h"
#include "GenericPlatform/GenericPlatformProcess.h"
#include "SocketSubsystem.h"
#include "Sockets.h"
#include "Networking.h"
#include "CoreMinimal.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/GameState.h"
#include "MenuGameState.generated.h"


UCLASS()
class TICTACTOE_API AMenuGameState : public AGameState
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	AMenuGameState();

	TSubclassOf<USearchGameWidget> MainMenuWidget;
	TSubclassOf<ULoadingScreen> LoadingScreenWidget;

	UFUNCTION()
	void SearchGame();

	void ClearViewport();

	void OnDisconnect();
	void OnPacketReceived(ServerConnectionThread* connection, 
						  MessageUtils::MessageHeader header,
						  TSharedPtr<FJsonObject> json);
private:
	UMatchmakingManager* MMManager;
	FGeneralStruct GeneralConfig;
	UConfigurationLoader *ConfigurationLoader;
};
