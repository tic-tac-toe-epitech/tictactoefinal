// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "GameData/Configuration/Units.h"
#include "GameData/Configuration/Buildings.h"
#include "GameData/Configuration/Map.h"
#include "Templates/SharedPointer.h"
#include "InGameGameState.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API AInGameGameState : public AGameState
{
	GENERATED_BODY()

public:
	AInGameGameState();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void PreInitializeComponents() override;

protected:
	virtual void BeginPlay() override;
	
	virtual void HandleMatchIsWaitingToStart() override;

	virtual void HandleMatchHasStarted() override;

	virtual void HandleMatchHasEnded() override;

private:
	UPROPERTY(Replicated)
		FMapStruct MapData;

	UPROPERTY(Replicated)
		FUnitsStruct UnitsData;

	UPROPERTY(Replicated)
		FBuildingsStruct BuildingsData;

public:
	UFUNCTION(BlueprintPure)
		FMapStruct& GetMapData() { return MapData; }

	UFUNCTION(BlueprintPure)
		FUnitsStruct& GetUnitsData() { return UnitsData; }

	UFUNCTION(BlueprintPure)
		FBuildingsStruct& GetBuildingsData() { return BuildingsData; }

	UFUNCTION(BlueprintPure)
		FBuildingStruct &GetBuildingData(const FString& BuildingID) { return FBuildingsStruct::GetBuildingData(BuildingsData, BuildingID); }

	//UFUNCTION(BlueprintNativeEvent)
	//	void DeclareWinner(int playerIndex);
	//	void DeclareWinner_Implementation(int playerIndex);
	UFUNCTION(BlueprintCallable)
	void DeclareWinner(int playerIndex);
};
