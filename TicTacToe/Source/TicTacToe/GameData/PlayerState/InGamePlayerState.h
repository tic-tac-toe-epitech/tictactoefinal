// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "GameplayTagContainer.h"
#include "InGame/ResourcesManager/ResourcesManagerComponent.h"
#include "InGamePlayerState.generated.h"


class AUnit;
class ABuilding;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_InGamePlayerState_Unit, AInGamePlayerState*, PlayerState, AUnit*, Unit);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_InGamePlayerState_Building, AInGamePlayerState*, PlayerState, ABuilding*, Building);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_InGamePlayerState_int32, AInGamePlayerState*, PlayerState, int32, Value);

/**
 * 
 */
UCLASS()
class TICTACTOE_API AInGamePlayerState : public APlayerState
{
	GENERATED_BODY()
public:
	AInGamePlayerState();

	virtual void PreInitializeComponents() override;

protected:
	UPROPERTY(ReplicatedUsing = OnRep_MaxPopulation)
		int32 MaximumPopulation;

	UFUNCTION()
		void OnRep_MaxPopulation();

	UPROPERTY()
		TArray<AUnit*> Units;

	UPROPERTY()
		TArray<ABuilding*> Buildings;

	UPROPERTY(Replicated)
		UResourcesManagerComponent *PlayerRessources;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

public:

	UFUNCTION(BlueprintCallable)
		UResourcesManagerComponent *GetResourcesManager() { return PlayerRessources; }

	UFUNCTION(BlueprintPure)
		bool IsPopulationFull() const;

	UFUNCTION(BlueprintPure)
		int32 GetCurrentPopulation() const { return Units.Num(); }

	UFUNCTION(BlueprintPure)
		int32 GetMaxPopulation() const { return MaximumPopulation; }

	UFUNCTION(BlueprintCallable)
		void SetMaximumPopulation(int32 InMaxPop);

	UFUNCTION(BlueprintCallable)
		void AddMaximumPopulation(int32 AdditionnalMaxPop);

	UFUNCTION(BlueprintPure)
		UResourcesManagerComponent* GetPlayerResources() { return PlayerRessources; }

	UFUNCTION(BlueprintPure)
		const TArray<AUnit*>& GetUnits() const { return Units; }

	UFUNCTION(BlueprintPure)
		const TArray<ABuilding*>& GetBuildings() const { return Buildings; }

	UFUNCTION(BlueprintPure)
		TArray<AUnit*> GetAllUnitsOfType(FGameplayTag UnitType);

	UFUNCTION(BlueprintPure)
		TArray<ABuilding*> GetAllBuildingsOfType(FGameplayTag BuildingType);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void AddUnitRPCM(AUnit* Unit);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void AddBuildingRPCM(ABuilding* Building);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void RemoveUnitRPCM(AUnit* Unit);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void RemoveBuildingRPCM(ABuilding* Building);

	UFUNCTION(BlueprintCallable)
		void AddUnit(AUnit* Unit);

	UFUNCTION(BlueprintCallable)
		void AddBuilding(ABuilding* Building);

	UFUNCTION(BlueprintCallable)
		void RemoveUnit(AUnit* Unit);

	UFUNCTION(BlueprintCallable)
		void RemoveBuilding(ABuilding* Building);

	UPROPERTY(BlueprintAssignable)
		FEvent_InGamePlayerState_int32 OnMaximumPopulationChanged;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerState_Unit OnUnitAdded;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerState_Unit OnUnitRemoved;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerState_Building OnBuildingAdded;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerState_Building OnBuildingRemoved;
};
