// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "InGameGameMode.generated.h"

class AUnit;
class ABuilding;
class UUnitSpawner;
class UStartingPlayerActors;
class UMapGenerator;
class AObjectNetworkSyncronizer;
class AInGamePlayerController;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FEvent_InGameGameMode_ObjectNetworkSyncronizer_InGamePlayerController, AInGameGameMode*, GameMode, AObjectNetworkSyncronizer*, Synchronizer, AInGamePlayerController*, OwningPlayer);

UCLASS()
class TICTACTOE_API AInGameGameMode : public AGameMode
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	virtual void HandleMatchIsWaitingToStart() override;

	virtual bool ReadyToStartMatch_Implementation() override;

	virtual void HandleMatchHasStarted() override;

	int32 InitIndex = 0;

public:
	UFUNCTION(BlueprintCallable)
	AUnit* SpawnNewUnit(TSoftClassPtr<AUnit> UnitClass, APlayerController* OwningPlayer, FTransform UnitTransform);

	UPROPERTY()
		class UMatchmakingManager* mmManager;

	UPROPERTY()
		class UMatchmakingServerManager* mmServerManager;

	UFUNCTION(BlueprintCallable)
	ABuilding* SpawnNewBuilding(TSoftClassPtr<ABuilding> BuildingClass, APlayerController* OwningPlayer, FTransform BuildingTransform);

	UPROPERTY(EditAnywhere)
	TSoftClassPtr<UUnitSpawner> BP_UUnitSpawnerClass;

	UPROPERTY(EditAnywhere)
		TSoftClassPtr<UMapGenerator> MapGeneratorClass;

	UFUNCTION()
		AObjectNetworkSyncronizer* SpawnSynchronizerForPlayer(APlayerController* Player);

	UFUNCTION(BlueprintPure)
		AObjectNetworkSyncronizer* GetSynchronizerForPlayer(APlayerController* Player);

	UPROPERTY(BlueprintAssignable)
		FEvent_InGameGameMode_ObjectNetworkSyncronizer_InGamePlayerController OnSynchronizerSpawned;

	UFUNCTION()
		void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;

	UFUNCTION()
	void SpawnBaseUnitsForPlayers();
private:
	UFUNCTION()
		void OnReceivePlayersStart(UMapGenerator *MapGenerator);
private:
	AInGamePlayerController* Player1 = nullptr;
	AInGamePlayerController* Player2 = nullptr;
	AObjectNetworkSyncronizer* Synchronizer1 = nullptr;
	AObjectNetworkSyncronizer* Synchronizer2 = nullptr;

protected:
	UFUNCTION()
		void OnPlayerSynchronized(AObjectNetworkSyncronizer* Synchronizer);

private:
	UPROPERTY()
		UStartingPlayerActors* playerActors;

	UPROPERTY()
		UMapGenerator *MG;

	UPROPERTY()
		UUnitSpawner *US;

	bool TwoPlayersConnected = false;
	bool PlayerStartGathered = false;
};
