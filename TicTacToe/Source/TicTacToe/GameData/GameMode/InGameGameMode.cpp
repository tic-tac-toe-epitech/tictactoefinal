// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameGameMode.h"
#include "InGame/MapGenerator/MapGenerator.h"
#include "GameData/Network/MatchmakingManager.h"
#include "GameData/Network/MatchmakingServerManager.h"
#include "InGame/Unit/Unit.h"
#include "InGame/Building/Building.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/PlayerState.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "GameData/UnitSpawner/UnitSpawner.h"
#include "GameData/UnitSpawner/StartingPlayerActors.h"
#include "TicTacToe.h"
#include "Components/CapsuleComponent.h"
#include "GameData/Network/ObjectNetworkSyncronizer.h"
#include "GameData/Controller/InGamePlayerController.h"
#include "GameData/GameInstance/TicTacGameInstance.h"
#include "GameData/GameState/InGameGameState.h"

void AInGameGameMode::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(TicTacLogInGameInitialization, Log,
		TEXT("AInGameGameMode::BeginPlay: %d - BeginPlay."),
		InitIndex++);
}

void AInGameGameMode::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchIsWaitingToStart();

	this->playerActors = NewObject<UStartingPlayerActors>();
	US = NewObject<UUnitSpawner>(this, BP_UUnitSpawnerClass.LoadSynchronous());
	MG = NewObject<UMapGenerator>(this, MapGeneratorClass.LoadSynchronous());
	if (MG)
	{
		MG->Init(US);
		MG->GenerateMap(this, GetRootComponent());

		if (this->GetWorld()->GetNetMode() == NM_DedicatedServer) {
			this->mmServerManager = NewObject<UMatchmakingServerManager>();
			this->mmServerManager->Init(this->GetWorld());
		}
	}
	else
	{
		UE_LOG(TicTacLogMapGeneration, Fatal,
			TEXT("AInGameGameMode::BeginPlay: Map Generator class is null. Map generation cannot occur and game cannot start."));
	}
	UE_LOG(TicTacLogInGameInitialization, Log,
		TEXT("AInGameGameMode::HandleMatchIsWaitingToStart: %d - HandleMatchIsWaitingToStart."),
		InitIndex++);
}

bool AInGameGameMode::ReadyToStartMatch_Implementation()
{
	return (Synchronizer1 && Synchronizer2 && Synchronizer1->IsSynchronized() && Synchronizer2->IsSynchronized());
}

void AInGameGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	UTicTacGameInstance::RemoveLoadingScreenWithoutContext(this);
	UE_LOG(TicTacLogInGameInitialization, Log,
		TEXT("AInGameGameMode::HandleMatchHasStarted: %d - HandleMatchHasStarted."),
		InitIndex++);
}

void AInGameGameMode::OnReceivePlayersStart(UMapGenerator * MapGenerator)
{
	MapGenerator->OnPlayerStartGathered.RemoveDynamic(this, &AInGameGameMode::OnReceivePlayersStart);
	PlayerStartGathered = true;
	if (TwoPlayersConnected)
	{
		SpawnBaseUnitsForPlayers();
	}
}

AUnit* AInGameGameMode::SpawnNewUnit(TSoftClassPtr<AUnit> UnitClass, APlayerController* OwningPlayer, FTransform UnitTransform)
{
 	AUnit* NewUnit = nullptr;
	FActorSpawnParameters params;
	if (OwningPlayer)
	{
		params.Owner = OwningPlayer->GetPlayerState<APlayerState>();
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		NewUnit = GetWorld()->SpawnActor<AUnit>(UnitClass.LoadSynchronous(), UnitTransform, params);

		// Ensure the unit does not get stuck into the ground.
		NewUnit->SetActorLocation(NewUnit->GetActorLocation() + FVector(0, 0, NewUnit->FindComponentByClass<UCapsuleComponent>()->GetScaledCapsuleHalfHeight()));
	}

	return NewUnit;
}

ABuilding* AInGameGameMode::SpawnNewBuilding(TSoftClassPtr<ABuilding> BuildingClass, APlayerController* OwningPlayer, FTransform BuildingTransform)
{
	ABuilding* NewBuilding = nullptr;
	FActorSpawnParameters params;
	if (OwningPlayer)
	{
		params.Owner = OwningPlayer->GetPlayerState<APlayerState>();
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		NewBuilding = GetWorld()->SpawnActor<ABuilding>(BuildingClass.LoadSynchronous(), BuildingTransform, params);
	}
	return NewBuilding;
}

void AInGameGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);

	if (Player1 == nullptr)
	{
		Player1 = Cast<AInGamePlayerController>(NewPlayer);
		Synchronizer1 = SpawnSynchronizerForPlayer(NewPlayer);
	}
	else
	{
		Player2 = Cast<AInGamePlayerController>(NewPlayer);
		Synchronizer2 = SpawnSynchronizerForPlayer(NewPlayer);
		
		TwoPlayersConnected = true;
		SpawnBaseUnitsForPlayers();
		Synchronizer1->Activate();
		Synchronizer2->Activate();
	}
}

AObjectNetworkSyncronizer* AInGameGameMode::SpawnSynchronizerForPlayer(APlayerController* Player)
{
	FActorSpawnParameters params;

	params.Owner = Player;
	AObjectNetworkSyncronizer* synchronizer = GetWorld()->SpawnActor<AObjectNetworkSyncronizer>(AObjectNetworkSyncronizer::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, params);
	synchronizer->OnClientSyncronized.AddDynamic(this, &AInGameGameMode::OnPlayerSynchronized);
	
	synchronizer->RegisterActor(Player);
	synchronizer->RegisterActor(Player->GetPlayerState<AInGamePlayerState>());
	synchronizer->RegisterActor(GetGameState<AInGameGameState>());

	OnSynchronizerSpawned.Broadcast(this, synchronizer, Cast<AInGamePlayerController>(Player));
	return synchronizer;
}

AObjectNetworkSyncronizer* AInGameGameMode::GetSynchronizerForPlayer(APlayerController* Player)
{
	AObjectNetworkSyncronizer* ret = nullptr;
	if (Player)
	{
		if (Player == Player1)
		{
			ret = Synchronizer1;
		}
		else if (Player == Player2)
		{
			ret = Synchronizer2;
		}
	}
	return ret;
}

void AInGameGameMode::OnPlayerSynchronized(AObjectNetworkSyncronizer* Synchronizer)
{
	Synchronizer->OnClientSyncronized.RemoveDynamic(this, &AInGameGameMode::OnPlayerSynchronized);
	if (Synchronizer1 && Synchronizer2 && Synchronizer1->IsSynchronized() && Synchronizer2->IsSynchronized())
	{
		StartMatch();
	}
}

void AInGameGameMode::SpawnBaseUnitsForPlayers()
{
	playerActors = MG->GetArrayPlayersStartingActors();
	playerActors->player1 = Player1;
	playerActors->player2 = Player2;
	MG->InitBaseAreasOwnership(GetRootComponent(), playerActors);
	MG->SpawnPlayerStartAssets(GetRootComponent(), playerActors);
}
