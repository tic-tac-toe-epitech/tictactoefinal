// Fill out your copyright notice in the Description page of Project Settings.


#include "TicTacGameInstance.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "MoviePlayer.h"

void UTicTacGameInstance::Init()
{
	Super::Init();

	if (GIsEditor)
	{
		DisplayLoadingScreen();
	}
}

void UTicTacGameInstance::Shutdown()
{
	Super::Shutdown();
}

void UTicTacGameInstance::DisplayLoadingScreenWithoutContext(UObject* WorldContextObject)
{
	UTicTacGameInstance* gameInstance = Cast<UTicTacGameInstance>(UGameplayStatics::GetGameInstance(WorldContextObject));

	if (gameInstance)
	{
		gameInstance->DisplayLoadingScreen();
	}
}

void UTicTacGameInstance::RemoveLoadingScreenWithoutContext(UObject* WorldContextObject)
{
	UTicTacGameInstance* gameInstance = Cast<UTicTacGameInstance>(UGameplayStatics::GetGameInstance(WorldContextObject));

	if (gameInstance)
	{
		gameInstance->RemoveLoadingScreen();
	}
}

void UTicTacGameInstance::DisplayLoadingScreen()
{
	if (!LoadingScreen)
	{
		if (LoadingScreenWidgetClass.IsNull())
		{
			return;
		}
		LoadingScreen = NewObject<UUserWidget>(this, LoadingScreenWidgetClass.LoadSynchronous());
		if (LoadingScreen)
		{
			LoadingScreen->AddToViewport();
		}
	}
}

void UTicTacGameInstance::RemoveLoadingScreen()
{
	//if (LoadingScreen)
	//{
	//	LoadingScreen->RemoveFromViewport();
	//	LoadingScreen = nullptr;
	//}
	if (GetMoviePlayer())
	{
		GetMoviePlayer()->Shutdown();
	}
}
