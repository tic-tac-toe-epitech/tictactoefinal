// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TicTacGameInstance.generated.h"

class UUserWidget;

/**
 * 
 */
UCLASS()
class TICTACTOE_API UTicTacGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	virtual void Init() override;
	virtual void Shutdown() override;

#pragma region Loading screen

protected:
	UPROPERTY(EditAnywhere)
		TSoftClassPtr<UUserWidget> LoadingScreenWidgetClass;

	UPROPERTY()
		UUserWidget* LoadingScreen;

public:
	UFUNCTION(BlueprintCallable)
		void DisplayLoadingScreen();

	UFUNCTION(BlueprintCallable)
		void RemoveLoadingScreen();

	//Statics for easy calls.
	UFUNCTION(BlueprintCallable)
		static void DisplayLoadingScreenWithoutContext(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable)
		static void RemoveLoadingScreenWithoutContext(UObject* WorldContextObject);

#pragma endregion Loading screen
};
