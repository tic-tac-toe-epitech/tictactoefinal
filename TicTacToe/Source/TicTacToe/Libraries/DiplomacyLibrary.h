// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "InGame/Diplomacy/DiplomacyInclude.h"
#include "DiplomacyLibrary.generated.h"

class APlayerState;
class UPlayer;
class AInGamePlayerController;
class AInGamePlayerState;

/**
 * 
 */
UCLASS()
class TICTACTOE_API UDiplomacyLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure)
		static EDiplomacy GetDiplomacyWithPlayerController(AActor* Actor, APlayerController* Player);

	UFUNCTION(BlueprintPure)
		static EDiplomacy GetDiplomacyWithPlayerState(AActor* Actor, APlayerState* Player);

	UFUNCTION(BlueprintPure)
		static EDiplomacy GetDiplomacyWithPlayer(AActor* Actor, UPlayer* Player);

	UFUNCTION(BlueprintPure)
		static EDiplomacy GetDiplomacyWithActor(AActor* Actor, AActor* OtherActor);

	UFUNCTION(BlueprintPure)
		static EDiplomacy GetDiplomacyWithLocalPlayer(AActor* Actor);

	// Will return a valid controller only on server or if it is the local controller. Returns null if not owned or owned by another player.
	UFUNCTION(BlueprintPure)
		static AInGamePlayerController* GetOwningController(AActor* Actor);

	// Will return null if not owned by anyone.
	UFUNCTION(BlueprintPure)
		static AInGamePlayerState* GetOwningPlayerState(AActor* Actor);
};
