// Fill out your copyright notice in the Description page of Project Settings.


#include "RecruitAction.h"
#include "Libraries/DiplomacyLibrary.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "ActionsManagerComponent.h"
#include "InGame/Unit/UnitSpawnComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameData/GameState/InGameGameState.h"

bool URecruitAction::isRunnable_Implementation() const
{
	if (UnitTag.IsValid() && _ActionsManager)
	{
		AInGamePlayerState* playerState = UDiplomacyLibrary::GetOwningPlayerState(_ActionsManager->GetOwner());

		if (playerState && !playerState->IsPopulationFull())
		{

			return Super::isRunnable_Implementation();
		}
	}
	return false;
}

bool URecruitAction::run_Implementation()
{
	if (Super::run_Implementation())
	{
		if (UnitTag.IsValid() && _ActionsManager)
		{
			AInGamePlayerState* playerState = UDiplomacyLibrary::GetOwningPlayerState(_ActionsManager->GetOwner());

			if (playerState && !playerState->IsPopulationFull())
			{
				UUnitSpawnComponent* USC = _ActionsManager->GetOwner()->FindComponentByClass<UUnitSpawnComponent>();
				if (USC)
				{
					USC->SpawnUnitWithTag(UnitTag);
					return true;
				}
			}
		}
	}
	return false;
}

float URecruitAction::getTimer_Implementation() const
{
	if (_ActionsManager && UnitTag.IsValid())
	{
		AInGameGameState* gameState = Cast<AInGameGameState>(UGameplayStatics::GetGameState(_ActionsManager));

		if (gameState)
		{
			const FUnitStruct& unitStruct = FUnitsStruct::GetUnitData(gameState->GetUnitsData(), UnitTag.ToString());
			return unitStruct._HiringTime;
		}
	}
	return Super::getTimer_Implementation();
}

FActionConditionStruct URecruitAction::getCondition_Implementation() const
{
	AInGameGameState* gameState = Cast<AInGameGameState>(UGameplayStatics::GetGameState(_ActionsManager));
	if (_ActionsManager && gameState)
	{
		FActionConditionStruct condition = _Conditions;

		condition._Price = FUnitsStruct::GetUnitData(gameState->GetUnitsData(), UnitTag.ToString())._Price;
		return condition;
	}
	return Super::getCondition_Implementation();
}
