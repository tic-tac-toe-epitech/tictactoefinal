// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TicTacToe/InGame/Action/ActionCondition.h"
#include "Engine/Texture2D.h"
#include "UObject/SoftObjectPtr.h"
#include "UObject/NoExportTypes.h"
#include "Engine/EngineTypes.h"
#include "UAction.generated.h"

class UActionsManagerComponent;
/**
 * @class UUAction
 * @brief Defining what is a action.
 */
UCLASS(abstract, BlueprintType, Blueprintable)
class TICTACTOE_API UAction : public UObject
{
	GENERATED_BODY()
public:
	UAction();

	protected:
		// Gameplay tag defining the action name.
		UPROPERTY(EditAnywhere)
		FGameplayTag				_Tag;
		// Action description.
		UPROPERTY(EditAnywhere)
		FText						_Description;
		// Action icon.
		UPROPERTY(EditAnywhere)
		TSoftObjectPtr<UTexture2D>	_Icon;
		// Conditions allowing the action. Whithout thoses conditions, the action is not runnable.
		UPROPERTY(EditAnywhere)
		FActionConditionStruct		_Conditions;

	protected:
		// Time to wait before the action can be run.
		UPROPERTY(EditAnywhere)
		float						_WaitingTime;

		// The action manager responsible for this action.
		UPROPERTY(BlueprintReadOnly)
		UActionsManagerComponent* _ActionsManager;

	public:
		bool	operator==(const UAction &action) {	return (_Tag == action.getTag()); }

	public:
		//!
		//! @brief Initialize the action tag name.
		//! @param actionTag Tag name. Must be a existing tag.
		//!
		UFUNCTION(BlueprintCallable)
		void	setTag(const FGameplayTag &actionTag) { if (actionTag.IsValid()) _Tag = FGameplayTag(actionTag); }
		//!
		//! @brief Initialize the action description.
		//! @param actionDescription Action description.
		//!
		UFUNCTION(BlueprintCallable)
		void	setDescription(const FText &actionDescription) { _Description = actionDescription; }
		//!
		//! @brief Initialize the action conditions.
		//! @param actionConditions Action conditions.
		//!
		UFUNCTION(BlueprintCallable)
		void	setConditions(const FActionConditionStruct &actionConditions) { _Conditions = actionConditions; }
		//!
		//! @brief Extract the tag name.
		//! @return Action tag name.
		//!
		UFUNCTION(BlueprintPure)
		const FGameplayTag				&getTag() const { return (_Tag); }
		//!
		//! @brief Extract the action description.
		//! @return Action description.
		//!
		UFUNCTION(BlueprintPure)
		const FText						&getDescription() const { return (_Description);  }
		//!
		//! @brief Extract the action conditions.
		//! @return Action conditions.
		//!
		UFUNCTION(BlueprintPure, BlueprintNativeEvent)
			FActionConditionStruct getCondition() const;
		virtual FActionConditionStruct getCondition_Implementation() const { return (_Conditions); }
		//!
		//! @brief Extract the action icon.
		//! @return Icon soft class pointer.
		//!
		UFUNCTION(BlueprintPure)
		const TSoftObjectPtr<UTexture2D>	&getIcon() const { return (_Icon); }
		//!
		//! @brief Load a action icon.
		//! @return Icon pointer.
		//!
		UFUNCTION(BlueprintPure)
		UTexture2D						*getLoadedIcon() const { return ((UTexture2D *)_Icon.LoadSynchronous()); }

	public:
		//!
		//! @brief Initialize the timer value.
		//! @param time Float time value.
		//!
		UFUNCTION(BlueprintCallable)
		void	setTimer(float time) { _WaitingTime = time; }
		//!
		//! @brief Extract the timer value.
		//! @return Float time value.
		//!
		UFUNCTION(BlueprintNativeEvent, BlueprintPure)
		float	getTimer() const;
		virtual float	getTimer_Implementation() const { return (_WaitingTime); }
		//!
		//! @brief Tell if to run this action, wait is needed.
		//! @return true : Need to wait, false : no need to wait.
		//!
		UFUNCTION(BlueprintCallable)
		bool	needToWait() const { return (getTimer() != 0);  }

	public:
		//!
		//! @brief Compute if the action is runnable. This method must be develop for each action.
		//! @return Status, runnable or not.
		//!
		UFUNCTION(BlueprintNativeEvent)
		bool			isRunnable() const;
		virtual bool	isRunnable_Implementation() const;
		//!
		//! @brief Run the action, this function contain the action logic. This method must be develop for each action.
		//! @return Status, runnable or not.
		//!
		UFUNCTION(BlueprintNativeEvent)
		bool			run();
		virtual bool	run_Implementation();

		UFUNCTION(BlueprintPure)
			bool ShouldRunOnClient() { return ShouldAlsoRunOnClient; }

		UFUNCTION(BlueprintNativeEvent)
			void RunOnClient();
		virtual void RunOnClient_Implementation() {}

	protected:
		UPROPERTY(EditAnywhere)
			bool ShouldAlsoRunOnClient = false;
};
