// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TicTacToe/InGame/Action/UAction.h"
#include "TicTacToe/InGame/Action/FActionClassStruct.h"
#include "Engine/DataTable.h"
#include "Components/ActorComponent.h"
#include "TimerManager.h"
#include "Engine/EngineTypes.h"
#include "ActionsManagerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FEvent_ActionsManager_Action_Float, UActionsManagerComponent*, ActionsManager, UAction*, Action, float, Delay);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TICTACTOE_API UActionsManagerComponent : public UActorComponent
{
	GENERATED_BODY()

	private:
		// Stock all FActionClassStruct.
		UPROPERTY(EditAnywhere)
		UDataTable				*_ActionClassDataTable;
		// List of all tags available already set with UE UI.
		UPROPERTY(EditAnywhere)
		FGameplayTagContainer	_AvailableActionsTag;
		// List of all available action.
		UPROPERTY()
		TArray<UAction*>		_Actions;
		// Action stack. All actions in this stack should be run in order according all conditions.
		UPROPERTY()
		TArray<UAction*>		_ActionsStack;
		// Current action working on.
		UPROPERTY()
		UAction					*_CurrentAction;

	private:
		// Unique handle that can be used to distinguish timers that have identical delegates.
		UPROPERTY(EditAnywhere)
		FTimerHandle			_TimerHandle;

	public:	
		// Sets default values for this component's properties
		UActionsManagerComponent();

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;

	public:	
		// Called every frame
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	public:
		//!
		//! @brief Add a action to the action manager.
		//! @param tag Action tag to add.
		//! @return true -> The action was found and added.
		//!			false -> The action was not found.
		//!
		UFUNCTION(BlueprintCallable)
		bool   add(const FGameplayTag	&tag);
		//!
		//! @brief Remove a action to the action manager.
		//! @param tag Action tag to remove.
		//! @return true -> The action was found and removed.
		//!			false -> The action was not found.
		//!
		UFUNCTION(BlueprintCallable)
		bool   remove(const FGameplayTag &tag);

	public:
		//!
		//! @brief Stack the action in parameters and run all the stack in stacking order.
		//! @param actionTag Tag of the action to stack.
		//!
		UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
		void	runActionsToServer(const FGameplayTag &actionTag);
		void	runActionsToServer_Implementation(const FGameplayTag &actionTag);
		bool	runActionsToServer_Validate(const FGameplayTag &) { return (true); }
		//!
		//! @brief Execute a action from the client side. The action will be ordered to the server after the timer done.
		//! @param actionTag Tag of the action to stack.
		//! @return true: Action found and ordered to the server, false : no action found or invalid timer.
		//!
		UFUNCTION(BlueprintCallable)
		void	run(const FGameplayTag &actionTag);

	public:
		//!
		//! @brief Extract all available actions tags. Available does not mean runnable.
		//! @return A unmodifiable reference of all available actions tags.
		//!
		UFUNCTION(BlueprintPure)
		const FGameplayTagContainer	&getAvailableActionTags() const { return (_AvailableActionsTag);  }
		//!
		//! @brief Extract all runnnable actions tags. Runnable does not mean that this action will be run.
		//! @return Copy of all runnable action tags.
		//!
		UFUNCTION(BlueprintPure)
		FGameplayTagContainer		getRunnableActionTags() const;
		//!
		//! @brief Extract all stacked actions tags. Stacked mean that all next actions will be run.
		//! @return Copy of all stacked actions tags in a specific time.
		//!
		UFUNCTION(BlueprintPure)
		FGameplayTagContainer		getStackedActionTags() const;
		//!
		//! @brief Extract all available actions. Available does not mean runnable.
		//! @return A unmodifiable reference of all available actions.
		//!
		UFUNCTION(BlueprintPure)
		const TArray<UAction*>		&getAvailableAction() const { return (_Actions); }
		//!
		//! @brief Extract all runnnable actions. Runnable does not mean that this action will be run.
		//! @return Copy of all runnable actions.
		//!
		UFUNCTION(BlueprintPure)
		TArray<UAction*>			getRunnableAction() const;
		//!
		//! @brief Extract all stacked actions. Stacked mean that all next actions will be run.
		//! @return A unmodifiable reference of all stacked actions in a specific time.
		//!
		UFUNCTION(BlueprintPure)
		const TArray<UAction*>		&getStackedAction() const { return (_ActionsStack); }
		//!
		//! @brief Extract the current action.
		//! @return Current action.
		//!
		UFUNCTION(BlueprintPure)
		UAction						*getCurrentAction() const { return (_CurrentAction); }
		//!
		//! @brief Extract the action to run.
		//! @return Next action or nullptr if not existing.
		//!
		UFUNCTION(BlueprintPure)
		UAction						*getNextRunAction() const { return ( _ActionsStack.Num() <= 0 ? nullptr : _ActionsStack[0]); }


	public:
		UPROPERTY(BlueprintAssignable)
			FEvent_ActionsManager_Action_Float OnTimedActionStarted;

	private:
		//!
		//! @brief	Run the current action stack. This mean that the next valid action of the stack will be run.
		//!			The current action will be remove from the handle action (and stack) after the call.
		//! @return 0 -> A action has been run with success.
		//!			1 -> A action has been run with error.
		//!			2 -> No action to run has been found.
		//!			3 -> A action is currently running, impossible to run a new action.
		//! @warning If the timer is set, this is impossible to have the action status. The status will alway be 0.
		//!
		uint8	runCurrentStack();
		//!
		//! @brief Stack a action to the action stack.
		//! @param actionTag Tag of the action to stack.
		//! @return true -> The action has been stacked.
		//!			false -> The action was not found, impossible to stack the action.
		//!
		bool	stackThisAction(const FGameplayTag &actionTag);
		//!
		//! @brief Give to the order to the server to run the action and go to the next action on the stack.
		//! @param actionTag Tag of the action to stack.
		//!
		void	runActionToServerAndGoToNextAction();
		//!
		//! @brief Remove ressources after a action has been run.
		//! @param action Action that was run before.
		//!
		void	removeRessources(const UAction *action) const;

	public:
		//!
		//! @brief	Initialize the action manager.
		//!			Extract all data from global projet setting.
		//!
		UFUNCTION(BlueprintCallable)
		void   init();

	private:
		UPROPERTY(EditAnywhere)
			bool AutoInit = true;

		bool IsInitialized = false;
		//!
		//! @brief Extract a action pointer according a gameplay tag.
		//! @return UAction pointer, if not found nullptr is returned.
		//!
		UAction	*getAction(const FGameplayTag &actionTag) const;
};
