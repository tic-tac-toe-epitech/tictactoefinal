// Fill out your copyright notice in the Description page of Project Settings.

#include "TicTacToe/InGame/Action/UAction.h"
#include "ActionsManagerComponent.h"
#include "Libraries/DiplomacyLibrary.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "InGame/ResourcesManager/ResourcesManagerComponent.h"

UAction::UAction()
{
	_ActionsManager = Cast<UActionsManagerComponent>(GetOuter());
	if (!_ActionsManager)
	{
		UE_LOG(TicTacLogActionsManager, Warning,
			TEXT("UAction::UAction: Outer object is not of type 'UActionsManagerComponent'. '%s' may not work properly."),
			*GetNameSafe(this));
	}
}

bool	UAction::isRunnable_Implementation() const
{
	if (_ActionsManager)
	{
		AInGamePlayerState* playerState = UDiplomacyLibrary::GetOwningPlayerState(_ActionsManager->GetOwner());

		if (playerState)
		{
			return (playerState->GetPlayerResources()->HasRessourcesAvailableWithStruct(getCondition()._Price));
		}
	}
	return (true);
}

bool	UAction::run_Implementation()
{
	return true;
}