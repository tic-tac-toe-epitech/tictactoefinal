// Fill out your copyright notice in the Description page of Project Settings.

#include "TicTacToe/InGame/Action/ActionsManagerComponent.h"
#include "TicTacToe/InGame/Action/FActionClassStruct.h"
#include "TicTacToe/InGame/ResourcesManager/ResourcesManagerComponent.h"
#include "TicTacToe/InGame/Action/ActionCondition.h"
#include "TicTacToe/GameData/PlayerState/InGamePlayerState.h"
#include "Libraries/DiplomacyLibrary.h"
#include "TimerManager.h"

// Sets default values for this component's properties
UActionsManagerComponent::UActionsManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	bReplicates = true;
	// ...
}

// Called when the game starts
void UActionsManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	if (AutoInit)
	{
		init();
	}
	// ...
	
}

// Called every frame
void UActionsManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool   UActionsManagerComponent::add(const FGameplayTag	&tag)
{
	FActionClassStruct *action;

	action = _ActionClassDataTable->FindRow<FActionClassStruct>(tag.GetTagName(), "");
	if (action != nullptr && !action->_Action.IsNull())
	{
		UAction		*runAction = NewObject<UAction>(this, action->_Action.LoadSynchronous());

		if (runAction->getTag() == tag)
		{
			_Actions.Add(runAction);
			return (true);
		}
		return (false);
	}
	return (false);
}

bool	UActionsManagerComponent::remove(const FGameplayTag &tag)
{
	for (UAction* action : _Actions)
	{
		if (action->getTag() == tag)
		{
			_Actions.Remove(action);
			return (true);
		}
	}
	return (false);
}

void	UActionsManagerComponent::runActionsToServer_Implementation(const FGameplayTag &actionTag)
{
	UAction		*action = getAction(actionTag);

	if (action == nullptr)
		return ;
	action->run();
	removeRessources(action);
}

void	UActionsManagerComponent::run(const FGameplayTag &actionTag)
{
	if (stackThisAction(actionTag))
		runCurrentStack();
}

FGameplayTagContainer		UActionsManagerComponent::getRunnableActionTags() const
{
	FGameplayTagContainer	tags;

	for (UAction *action : _Actions)
		if (action->isRunnable())
			tags.AddTag(action->getTag());
	return (tags);
}

FGameplayTagContainer		UActionsManagerComponent::getStackedActionTags() const
{
	FGameplayTagContainer	tags;

	for (UAction *action : _ActionsStack)
		tags.AddTag(action->getTag());
	return (tags);
}

TArray<UAction*>			UActionsManagerComponent::getRunnableAction() const
{
	TArray<UAction*>	runnableAction;

	for (UAction *action : _Actions)
		if (action->isRunnable())
			runnableAction.Add(action);
	return (runnableAction);
}

uint8	UActionsManagerComponent::runCurrentStack()
{
	if (_CurrentAction != nullptr)
		return (3);
	if (_ActionsStack.Num() <= 0)
		return (2);
	_CurrentAction = _ActionsStack[0];
	_ActionsStack.RemoveAt(0);
	if (_CurrentAction == nullptr)
		return (2);

	FGameplayTag actionTag = _CurrentAction->getTag();

	if (_CurrentAction->needToWait())
	{
		UWorld *world = this->GetWorld();

		if (world == nullptr)
			return (1);

		float delay = _CurrentAction->getTimer();
		world->GetTimerManager().SetTimer(_TimerHandle, this, &UActionsManagerComponent::runActionToServerAndGoToNextAction, delay);
		OnTimedActionStarted.Broadcast(this, _CurrentAction, delay);
		return (0);
	}
	if (_CurrentAction->ShouldRunOnClient())
	{
		_CurrentAction->RunOnClient();
	}
	runActionsToServer(actionTag);
	_CurrentAction = nullptr;
	runCurrentStack();
	return (0);
}

bool	UActionsManagerComponent::stackThisAction(const FGameplayTag &actionTag)
{
	UAction	*action = getAction(actionTag);

	if (action == nullptr || !action->isRunnable())
		return (false);
	_ActionsStack.Add(action);
	return (true);
}

void	UActionsManagerComponent::runActionToServerAndGoToNextAction()
{
	if (_CurrentAction)
	{
		runActionsToServer(_CurrentAction->getTag());
		_CurrentAction = nullptr;
	}
	runCurrentStack();
}

void	UActionsManagerComponent::removeRessources(const UAction *action) const
{
	AInGamePlayerState	*playerState = UDiplomacyLibrary::GetOwningPlayerState(GetOwner());

	if (playerState != nullptr)
	{
		UResourcesManagerComponent	*ressourcesManager = playerState->GetPlayerResources();
		FPriceStruct				prices = action->getCondition()._Price;

		if (ressourcesManager != nullptr)
		{
			ressourcesManager->TakeGold(prices._Gold);
			ressourcesManager->TakeSteel(prices._Steel);
			ressourcesManager->TakeWood(prices._Wood);
		}
	}
}

void   UActionsManagerComponent::init()
{
	if (!IsInitialized)
	{
		FActionClassStruct* action;

		_CurrentAction = nullptr;
		for (FGameplayTag tag : _AvailableActionsTag)
		{
			action = _ActionClassDataTable->FindRow<FActionClassStruct>(tag.GetTagName(), "");
			if (action != nullptr && !action->_Action.IsNull())
			{
				UAction* runAction = NewObject<UAction>(this, action->_Action.LoadSynchronous());

				if (runAction->getTag() == tag)
					_Actions.Add(runAction);
			}
		}
		IsInitialized = true;
	}
}

UAction	*UActionsManagerComponent::getAction(const FGameplayTag &actionTag) const
{
	for (UAction *action : _Actions)
	{
		if (action->getTag() == actionTag)
			return (action);
	}
	return (nullptr);
}