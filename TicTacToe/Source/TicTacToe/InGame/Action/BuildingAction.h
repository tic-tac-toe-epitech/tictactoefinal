#pragma once

#include "CoreMinimal.h"
#include "InGame/Action/UAction.h"
#include "GameplayTagContainer.h"
#include "BuildingAction.generated.h"

UCLASS()
class TICTACTOE_API UBuildingAction : public UAction
{
	GENERATED_BODY()

public:
	UBuildingAction();

	protected:
		UPROPERTY(EditAnywhere)
		FGameplayTag	_BuildingTag;

	public:
		virtual bool	isRunnable_Implementation() const override { return (_BuildingTag.IsValid() && Super::isRunnable_Implementation()); }

		virtual FActionConditionStruct	getCondition_Implementation() const override;

protected:
	virtual void RunOnClient_Implementation() override;

};
