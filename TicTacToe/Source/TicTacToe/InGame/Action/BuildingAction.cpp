#include "TicTacToe/InGame/Action/BuildingAction.h"
#include "TicTacToe/InGame/Action/ActionsManagerComponent.h"
#include "TicTacToe/GameData/GameState/InGameGameState.h"
#include "TicTacToe/GameData/Controller/InGamePlayerController.h"
#include "Libraries/DiplomacyLibrary.h"
#include "Kismet/GameplayStatics.h"

UBuildingAction::UBuildingAction()
{
	ShouldAlsoRunOnClient = true;
}

void UBuildingAction::RunOnClient_Implementation()
{
	AInGamePlayerController* localController = Cast<AInGamePlayerController>(UGameplayStatics::GetPlayerController(this, 0));

	if (localController && UDiplomacyLibrary::GetDiplomacyWithPlayerController(_ActionsManager->GetOwner(), localController) == EDiplomacy::OWNED)
	{
		localController->ActivateConstructionMode(_BuildingTag);
	}
}

FActionConditionStruct UBuildingAction::getCondition_Implementation() const
{
	AInGameGameState	*gameState = Cast<AInGameGameState>(UGameplayStatics::GetGameState(_ActionsManager));

	if (!_ActionsManager || !gameState)
		return (Super::getCondition_Implementation());

	FActionConditionStruct	condition = _Conditions;

	condition._Price = FBuildingsStruct::GetBuildingData(gameState->GetBuildingsData(), _BuildingTag.ToString())._Price;
	return (condition);
}
