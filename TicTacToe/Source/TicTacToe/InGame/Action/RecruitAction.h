// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InGame/Action/UAction.h"
#include "GameplayTagContainer.h"
#include "RecruitAction.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API URecruitAction : public UAction
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere)
		FGameplayTag UnitTag;

public:
	virtual bool	isRunnable_Implementation() const override;
	virtual bool	run_Implementation() override;
	virtual float	getTimer_Implementation() const;
	virtual FActionConditionStruct getCondition_Implementation() const override;

private:
	bool HasReadCostData = false;
};
