#pragma once

#include "CoreMinimal.h"
#include "UObject/SoftObjectPtr.h"
#include "Engine/DataTable.h"
#include "TicTacToe/InGame/Action/UAction.h"
#include "FActionClassStruct.generated.h"

//!
//! @struct FActionClassStruct
//! @brief	Structure containing only a soft pointer to a action.
//!			This pointer is into a struct to enable the UDataTable use.
//!
USTRUCT(BlueprintType)
struct FActionClassStruct : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	public:
		UPROPERTY(EditAnywhere)
		TSoftClassPtr<UAction>	_Action;
};