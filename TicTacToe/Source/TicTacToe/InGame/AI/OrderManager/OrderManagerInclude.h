#pragma once

#include "CoreMinimal.h"
#include "OrderManagerInclude.generated.h"

UENUM(BlueprintType, Blueprintable)
enum class EOrderType : uint8
{
	None,
	Move,
	Collect,
	Build,
	Attack,
};

USTRUCT(BlueprintType)
struct FOrderStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite)
		EOrderType OrderType = EOrderType::None;

	UPROPERTY(BlueprintReadWrite)
		FVector TargetLocation = FVector::ZeroVector;

	UPROPERTY(BlueprintReadWrite)
		AActor* TargetActor = nullptr;

	UPROPERTY(BlueprintReadWrite)
		bool bIsShift = false;
};

UENUM(BlueprintType, Blueprintable)
enum class EUnitState : uint8
{
	Moving,
	Collecting,
	Idle,
	Building,
};
