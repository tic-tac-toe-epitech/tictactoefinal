// Fill out your copyright notice in the Description page of Project Settings.


#include "OrderManagerComponent.h"
#include "Libraries/DiplomacyLibrary.h"
#include "TicTacToe.h"

// Sets default values for this component's properties
UOrderManagerComponent::UOrderManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	bReplicates = true;
	bAutoActivate = true;
	bAutoRegister = true;
	// ...

	AuthorizedOrderTypes.Add(EOrderType::Move);
	IsExecutingOrder = false;
}


// Called when the game starts
void UOrderManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UOrderManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UOrderManagerComponent::ExecuteNextOrder()
{
	if (IsExecutingOrder)
	{
		FinishExecuteCurrentOrder();
	}
	if (OrderQueue.Num())
	{
		IsExecutingOrder = true;
		OnOrderExectutionStarted.Broadcast(this, OrderQueue[0]);
	}
}

void UOrderManagerComponent::FinishExecuteCurrentOrder()
{
	if (IsExecutingOrder)
	{
		IsExecutingOrder = false;
		if (OrderQueue.Num())
		{
			OnOrderExectutionFinished.Broadcast(this, OrderQueue[0]);
			OrderQueue.RemoveAt(0);
		}
	}
}

FOrderStruct UOrderManagerComponent::GetCurrentOrder()
{
	if (OrderQueue.Num())
	{
		return OrderQueue[0];
	}
	return FOrderStruct();
}

void UOrderManagerComponent::AddOrder(FOrderStruct NewOrder)
{
	if (CanAddOrder(NewOrder))
	{
		if (!NewOrder.bIsShift)
		{
			ClearOrders();
		}
		bool shouldExecute = GetCurrentOrder().OrderType == EOrderType::None;

		OrderQueue.Add(NewOrder);
		UE_LOG(TicTacLogOrderManagement, Log,
			TEXT("UOrderManagerComponent::AddOrderInternal_Implementation: Order given to '%s'."),
			*GetNameSafe(this));
		AddOrderInternal(NewOrder);
		OnOrderGiven.Broadcast(this, NewOrder);

		if (shouldExecute)
		{
			ExecuteNextOrder();
		}
	}
}

bool UOrderManagerComponent::CanAddOrder_Implementation(FOrderStruct NewOrder)
{
	return true;
	//return AuthorizedOrderTypes.Contains(NewOrder.OrderType);
}


void UOrderManagerComponent::AddOrderInternal_Implementation(FOrderStruct NewOrder)
{
}

void UOrderManagerComponent::ClearOrders()
{
	if (GetCurrentOrder().OrderType != EOrderType::None)
	{
		FinishExecuteCurrentOrder();
		OrderQueue.Empty();
	}
}