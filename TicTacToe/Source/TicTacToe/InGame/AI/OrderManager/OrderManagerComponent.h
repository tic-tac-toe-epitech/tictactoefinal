// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InGame/AI/OrderManager/OrderManagerInclude.h"
#include "OrderManagerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_OrderManagerComponent_Order, UOrderManagerComponent*, OrderManager, const FOrderStruct&, Order);

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TICTACTOE_API UOrderManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOrderManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UPROPERTY()
		TArray<FOrderStruct> OrderQueue;

	UPROPERTY()
		bool IsExecutingOrder;

	UPROPERTY(EditAnywhere)
		TArray<EOrderType> AuthorizedOrderTypes;

	UFUNCTION(BlueprintNativeEvent)
		void AddOrderInternal(FOrderStruct NewOrder);
	virtual void AddOrderInternal_Implementation(FOrderStruct NewOrder);

public:
	UFUNCTION(BlueprintNativeEvent)
		bool CanAddOrder(FOrderStruct NewOrder);
	virtual bool CanAddOrder_Implementation(FOrderStruct NewOrder);

	UFUNCTION(BlueprintCallable)
		void AddOrder(FOrderStruct NewOrder);

	UFUNCTION(BlueprintCallable)
		void ClearOrders();

	UFUNCTION(BlueprintPure)
		FOrderStruct GetCurrentOrder();

	UFUNCTION(BlueprintCallable)
		void ExecuteNextOrder();

	UFUNCTION(BlueprintCallable)
		void FinishExecuteCurrentOrder();

	UPROPERTY(BlueprintAssignable)
		FEvent_OrderManagerComponent_Order OnOrderGiven;

	UPROPERTY(BlueprintAssignable)
		FEvent_OrderManagerComponent_Order OnOrderExectutionStarted;

	UPROPERTY(BlueprintAssignable)
		FEvent_OrderManagerComponent_Order OnOrderExectutionFinished;
};
