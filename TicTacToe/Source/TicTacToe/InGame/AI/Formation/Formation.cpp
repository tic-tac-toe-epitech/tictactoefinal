// Fill out your copyright notice in the Description page of Project Settings.


#include "Formation.h"
#include "InGame/Unit/Unit.h"
#include "Components/ArrowComponent.h"
#include "DrawDebugHelpers.h"
#include "TicTacToe.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "UnrealNetwork.h"
#include "InGame/AI/OrderManager/OrderManagerComponent.h"
#include "NavigationSystem.h"
#include "NavigationPath.h"


DEFINE_LOG_CATEGORY(TicTacLogUnitFormation);

// Sets default values
AFormation::AFormation()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	IsMoving = false;
	HasArrived = false;
	Speed = -1;
	MinRunDistTolerance = 50;
	MaxRunDistTolerance = 800;
	DistanceThresholdForMaxRunSpeed = 200;
	StartPositionForwardThreshold = 200;
	MembersPerLine = 5;
	SpaceBetweenMembers = 100;
	SpaceBetweenLines = 140;

	bReplicates = false; // Set it to true for debug only (Draws debug path and points positions on clients).
	bReplicateMovement = true;
	bOnlyRelevantToOwner = false;
	bAlwaysRelevant = true;

	DebugArrow = CreateDefaultSubobject<UArrowComponent>("Debug Arrow");
	DebugArrow->SetIsReplicated(true);
	DebugArrow->SetHiddenInGame(false);
	SetRootComponent(DebugArrow);
}

void AFormation::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFormation, CurrentPathPoints);
	DOREPLIFETIME(AFormation, IsMoving);
	DOREPLIFETIME(AFormation, Points);
}

// Called when the game starts or when spawned
void AFormation::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFormation::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		if (IsMoving && CurrentPath && CurrentPathPoints.Num())
		{
			FVector resultPos = GetActorLocation() + GetActorForwardVector() * (DeltaTime * Speed);

			SetActorLocation(resultPos);

			if (GetActorLocation().Equals(CurrentPathPoints[0], 10))
			{
				CurrentPathPoints.RemoveAt(0);

				if (CurrentPathPoints.Num())
				{
					RotateFormation();
				}
			}
			if (!CurrentPathPoints.Num() || !Members.Num())
			{
				HandleArrival();
			}
		}
	}
	else
	{
		if (CurrentPathPoints.Num())
		{
			FVector prevLoc = GetActorLocation();
			for (auto it = CurrentPathPoints.CreateConstIterator(); it; it++)
			{
				FVector start = prevLoc + FVector(0, 0, 1);
				FVector dest = *it + FVector(0, 0, 1);
				DrawDebugLine(GetWorld(), start, dest, FColor::Green, false, -1, 0, 2);
				prevLoc = *it;
			}

			float dot = FVector::DotProduct(FVector::ForwardVector, GetActorForwardVector());
			float acos = FMath::Acos(dot);
			float angle = FMath::RadiansToDegrees(acos);

			// Angle is given between 0 and 180, so we bring it back to 180 and 360 if it is on the left side.
			float finalAngle = GetActorForwardVector().Y >= 0 ? angle : 360 - angle;
			UE_LOG(TicTacLogUnitFormation, VeryVerbose,
				TEXT("Forward = {%f, %f, %f} -- dot = %f -- acos = %f -- angle = %f -- final angle = %f."),
				GetActorForwardVector().X, GetActorForwardVector().Y, GetActorForwardVector().Z, dot, acos, angle, finalAngle);

			for (auto point : Points)
			{
				FVector pos = GetActorLocation() + point.RotateAngleAxis(finalAngle, FVector(0, 0, 1));

				FMatrix TM;
				TM.SetOrigin(pos);
				TM.SetAxis(0, FVector(1, 0, 0));
				TM.SetAxis(1, FVector(1, 0, 0));
				TM.SetAxis(2, FVector(0, 1, 0));
				DrawDebugCircle(GetWorld(), TM, 20, 50, FColor::Blue, false, -1, 0, 5, false);
				DrawDebugPoint(GetWorld(), pos, 5, FColor::Blue);
			}
		}
	}
}

void AFormation::AddMember(AUnit* NewMember, bool UpdateFormationData)
{
	if (NewMember && !Members.Contains(NewMember))
	{
		Members.Add(NewMember);
		NewMember->SetFormation(this);

		if (UpdateFormationData)
		{
			UpdateFormation();
		}
	}
}

void AFormation::RemoveMember(AUnit* Member, bool UpdateFormationData)
{
	if (Member && Members.Contains(Member))
	{
		Members.Remove(Member);
		Member->SetFormation(nullptr);

		if (UpdateFormationData)
		{
			UpdateFormation();
		}
	}
}

void AFormation::SetMembers(const TArray<AUnit*>& NewMembers, bool UpdateFormationData)
{
	Clear();

	for (auto unit : NewMembers)
	{
		if (unit)
		{
			AddMember(unit, false);
		}
	}
	if (UpdateFormationData)
	{
		UpdateFormation();
	}
}

void AFormation::Clear()
{
	while (Members.Num())
	{
		RemoveMember(Members[0], false);
	}
	MembersPositionMap.Empty();
	Points.Empty();
	CurrentPath = nullptr;
	CurrentPathPoints.Empty();
}

void AFormation::UpdateFormation()
{
	SetStartPosition();
	CurrentPath = UNavigationSystemV1::FindPathToLocationSynchronously(this, GetActorLocation(), Destination, this);

	if (CurrentPath && CurrentPath->PathPoints.Num() >= 2)
	{
		CurrentPathPoints = CurrentPath->PathPoints;

		RotateFormation();

		// IMPORTANT to do it after orientation, as orientation is used to determine where each member's place is.
		SetMembersPosition();

		// We replace the formation a bit further away forward, so that units immediately follow and avoid they just sit while the formation is behind them.
		//SetActorLocation(GetActorLocation() + GetActorForwardVector() * StartPositionForwardThreshold);


		SetSpeed();
	}
	else
	{
		UE_LOG(TicTacLogUnitFormation, Error,
			TEXT("AFormation::MoveToLocation_Implementation: Could not find any path from formation '%s' at position { %f, %f, %f } and destination { %f, %f, %f }."),
			*GetNameSafe(this), GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z, Destination.X, Destination.Y, Destination.Z);
	}
}

void AFormation::SetStartPosition()
{
	float x = 0;
	float y = 0;

	if (Members.Num())
	{
		for (AUnit* unit : Members)
		{
			x += unit->GetActorLocation().X;
			y += unit->GetActorLocation().Y;
		}
		x = x / Members.Num();
		y = y / Members.Num();
	}
	else
	{
		UE_LOG(TicTacLogUnitFormation, Warning,
			TEXT("Members are empty."));
	}
	SetActorLocation(FVector(x, y, 0));
	UE_LOG(TicTacLogUnitFormation, Warning,
		TEXT("Position %s positionned in { %f, %f, %f }"),
		*GetNameSafe(this), GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z);
}

void AFormation::RotateFormation()
{
	FVector currentDestination = Destination;

	if (CurrentPathPoints.Num())
	{
		currentDestination = CurrentPathPoints[0];
	}

	FVector dir = currentDestination - GetActorLocation();
	dir.Normalize();
	SetActorRotation(dir.ToOrientationRotator());
}

void AFormation::SetSpeed()
{
	Speed = -1;
	for (auto member : Members)
	{
		if (Speed == -1 || member->GetUnitData()._Speed < Speed)
		{
			Speed = member->GetUnitData()._Speed;
		}
	}
}

void AFormation::SetMembersPosition()
{
	MembersPositionMap.Empty();
	Points.Empty();

	Members.Sort([this](const AUnit& A, const AUnit& B)
		{
			return GetActorLocation().DistSquared(A.GetActorLocation(), GetActorLocation()) > GetActorLocation().DistSquared(A.GetActorLocation(), GetActorLocation());
		});

	TMap<FVector, FVector> points;
	CalculatePoints(points);

	for (AUnit* member : Members)
	{
		points.ValueSort([member](const FVector& A, const FVector& B)
			{
				return A.DistSquared(member->GetActorLocation(), A) < B.DistSquared(member->GetActorLocation(), B);
			});
		TArray<FVector> keys;
		points.GetKeys(keys);
		if (keys.Num())
		{
			MembersPositionMap.Add(member, keys[0]);
			points.Remove(keys[0]);
		}
		else
		{
			UE_LOG(TicTacLogUnitFormation, Warning,
				TEXT("AFormation::SetMembersPosition: No more position left for unit '%s'."),
				*GetNameSafe(member));
		}
	}
}

void AFormation::CalculatePoints(TMap<FVector, FVector>& OutPoints)
{
	if (Members.Num())
	{
		float line = 0;
		int32 posInLine = 0;
		int32 membersInFirstLine = Members.Num() < MembersPerLine ? Members.Num() : MembersPerLine;

		int32 nbLines = Members.Num() / MembersPerLine;
		if (Members.Num() % MembersPerLine > 0)
		{
			nbLines++;
		}
		line = nbLines / 2;

		// We calculate the relative starting position of the line based on how much members are on this line.
		// We make it negative to start on the left.
		float startPosInLine = (SpaceBetweenMembers * (membersInFirstLine - 1)) * -1;

		// finally we divide it by two, if not equal to 0, to recenter the line relatively to the formation position.
		startPosInLine = startPosInLine == 0 ? startPosInLine : startPosInLine / 2;
		for (int index = 0; index < Members.Num();)
		{
			FVector pos;

			// X is lines.
			pos.X = SpaceBetweenLines * line;

			// Y is position on line.
			pos.Y = startPosInLine + (SpaceBetweenMembers * posInLine);
			pos.Z = 0;

			OutPoints.Add(pos, AlignRelativePositionWithFormation(pos));
			Points.Add(pos);

			posInLine++;
			index++;
			if (posInLine == MembersPerLine)
			{
				posInLine = 0;
				line--;
				int32 membersLeft = Members.Num() - index;
				if (membersLeft && membersLeft < MembersPerLine)
				{
					startPosInLine = SpaceBetweenMembers * (membersLeft - 1) * -1;
					startPosInLine = startPosInLine == 0 ? startPosInLine : startPosInLine / 2;
				}
			}
		}
	}
}

FVector AFormation::GetAlignedMemberPosition(const AUnit* Member) const
{
	if (!Member || !Members.Contains(Member))
	{
		UE_LOG(TicTacLogUnitFormation, Warning,
			TEXT("AFormation::GetMemberPosition: Unit '%s' tries to get their position in formation but they are either null or not in this formation."),
			*GetNameSafe(Member));
		return FVector::ZeroVector;
	}
	else if (!MembersPositionMap.Num())
	{
		UE_LOG(TicTacLogUnitFormation, Warning,
			TEXT("AFormation::GetMemberPosition: Unit '%s' tries to get their position in formation but position has not been determined yet."),
			*GetNameSafe(Member));
		return FVector::ZeroVector;
	}
	else
	{
		const FVector* relativePos = MembersPositionMap.Find(Member);

		if (!relativePos)
		{
			UE_LOG(TicTacLogUnitFormation, Warning,
				TEXT("AFormation::GetMemberPosition: Position not found for unit '%s'."),
				*GetNameSafe(Member));
			return FVector::ZeroVector;
		}
		return AlignRelativePositionWithFormation(*relativePos);
	}
}

FVector AFormation::AlignRelativePositionWithFormation(const FVector& Position) const
{
	float dot = FVector::DotProduct(FVector::ForwardVector, GetActorForwardVector());
	float acos = FMath::Acos(dot);
	float angle = FMath::RadiansToDegrees(acos);

	// Angle is given between 0 and 180, so we bring it back to 180 and 360 if it is on the left side of Y axis.
	float finalAngle = GetActorForwardVector().Y >= 0 ? angle : 360 - angle;
	FVector pos = GetActorLocation() + Position.RotateAngleAxis(finalAngle, FVector(0, 0, 1));

	return pos;
}

bool AFormation::ShouldRun(AUnit* Member)
{
	if (!Member || !Members.Contains(Member))
	{
		UE_LOG(TicTacLogUnitFormation, Warning,
			TEXT("AFormation::GetMemberPosition: Unit '%s' tries to get their position in formation but they are either null or not in this formation."),
			*GetNameSafe(Member));
		return false;
	}
	FVector memberLoc(Member->GetActorLocation().X, Member->GetActorLocation().Y, 0.0f);
	return !memberLoc.Equals(GetAlignedMemberPosition(Member), MinRunDistTolerance);
}

float AFormation::GetUnitLerpedSpeed(AUnit* Member, float MinSpeed, float MaxSpeed)
{
	if (!Member || !Members.Contains(Member))
	{
		UE_LOG(TicTacLogUnitFormation, Warning,
			TEXT("AFormation::GetMemberPosition: Unit '%s' tries to get their position in formation but they are either null or not in this formation."),
			*GetNameSafe(Member));
		return 0.0f;
	}
	float alpha = 0.0f;
	float distSquared = FVector::DistSquaredXY(Member->GetActorLocation(), GetAlignedMemberPosition(Member));
	float minRunTolSquared = MinRunDistTolerance * MinRunDistTolerance;
	float maxRunTolSquared = MaxRunDistTolerance * MaxRunDistTolerance;
	if (distSquared > minRunTolSquared && distSquared < maxRunTolSquared)
	{
		return MaxSpeed;

		float maxDistSquared = FMath::Square(DistanceThresholdForMaxRunSpeed);
		alpha = (distSquared - minRunTolSquared) / (maxDistSquared - minRunTolSquared);
		alpha = FMath::Clamp(alpha, 0.0f, 1.0f);
	}
	else
	{
		return Speed;
	}
	return FMath::Lerp<float, float>(MinSpeed, MaxSpeed, alpha);
}

void AFormation::MoveToLocation_Implementation(const FVector& NewDestination)
{
	Destination = NewDestination;
	UpdateFormation();

	if (CurrentPath && CurrentPath->PathPoints.Num() >= 2)
	{
		IsMoving = true;
		SetTickableRPCM(true);
		OnFormationDeparted.Broadcast(this);
	}
}

void AFormation::SetTickableRPCM_Implementation(bool Tickable)
{
	SetActorTickEnabled(Tickable);
}

void AFormation::HandleArrival()
{
	IsMoving = false;
	HasArrived = true;
	SetTickableRPCM(false);
	OnFormationArrived.Broadcast(this);
	Clear();
	SetLifeSpan(2);
}
