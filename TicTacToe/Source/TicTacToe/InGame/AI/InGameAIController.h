// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "OrderManager/OrderManagerInclude.h"
#include "InGameAIController.generated.h"

class UOrderManagerComponent;
class AUnit;

/**
 * 
 */
UCLASS()
class TICTACTOE_API AInGameAIController : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

	UPROPERTY(BlueprintReadOnly)
		UOrderManagerComponent* PawnOrderManager;

	UPROPERTY(BlueprintReadOnly)
		AUnit* PawnAsUnit;

	UFUNCTION()
		void OnOrderExecutionStarted(UOrderManagerComponent* OrderManager, const FOrderStruct& NewOrder);

	UFUNCTION()
		void OnOrderExecutionFinished(UOrderManagerComponent* OrderManager, const FOrderStruct& NewOrder);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure)
		float GetPawnSpeed() const;

};
