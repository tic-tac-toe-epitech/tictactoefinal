// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "OrderManager/OrderManagerComponent.h"
#include "InGame/Unit/Unit.h"
#include "Formation/Formation.h"
#include "GameFramework/CharacterMovementComponent.h"

void AInGameAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	PawnAsUnit = Cast<AUnit>(InPawn);

	PawnOrderManager = InPawn->FindComponentByClass<UOrderManagerComponent>();

	if (PawnOrderManager)
	{
		PawnOrderManager->OnOrderExectutionStarted.AddDynamic(this, &AInGameAIController::OnOrderExecutionStarted);
		PawnOrderManager->OnOrderExectutionFinished.AddDynamic(this, &AInGameAIController::OnOrderExecutionFinished);
	}
}

void AInGameAIController::OnUnPossess()
{
	if (PawnOrderManager)
	{
		PawnOrderManager->OnOrderExectutionStarted.RemoveDynamic(this, &AInGameAIController::OnOrderExecutionStarted);
		PawnOrderManager->OnOrderExectutionFinished.RemoveDynamic(this, &AInGameAIController::OnOrderExecutionFinished);
		PawnOrderManager = nullptr;
	}
	PawnAsUnit = nullptr;

	Super::OnUnPossess();
}

void AInGameAIController::Tick(float DeltaTime)
{
	//if (PawnOrderManager && PawnOrderManager->GetCurrentOrder() && PawnOrderManager->GetCurrentOrder()->OrderType == EOrderType::Move)
	//{
	//	if (PawnAsUnit && PawnAsUnit->GetFormation())
	//	{
	//		UCharacterMovementComponent* CMC = PawnAsUnit->FindComponentByClass<UCharacterMovementComponent>();
	//		if (CMC)
	//		{
	//			CMC->MaxWalkSpeed = PawnAsUnit->GetFormation()->GetUnitLerpedSpeed(PawnAsUnit, PawnAsUnit->GetUnitData()._Speed, PawnAsUnit->GetUnitData()._Speed * 1.8f);
	//		}
	//	}
	//}
}

float AInGameAIController::GetPawnSpeed() const
{
	if (PawnAsUnit)
	{
		return PawnAsUnit->GetUnitData()._Speed;
	}
	return 0.0f;
}

void AInGameAIController::OnOrderExecutionStarted(UOrderManagerComponent* OrderManager, const FOrderStruct& NewOrder)
{
	GetBlackboardComponent()->SetValueAsEnum(TEXT("CurrentOrder"), (uint8)NewOrder.OrderType);
}

void AInGameAIController::OnOrderExecutionFinished(UOrderManagerComponent* OrderManager, const FOrderStruct& NewOrder)
{
	GetBlackboardComponent()->SetValueAsEnum(TEXT("CurrentOrder"), (uint8)EOrderType::None);
}