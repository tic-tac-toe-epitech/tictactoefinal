#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "UObject/SoftObjectPtr.h"
#include "Unit.h"
#include "UnitTableRow.generated.h"

/*
** DataTable Row used to map Tag to Units classes.
*/
USTRUCT(BlueprintType)
struct FUnitTableRow: public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSoftClassPtr<AUnit> UnitClass;
};