// Fill out your copyright notice in the Description page of Project Settings.


#include "RecruitLocationComponent.h"

URecruitLocationComponent::URecruitLocationComponent() : Super()
{
	bWantsInitializeComponent = true;

	CapsuleHalfHeight = 88;
	CapsuleRadius = 70;

	OverlappingObjectsCount = 0;
}

void URecruitLocationComponent::InitializeComponent()
{
	Super::InitializeComponent();

	OnComponentBeginOverlap.AddDynamic(this, &URecruitLocationComponent::OnComponentBeginOverlapCallBack);
	OnComponentEndOverlap.AddDynamic(this, &URecruitLocationComponent::OnComponentEndOverlapCallBack);
}

void URecruitLocationComponent::OnComponentBeginOverlapCallBack(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != GetOwner())
	{
		OverlappingObjectsCount++;

		if (OverlappingObjectsCount == 1)
		{
			OnComponentObstructed.Broadcast(this);
		}
	}
}

void URecruitLocationComponent::OnComponentEndOverlapCallBack(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && OtherActor != GetOwner())
	{
		OverlappingObjectsCount--;

		if (OverlappingObjectsCount == 0)
		{
			OnComponentFreed.Broadcast(this);
		}
	}
}
