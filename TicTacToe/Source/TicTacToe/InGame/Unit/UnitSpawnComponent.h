// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InGame/AI/OrderManager/OrderManagerInclude.h"
#include "GameplayTagContainer.h"
#include "UnitSpawnComponent.generated.h"

class ARallyPoint;
class AUnit;
class URecruitLocationComponent;
class UDataTable;

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TICTACTOE_API UUnitSpawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUnitSpawnComponent();
	virtual void InitializeComponent() override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
		void PlaceRallyPointRPCS(const FVector& RallyPointLocation);
	void PlaceRallyPointRPCS_Implementation(const FVector& RallyPointLocation);
	bool PlaceRallyPointRPCS_Validate(const FVector& RallyPointLocation) { return true; }

	UFUNCTION(BlueprintCallable)
		void SpawnUnit(TSoftClassPtr<AUnit> UnitClass);

	UFUNCTION(BlueprintCallable)
		void SpawnUnitWithTag(const FGameplayTag& UnitTag);

protected:
	ARallyPoint* RallyPoint;

	UPROPERTY(EditAnywhere)
		TSoftClassPtr<ARallyPoint> RallyPointClass;

	UPROPERTY(EditAnywhere)
		UDataTable* UnitClassDataTable;

	UPROPERTY()
		TArray<URecruitLocationComponent*> RecruitLocations;

	UPROPERTY()
		TArray<TSoftClassPtr<AUnit>> PendingSpawns;
	
	UFUNCTION()
		ARallyPoint* GetRallyPoint() const { return RallyPoint; }

	UFUNCTION()
		ARallyPoint* PlaceRallyPoint(const FVector& RallyPointLocation);

	URecruitLocationComponent* GetBestRecruitLocation();

	UFUNCTION()
		void OnRecruitLocationObstructed(URecruitLocationComponent* RecruitLocationComponent);

	UFUNCTION()
		void OnRecruitLocationFreed(URecruitLocationComponent* RecruitLocationComponent);

};
