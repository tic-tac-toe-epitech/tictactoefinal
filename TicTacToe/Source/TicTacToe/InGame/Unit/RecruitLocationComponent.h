// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/CapsuleComponent.h"
#include "RecruitLocationComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEvent_RecruitLocationComponent, URecruitLocationComponent*, RecruitLocationComponent);

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TICTACTOE_API URecruitLocationComponent : public UCapsuleComponent
{
	GENERATED_BODY()

public:
		URecruitLocationComponent();
		virtual void InitializeComponent() override;

protected:
	UFUNCTION()
		void OnComponentBeginOverlapCallBack(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnComponentEndOverlapCallBack(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	int32 OverlappingObjectsCount;

public:
	UPROPERTY(BlueprintAssignable)
		FEvent_RecruitLocationComponent OnComponentObstructed;

	UPROPERTY(BlueprintAssignable)
		FEvent_RecruitLocationComponent OnComponentFreed;

	UFUNCTION(BlueprintPure)
		bool IsObsructed() const { return OverlappingObjectsCount > 0; }
};
