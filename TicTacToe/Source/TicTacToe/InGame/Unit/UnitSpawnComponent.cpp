// Fill out your copyright notice in the Description page of Project Settings.


#include "UnitSpawnComponent.h"
#include "InGame/Building/RallyPoint.h"
#include "Engine/World.h"
#include "InGame/Unit/Unit.h"
#include "Components/MeshComponent.h"
#include "GameData/GameMode/InGameGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerState.h"
#include "InGame/AI/OrderManager/OrderManagerComponent.h"
#include "RecruitLocationComponent.h"
#include "UnitTableRow.h"
#include "Engine/DataTable.h" 

// Sets default values for this component's properties
UUnitSpawnComponent::UUnitSpawnComponent() : Super()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	bWantsInitializeComponent = true;

	bReplicates = true;
	// ...

	RallyPointClass = ARallyPoint::StaticClass();
}

void UUnitSpawnComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if (GetOwner())
	{
		for (auto comp : GetOwner()->GetComponentsByClass(URecruitLocationComponent::StaticClass()))
		{
			URecruitLocationComponent* asRecruitLoc = Cast<URecruitLocationComponent>(comp);

			if (asRecruitLoc)
			{
				asRecruitLoc->OnComponentObstructed.AddDynamic(this, &UUnitSpawnComponent::OnRecruitLocationObstructed);
				asRecruitLoc->OnComponentFreed.AddDynamic(this, &UUnitSpawnComponent::OnRecruitLocationFreed);
				RecruitLocations.Add(asRecruitLoc);
			}
		}
	}
}

// Called when the game starts
void UUnitSpawnComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UUnitSpawnComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

ARallyPoint* UUnitSpawnComponent::PlaceRallyPoint(const FVector& RallyPointLocation)
{
	if (GetOwner()->HasAuthority())
	{
		if (RallyPoint)
		{
			RallyPoint->Destroy();
			RallyPoint = nullptr;
		}

		RallyPoint = GetWorld()->SpawnActor<ARallyPoint>(RallyPointClass.LoadSynchronous(), RallyPointLocation, FRotator::ZeroRotator);

		return RallyPoint;
	}
	return nullptr;
}

void UUnitSpawnComponent::PlaceRallyPointRPCS_Implementation(const FVector& RallyPointLocation)
{
	PlaceRallyPoint(RallyPointLocation);
}

void UUnitSpawnComponent::SpawnUnitWithTag(const FGameplayTag& UnitTag)
{
	if (UnitClassDataTable)
	{
		FUnitTableRow* row = UnitClassDataTable->FindRow<FUnitTableRow>(*UnitTag.ToString(), "");
		if (row)
		{
			SpawnUnit(row->UnitClass);
		}
	}
}

void UUnitSpawnComponent::SpawnUnit(TSoftClassPtr<AUnit> UnitClass)
{
	if (GetOwner() && !UnitClass.IsNull())
	{
		URecruitLocationComponent* spawnLoc = GetBestRecruitLocation();

		if (!spawnLoc)
		{
			PendingSpawns.Add(UnitClass);
		}
		else
		{
			AInGameGameMode* gameMode = Cast<AInGameGameMode>(GetWorld()->GetAuthGameMode());

			if (gameMode)
			{
				APlayerState* playerState = Cast<APlayerState>(GetOwner()->GetOwner());
				if (playerState)
				{
					AUnit* unit = gameMode->SpawnNewUnit(UnitClass, Cast<APlayerController>(playerState->GetOwner()), FTransform(spawnLoc->GetComponentLocation()));

					if (unit && GetRallyPoint())
					{
						UOrderManagerComponent* OMC = unit->FindComponentByClass<UOrderManagerComponent>();

						if (OMC)
						{
							FOrderStruct order;
							order.OrderType = EOrderType::Move;
							order.TargetLocation = GetRallyPoint()->GetRallyLocation();

							OMC->AddOrder(order);
						}
					}
				}
			}
		}
	}
}

URecruitLocationComponent* UUnitSpawnComponent::GetBestRecruitLocation()
{
	for (auto location : RecruitLocations)
	{
		if (!location->IsObsructed())
		{
			return location;
		}
	}
	return nullptr;
}

void UUnitSpawnComponent::OnRecruitLocationObstructed(URecruitLocationComponent* RecruitLocationComponent)
{

}

void UUnitSpawnComponent::OnRecruitLocationFreed(URecruitLocationComponent* RecruitLocationComponent)
{
	if (PendingSpawns.Num())
	{
		TSoftClassPtr<AUnit> unitClass = PendingSpawns[0];
		PendingSpawns.RemoveAt(0);
		SpawnUnit(unitClass);
	}
}
