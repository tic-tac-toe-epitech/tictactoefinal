// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameData/Configuration/Shared/Price.h"
#include "InGame/Resources/ResourceInclude.h"
#include "ResourcesManagerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FEvent_UResourcesManagerComponent, UResourcesManagerComponent*, UResourcesManagerComponent, EResourceType, Type, int32, Value);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TICTACTOE_API UResourcesManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UResourcesManagerComponent();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	//virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction);


public:	
	// Called every frame
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
public:
	UFUNCTION(BlueprintCallable)
		bool HasRessourcesAvailable(int32 Gold, int32 Wood, int32 Steel);

	UFUNCTION(BlueprintCallable)
		bool HasRessourcesAvailableWithStruct(const FPriceStruct& PriceStruct);

public:
	UFUNCTION(BlueprintCallable)
		int32 GetResources(EResourceType type) const;
	UFUNCTION(BlueprintCallable)
		void AddResources(EResourceType type, int32 addValue);
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_UResourcesManagerComponent OnResourcesUpdated;
	UFUNCTION()
		void OnRep_Gold();
	UFUNCTION()
		void OnRep_Wood();
	UFUNCTION()
		void OnRep_Steel();
//All seters and getters for the resources
public:
	UFUNCTION(BlueprintCallable)
		int32 GetGold() const;
	UFUNCTION(BlueprintCallable)
		int32 GetWood() const;
	UFUNCTION(BlueprintCallable)
		int32 GetSteel() const;
	UFUNCTION(BlueprintCallable)
		void SetGold(int32 newValue);
	UFUNCTION(BlueprintCallable)
		void SetWood(int32 newValue);
	UFUNCTION(BlueprintCallable)
		 void SetSteel(int32 newValue);
	UFUNCTION(BlueprintCallable)
		int32 AddGold(int32 addValue);
	UFUNCTION(BlueprintCallable)
		int32 AddWood(int32 addValue);
	UFUNCTION(BlueprintCallable)
		int32 AddSteel(int32 addValue);
	UFUNCTION(BlueprintCallable)
		int32 TakeGold(int32 newValue);
	UFUNCTION(BlueprintCallable)
		int32 TakeWood(int32 newValue);
	UFUNCTION(BlueprintCallable)
		int32 TakeSteel(int32 newValue);
private:
	//Replicated Using pour mettre � jour le GUI
	UPROPERTY(ReplicatedUsing=OnRep_Gold)
		int32 Gold;
	UPROPERTY(ReplicatedUsing=OnRep_Wood)
		int32 Wood;
	UPROPERTY(ReplicatedUsing=OnRep_Steel)
		int32 Steel;
	UPROPERTY()
		float deltasecondes;
	
};
