// Fill out your copyright notice in the Description page of Project Settings.

#include "ResourcesManagerComponent.h"
#include "TicTacToe.h"
#include "UnrealNetwork.h"
#include "GameData/Configuration/ConfigurationLoader.h"
#include "GameFramework/Actor.h"


// Sets default values for this component's properties
UResourcesManagerComponent::UResourcesManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryComponentTick.bCanEverTick = true;
	//PrimaryComponentTick.bStartWithTickEnabled = true;
	bReplicates = true;
}


void UResourcesManagerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Property replication sample. Replace ReplicatedProperty by your property and repeat this line for any properties that need to be replicated.
	DOREPLIFETIME_CONDITION(UResourcesManagerComponent, Gold, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UResourcesManagerComponent, Wood, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UResourcesManagerComponent, Steel, COND_OwnerOnly);
}

// Called when the game starts
void UResourcesManagerComponent::BeginPlay()
{
	Super::BeginPlay();
	UConfigurationLoader* loader = NewObject<UConfigurationLoader>(this);
	FMapStruct MapData = loader->LoadAndGetMapConfig();
	Gold = MapData._DefaultPlayerRessources._Gold;
	Wood = MapData._DefaultPlayerRessources._Wood;
	Steel = MapData._DefaultPlayerRessources._Steel;
}

//void UResourcesManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
//{
//	deltasecondes += DeltaTime;
//	if (deltasecondes >= 1)
//	{
//		deltasecondes = 0;
//		Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//		if (GetOwner()->HasAuthority())
//		{
//			this->SetGold(Gold + 1);
//			this->SetWood(Wood + 1);
//			this->SetSteel(Steel + 1);
//		}
//	}
//}

bool UResourcesManagerComponent::HasRessourcesAvailable(int32 _Gold, int32 _Wood, int32 _Steel)
{
	if (_Gold <= Gold && _Wood <= Wood && _Steel <= Steel)
	{
		return true;
	}

	return false;
}

bool UResourcesManagerComponent::HasRessourcesAvailableWithStruct(const FPriceStruct& PriceStruct)
{
	return HasRessourcesAvailable(PriceStruct._Gold, PriceStruct._Wood, PriceStruct._Steel);
}


int32 UResourcesManagerComponent::GetGold() const
{
	return Gold;
}

int32 UResourcesManagerComponent::GetWood() const
{
	return Wood;
}

int32 UResourcesManagerComponent::GetSteel() const
{
	return Steel;
}

void UResourcesManagerComponent::SetGold(int32 newValue)
{
	if (newValue >= 0)
	{
		Gold = newValue;
	}
}

void UResourcesManagerComponent::SetWood(int32 newValue)
{
	if (newValue >= 0)
	{
		Wood = newValue;
	}
}

void UResourcesManagerComponent::SetSteel(int32 newValue)
{
	if (newValue >= 0)
	{
		Steel = newValue;
	}
}

int32 UResourcesManagerComponent::AddGold(int32 addValue)
{
	if (Gold + addValue >= 0)
	{
		Gold += addValue;
		OnRep_Gold();
		return Gold;
	}
	else
	{
		return -1;
	}
}

int32 UResourcesManagerComponent::AddWood(int32 addValue)
{
	if (Wood + addValue >= 0)
	{
		Wood += addValue;
		return Wood;
	}
	else
	{
		return -1;
	}
}

int32 UResourcesManagerComponent::AddSteel(int32 addValue)
{
	if (Steel + addValue >= 0)
	{
		Steel += addValue;
		return Steel;
	}
	else
	{
		return -1;
	}
}

int32 UResourcesManagerComponent::TakeGold(int32 newValue)
{
	if (Gold - newValue >= 0)
	{
		Gold -= newValue;
		OnRep_Gold();
		return Gold;
	}
	else
	{
		return -1;
	}
}

int32 UResourcesManagerComponent::TakeWood(int32 newValue)
{
	if (Wood - newValue >= 0)
	{
		Wood -= newValue;
		return Wood;
	}
	else
	{
		return -1;
	}
}

int32 UResourcesManagerComponent::TakeSteel(int32 newValue)
{
	if (Steel - newValue >= 0)
	{
		Steel -= newValue;
		return Steel;
	}
	else
	{
		return -1;
	}
}

int32 UResourcesManagerComponent::GetResources(EResourceType type) const
{
	if (type == EResourceType::Gold)
	{
		return Gold;
	}
	else if (type == EResourceType::Wood)
	{
		return Wood;
	}
	else if (type == EResourceType::Steel)
	{
		return Steel;
	}
	else
	{
		UE_LOG(TicTacLogResourceManager, Error, TEXT("Unknown Resource type, value is: %u"), type);
		return 0;
	}
}

void UResourcesManagerComponent::AddResources(EResourceType type, int32 addValue)
{
	if (type == EResourceType::Gold)
	{
		Gold += addValue;
	}
	else if (type == EResourceType::Wood)
	{
		Wood += addValue;
	}
	else if (type == EResourceType::Steel)
	{
		Steel += addValue;
	}
	else
	{
		UE_LOG(TicTacLogResourceManager, Error, TEXT("Unknown Resource type, value is: %u"), type);
	}
}

void UResourcesManagerComponent::OnRep_Gold()
{
	OnResourcesUpdated.Broadcast(this, EResourceType::Gold, Gold);
}

void UResourcesManagerComponent::OnRep_Wood()
{
	OnResourcesUpdated.Broadcast(this, EResourceType::Wood, Wood);
}

void UResourcesManagerComponent::OnRep_Steel()
{
	OnResourcesUpdated.Broadcast(this, EResourceType::Steel, Steel);
}



//// Called every frame
//void UResourcesManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//
//	// ...
//}

