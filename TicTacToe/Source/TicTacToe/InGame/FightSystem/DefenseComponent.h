// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameplayTagsModule.h"
#include "GameplayTags.h"
#include "Components/CapsuleComponent.h"
#include "Engine/EngineTypes.h"
#include "Components/ActorComponent.h"
#include "DefenseComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FHPChangedDelegate, UDefenseComponent*, DefenseComponent, int32, TotalHP, int32, RemainingHP);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_DefenseComponent_AttackComponent, UDefenseComponent*, DefenseComponent, UAttackComponent*, AttackComponent);


UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TICTACTOE_API UDefenseComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDefenseComponent();

#pragma region Components

protected:
	UPROPERTY(EditAnywhere)
		float DamageZoneRadius = 60.0f;

	UPROPERTY()
		USphereComponent* DamageZone;

#pragma endregion Components


protected:
	// Total Health points
	UPROPERTY(ReplicatedUsing = OnRep_TotalHP, BlueprintReadOnly)
	int	_TotalHP;

	// Total Health points
	UPROPERTY(ReplicatedUsing = OnRep_RemainingHP, BlueprintReadOnly)
	int	_RemainingHP;

	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

	UFUNCTION()
		void OnRep_TotalHP();

	UFUNCTION()
		void OnRep_RemainingHP();

	UFUNCTION(NetMulticast, Reliable)
		void ClientOnAttack(UAttackComponent *attack);

public:
	// Setup component
	virtual void InitializeComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void TakeDamage(UAttackComponent *attack);

	UFUNCTION(BlueprintPure)
	int  GetTotalHP() { return _TotalHP; }

	UFUNCTION(BlueprintPure)
	int  GetRemainingHP() { return _RemainingHP; }

	UPROPERTY(BlueprintAssignable)
		FHPChangedDelegate OnHPChanged;

	UPROPERTY(BlueprintAssignable)
		FEvent_DefenseComponent_AttackComponent OnTakeDamage;
};
