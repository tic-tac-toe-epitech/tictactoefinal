// Fill out your copyright notice in the Description page of Project Settings.


#include "DefenseComponent.h"
#include "AttackComponent.h"
#include "GameData/Configuration/ConfigurationLoader.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "Logging/MessageLog.h"
#include "TicTacToe.h"
#include "UnrealNetwork.h"
#include "InGame/Unit/Unit.h"
#include "InGame/Building/Building.h"
#include "InGame/AI/OrderManager/OrderManagerComponent.h"
#include "InGame/AI/OrderManager/OrderManagerInclude.h"

// Sets default values for this component's properties
UDefenseComponent::UDefenseComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bReplicates = true;
	bAutoActivate = true;
	bAutoRegister = true;
	bIsActive = true;
	bReplicates = true;
	DamageZone = CreateDefaultSubobject<USphereComponent>(TEXT("Damage Zone"));
}

void UDefenseComponent::InitializeComponent() {
	DamageZone->SetCollisionProfileName(TEXT("Defense"));
}


// Called when the game starts
void UDefenseComponent::BeginPlay()
{
	Super::BeginPlay();
	UConfigurationLoader*	loader = NewObject<UConfigurationLoader>(this);
	AUnit*					unitOwner = Cast<AUnit>(GetOwner());
	ABuilding*				buildingOwner = Cast<ABuilding>(GetOwner());

	if (unitOwner) {
		FUnitsStruct			UnitsData = loader->LoadAndGetUnitsConfig();
		FGameplayTag			UnitType = unitOwner->GetUnitType();
		FUnitStruct				UnitData = FUnitsStruct::GetUnitData(UnitsData, UnitType.ToString());

		this->_TotalHP = UnitData._Hp;
		this->_RemainingHP = UnitData._Hp;
	}
	else if (buildingOwner) {
		FBuildingsStruct		BuildingsData = loader->LoadAndGetBuildingsConfig();
		FGameplayTag			BuildingType = buildingOwner->GetBuildingType();
		FBuildingStruct			BuildingData = FBuildingsStruct::GetBuildingData(BuildingsData, BuildingType.ToString());

		this->_TotalHP = BuildingData._Hp;
		this->_RemainingHP = BuildingData._Hp;
	}

	DamageZone->ComponentTags.Add(TEXT("Damageable"));
	DamageZone->AttachToComponent(this->GetOwner()->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	DamageZone->SetSphereRadius(DamageZoneRadius);
	DamageZone->Activate();
}

void UDefenseComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Property replication sample. Replace ReplicatedProperty by your property and repeat this line for any properties that need to be replicated.
	DOREPLIFETIME(UDefenseComponent, _TotalHP);
	DOREPLIFETIME(UDefenseComponent, _RemainingHP);
}


// Called every frame
void UDefenseComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UDefenseComponent::ClientOnAttack_Implementation(UAttackComponent *attack) {
	OnTakeDamage.Broadcast(this, attack);
	if (IsValid(attack)) {
		attack->OnMakeDamage.Broadcast(attack, this);
	}
}

void UDefenseComponent::TakeDamage(UAttackComponent* attack) {
	AUnit*				Unit = Cast<AUnit>(this->GetOwner());
	ABuilding*			Building = Cast<ABuilding>(this->GetOwner());
	AInGamePlayerState*	OwnerPlayerState = nullptr;


	_RemainingHP -= attack->GetAttackDamage();
	ClientOnAttack(attack);
	if (Unit) {
		OwnerPlayerState = Cast<AInGamePlayerState>(Unit->GetOwner());
	}
	else if (Building) {
		OwnerPlayerState = Cast<AInGamePlayerState>(Building->GetOwner());
	}

	UE_LOG(TicTacLogFightSystem, Verbose, TEXT("UDefenseComponent::TakeDamage: %s loose %d hp."), *this->GetFName().ToString(), attack->GetAttackDamage());
	if (_RemainingHP <= 0) {
		if (OwnerPlayerState) {
			UE_LOG(TicTacLogFightSystem, Verbose, TEXT("UDefenseComponent::TakeDamage: %s died."), *this->GetFName().ToString());
			attack->StopAttack();
			if (Unit) {
				OwnerPlayerState->RemoveUnit(Unit);
				GetWorld()->DestroyActor(Unit);
			}
			else if (Building) {
				OwnerPlayerState->RemoveBuilding(Building);
				GetWorld()->DestroyActor(Building);
			}
		}
	}
}

void UDefenseComponent::OnRep_TotalHP()
{
	OnHPChanged.Broadcast(this, GetTotalHP(), GetRemainingHP());
}

void UDefenseComponent::OnRep_RemainingHP()
{
	OnHPChanged.Broadcast(this, GetTotalHP(), GetRemainingHP());
}

