// Fill out your copyright notice in the Description page of Project Settings.
#include "AttackComponent.h"
#include "Logging/MessageLog.h"
#include "TicTacToe.h"
#include "GameData/Configuration/ConfigurationLoader.h"
#include "Libraries/DiplomacyLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"



// Sets default values for this component's properties
UAttackComponent::UAttackComponent(): Super()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	bAutoActivate = true;
	bAutoRegister = true;
	bIsActive = true;
	DetectionZone = CreateDefaultSubobject<USphereComponent>(TEXT("Detection Zone"));
	AttackZone = CreateDefaultSubobject<USphereComponent>(TEXT("Attack Zone"));
}

void UAttackComponent::InitializeComponent() {
	DetectionZone->SetCollisionProfileName(TEXT("Attack"));
	AttackZone->SetCollisionProfileName(TEXT("Attack"));
}

bool UAttackComponent::ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	// Subobject replication sample. Replace ReplicatedSubobject by your subobjects and repeat these steps for any objects that need to be replicated.
	//if (ReplicatedSubobject != nullptr)
	//{
	//	WroteSomething |= Channel->ReplicateSubobject(ReplicatedSubobject, *Bunch, *RepFlags);
	//}

	return WroteSomething;
}

void UAttackComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Property replication sample. Replace ReplicatedProperty by your property and repeat this line for any properties that need to be replicated.
	DOREPLIFETIME( UAttackComponent, _Target );
}


// Called when the game starts
void UAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	UConfigurationLoader*	loader = NewObject<UConfigurationLoader>(this);
	FUnitsStruct			UnitsData = loader->LoadAndGetUnitsConfig();
	FGameplayTag			UnitType = Cast<AUnit>(GetOwner())->GetUnitType();
	FUnitStruct				UnitData = FUnitsStruct::GetUnitData(UnitsData, UnitType.ToString());

	this->_AttackDamage = UnitData._Damage._Value;

	DetectionZone->AttachToComponent(this->GetOwner()->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	DetectionZone->SetSphereRadius(DetectionZoneRadius * (UnitData._Scope + 1));
	DetectionZone->Activate();

	AttackZone->AttachToComponent(this->GetOwner()->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	AttackZone->SetSphereRadius(AttackZoneRadius * (UnitData._Scope + 1));
	AttackZone->Activate();
}


// Called every frame
void UAttackComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UAttackComponent::AttackTargetOrder_Implementation(UDefenseComponent* target) {
    //TODO Get Defense component and apply damage
	_Target = target;
}

void UAttackComponent::AttackTarget(UDefenseComponent* target) {
	if (IsValid(target)) {
		AttackTargetOrder(target);
	}
}

void UAttackComponent::GetDamageableComponentsOnDetectionZone(TArray<UDefenseComponent*> &overlappingDefenseComponents) {
	TArray<UPrimitiveComponent*> overlappingComponents;

	DetectionZone->GetOverlappingComponents(overlappingComponents);
	for (UPrimitiveComponent* component : overlappingComponents) {
		UDefenseComponent* defenseComponent = Cast<UDefenseComponent>(component->GetOwner()->GetComponentByClass(UDefenseComponent::StaticClass()));
		if (defenseComponent != nullptr && component->ComponentHasTag(TEXT("Damageable"))  && defenseComponent->GetOwner() != this->GetOwner()) {
			overlappingDefenseComponents.Add(defenseComponent);
		}
	}
}

void UAttackComponent::GetDamageableComponentsOnAttackZone(TArray<UDefenseComponent*> &overlappingDefenseComponents) {
	TArray<UPrimitiveComponent*> overlappingComponents;

	AttackZone->GetOverlappingComponents(overlappingComponents);
	for (UPrimitiveComponent* component : overlappingComponents) {
		UDefenseComponent* defenseComponent = Cast<UDefenseComponent>(component->GetOwner()->GetComponentByClass(UDefenseComponent::StaticClass()));
		if (defenseComponent != nullptr && component->ComponentHasTag(TEXT("Damageable")) && defenseComponent->GetOwner() != this->GetOwner()) {
			overlappingDefenseComponents.Add(defenseComponent);
		}
	}
}

void UAttackComponent::StopAttack() {
	_Target = nullptr;
}
