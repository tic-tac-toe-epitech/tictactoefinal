// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/SphereComponent.h"
#include "GameplayTagsModule.h"
#include "GameplayTags.h"
#include "InGame/Unit/Unit.h"
#include "InGame/FightSystem/DefenseComponent.h"
#include "Components/ActorComponent.h"
#include "AttackComponent.generated.h"

class UStaticMeshComponent;


UENUM(BlueprintType, Blueprintable)
enum class EAttackMode : uint8
{
	Search,
	Attack,
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_AttackComponent_DefenseComponent, UAttackComponent*, AttackComponent, UDefenseComponent*, DefenseComponent);


UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TICTACTOE_API UAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UAttackComponent();

	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#pragma region Components

protected:
	UPROPERTY(EditAnywhere)
		float DetectionZoneRadius = 280.0f;

	UPROPERTY(EditAnywhere)
		float AttackZoneRadius = 80.0f;

	UPROPERTY()
		USphereComponent* DetectionZone;

	UPROPERTY()
		USphereComponent* AttackZone;

#pragma endregion Components

protected:
	UPROPERTY(Replicated)
	UDefenseComponent*	_Target = nullptr;

	// Damage by seconds
	UPROPERTY()
	int	_AttackDamage;

	// Called when the game starts
	virtual void BeginPlay() override;

	// Attack unit
	UFUNCTION(Server, Reliable, WithValidation)
	void AttackTargetOrder(UDefenseComponent* target);
	void AttackTargetOrder_Implementation(UDefenseComponent* target);
	bool AttackTargetOrder_Validate(UDefenseComponent* target) { return true; }

public:	
	// Setup component
	virtual void InitializeComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Get damageable components on detection zone
	UFUNCTION(BlueprintCallable)
	void GetDamageableComponentsOnDetectionZone(TArray<UDefenseComponent*> &overlappingDefenseComponents);

	// Get damageable components on attack zone
	UFUNCTION(BlueprintCallable)
	void GetDamageableComponentsOnAttackZone(TArray<UDefenseComponent*> &overlappingDefenseComponents);

	UFUNCTION(BlueprintPure)
	int GetAttackDamage() { return _AttackDamage; }

	UFUNCTION(BlueprintCallable)
	void AttackTarget(UDefenseComponent* target);

	UFUNCTION(BlueprintCallable)
	void StopAttack();

	UFUNCTION(BlueprintPure)
	UDefenseComponent* GetCurrentTarget() {
		if (!IsValid(_Target))
			_Target = nullptr;
		return _Target;
	}

	UPROPERTY(BlueprintAssignable)
		FEvent_AttackComponent_DefenseComponent OnMakeDamage;
};
