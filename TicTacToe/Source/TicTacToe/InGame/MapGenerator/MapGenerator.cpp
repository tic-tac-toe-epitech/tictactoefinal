// Fill out your copyright notice in the Description page of Project Settings.


#include "MapGenerator.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/LevelStreaming.h"
#include "Math/Vector.h"
#include "Misc/Paths.h" 
#include "HAL/FileManager.h"
#include "Logging/MessageLog.h"
#include "Components/BoxComponent.h"
#include "UObject/UObjectGlobals.h"
#include "UObject/SoftObjectPtr.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerStart.h"
#include "GameData/GameMode/InGameGameMode.h"
#include "GameData/UnitSpawner/UnitSpawner.h"
#include "GameData/UnitSpawner/StartingPlayerActors.h"
#include "TicTacToe.h"
#include "GameData/Network/ObjectNetworkSyncronizer.h"
#include "GameData/Controller/InGamePlayerController.h"
#include "InGame/Resources/ResourcePoint.h"
#include "InGame/Building/BuildableArea.h"
#include "GameFramework/PlayerState.h"

//TODO: Constants put in gameplay statics

void UMapGenerator::Init(UUnitSpawner *InUS)
{
	this->US = InUS;
	AInGameGameMode* gameMode = Cast<AInGameGameMode>(GetOuter());
	if (gameMode)
	{
		gameMode->OnSynchronizerSpawned.AddDynamic(this, &UMapGenerator::OnSynchronizerSpawnedByGameMode);
	}
}

void UMapGenerator::GenerateMap(UObject* worldContextObject, USceneComponent *rootComponent)
{
	const int32 squaresPerSides = 3;
	TArray<FName> ListOfNameLevels = GetNameOfLevels(true);
	TArray<FName> ListOfMiddleLevels = GetNameOfCentralLevels(true);

	if (ListOfNameLevels.Num() < 8 || ListOfMiddleLevels.Num() < 1)
	{
		UE_LOG(TicTacLogMapGeneration, Error,
			TEXT("UMapGenerator::GenerateMap: Not enough levels to fill in the map. At least 8 side levels and 1 center level needed."));
	}

	int32 indexMap = 0;
	for (int i = 0; i < 9; i++)
	{
		FLatentActionInfo	info;
		ULevelStreaming		*subLevel = nullptr;
		int32				hasError = false;

		//middle tile
		if (i == 4)
		{
			GenerateTile(worldContextObject, ListOfMiddleLevels, info, i);
		}
		else
		{
			GenerateTile(worldContextObject, ListOfNameLevels, info, i);
		}
	}
	GenerateMapWalls();
}

void UMapGenerator::GenerateTile(UObject* worldContextObject, TArray<FName> &listOfLevels, FLatentActionInfo info, int32 index)
{
	ULevelStreaming		*subLevel = nullptr;

	if (!listOfLevels.Num())
	{
		UE_LOG(TicTacLogMapGeneration, Error,
			TEXT("UMapGenerator::GenerateMap: %s level still needed but none is left to place."), (index == 4 ? TEXT("Central") : TEXT("Side")));
		return ;
	}
	int32 indexMap = UKismetMathLibrary::RandomIntegerInRange(0, listOfLevels.Num() - 1);
	UGameplayStatics::LoadStreamLevel(worldContextObject, listOfLevels[indexMap], false, false, info);
	subLevel = UGameplayStatics::GetStreamingLevel(worldContextObject, listOfLevels[indexMap]);
	if (subLevel == NULL)
	{
		UE_LOG(TicTacLogMapGeneration, Error,
			TEXT("UMapGenerator::GenerateMap: Could not load %s level '%s'."),
			(index == 4 ? "central" : "side"),
			*listOfLevels[indexMap].ToString());
		return ;
	}
	listOfLevels.RemoveAt(indexMap);
	if (subLevel)
	{
		subLevel->LevelTransform = CreateTransformForSubLevel(index);
		subLevel->SetShouldBeLoaded(true);
		subLevel->SetShouldBeVisible(true);
	}
}

void UMapGenerator::OnSynchronizerSpawnedByGameMode(AInGameGameMode* GameMode, AObjectNetworkSyncronizer* Synchronizer, AInGamePlayerController* OwningPlayer)
{
	if (Synchronizer)
	{
		TArray<AActor*> outResourcePoints;
		UGameplayStatics::GetAllActorsOfClass(GameMode, AResourcePoint::StaticClass(), outResourcePoints);

		for (auto resourcePoint : outResourcePoints)
		{
			Synchronizer->RegisterActor(resourcePoint);
		}
	}

}

FString const UMapGenerator::GetUniqueInstanceMapName(int32 tileIterator)
{
	FString FStringMap = FString(TEXT("Map"));
	FString uniqueIterator = FString::FromInt(tileIterator);
	FString uniqueInstanceName = FStringMap + uniqueIterator;
	return uniqueInstanceName;
}

FTransform UMapGenerator::CreateTransformForSubLevel(int index)
{
	float x = (((index % 3) - 1) * TILESIZE) * VERTEXVALUE;
	float y = (((index / 3) - 1) * TILESIZE) * VERTEXVALUE;
	FTransform transformVector;
	if (index == 4)
	{
		transformVector = FTransform(FRotator(0.f, 90.f, 0.f), FVector(x, y, 0.f));
	}
	else
	{
		transformVector = FTransform(FVector(x, y, 0.f));
	}
	return transformVector;
}

const TArray<FName>& UMapGenerator::GetNameOfLevels(bool isExample) const
{
	return SideZonesSubLevelNames;
}

const TArray<FName>& UMapGenerator::GetNameOfCentralLevels(bool isExample) const
{
	return CentralZoneSubLevelNames;
}

int32 UMapGenerator::GenerateMapWalls()
{
	return false;
}

void UMapGenerator::SpawnBaseUnitForPlayer(APlayerController * playerController, UObject* worldContextObject, FString nameOfActor, AActor *playerStart)
{
	AInGameGameMode *ptrIngameGameMode = Cast<AInGameGameMode>(UGameplayStatics::GetGameMode(worldContextObject));
	FTransform transformVector = playerStart->GetTransform();
	TSoftClassPtr<AUnit> softclassUnit = US->GetSoftClassPtrAUnit(nameOfActor);
	AUnit* unit = ptrIngameGameMode->SpawnNewUnit(softclassUnit, playerController, transformVector);

	if (unit && ptrIngameGameMode->GetSynchronizerForPlayer(playerController))
	{
		ptrIngameGameMode->GetSynchronizerForPlayer(playerController)->RegisterActor(unit);
	}
}

void UMapGenerator::SpawnBaseBuildingForPlayer(APlayerController * playerController, UObject * worldContextObject, FString nameOfActor, AActor * playerStart)
{
	AInGameGameMode *ptrIngameGameMode = Cast<AInGameGameMode>(UGameplayStatics::GetGameMode(worldContextObject));
	FTransform transformVector = playerStart->GetTransform();
	TSoftClassPtr<ABuilding> softclassBuilding = US->GetSoftClassPtrABuilding(nameOfActor);
	ABuilding* building = ptrIngameGameMode->SpawnNewBuilding(softclassBuilding, playerController, transformVector);

	if (building && ptrIngameGameMode->GetSynchronizerForPlayer(playerController))
	{
		ptrIngameGameMode->GetSynchronizerForPlayer(playerController)->RegisterActor(building);
	}
}

void UMapGenerator::SetBaseAreaOwnerToPlayer(APlayerState *playerState, AActor *BaseArea) {
	BaseArea->SetOwner(playerState);
}

void UMapGenerator::InitBaseAreasOwnership(UObject *WorldContextObject, UStartingPlayerActors *playerActors)
{
	TArray<AActor*> AllBuildableArea;
	FGameplayTag	BaseAreaPlayer1Tag = UGameplayTagsManager::Get().RequestGameplayTag("BuildableArea.BaseArea.Player1");
	FGameplayTag	BaseAreaPlayer2Tag = UGameplayTagsManager::Get().RequestGameplayTag("BuildableArea.BaseArea.Player2");

	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, ABuildableArea::StaticClass(), AllBuildableArea);
	for (auto Actor : AllBuildableArea)
	{
		ABuildableArea* BuildableArea = Cast<ABuildableArea>(Actor);
		APlayerController* player = nullptr;
		if (BuildableArea->GetBuildableAreaPositionTag().MatchesTag(BaseAreaPlayer1Tag))
		{
			this->SetBaseAreaOwnerToPlayer(playerActors->player1->GetPlayerState<APlayerState>(), BuildableArea);
		}
		else if (BuildableArea->GetBuildableAreaPositionTag().MatchesTag(BaseAreaPlayer2Tag))
		{
			this->SetBaseAreaOwnerToPlayer(playerActors->player2->GetPlayerState<APlayerState>(), BuildableArea);
		}

	}
}

UStartingPlayerActors *UMapGenerator::GetArrayPlayersStartingActors()
{
	UStartingPlayerActors *funradio = NewObject<UStartingPlayerActors>();

	funradio->player1Actors = player1StartingActors;
	funradio->player2Actors = player2StartingActors;

	return funradio;
}

void UMapGenerator::SpawnPlayerStartAssets(UObject *WorldContextObject, UStartingPlayerActors *playerActors)
{
	TArray<AActor*> AllPlayerStart;

	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, APlayerStart::StaticClass(), AllPlayerStart);

	for (auto playerStart : AllPlayerStart)
	{
		APlayerController* player = nullptr;

		if (playerStart->ActorHasTag("Player1"))
		{
			player = playerActors->player1;
		}
		else if (playerStart->ActorHasTag("Player2"))
		{
			player = playerActors->player2;
		}

		if (player)
		{
			if (playerStart->ActorHasTag("Worker"))
			{
				this->SpawnBaseUnitForPlayer(player, WorldContextObject, FString(TEXT("Worker")), playerStart);
			}
			else if (playerStart->ActorHasTag("Soldier"))
			{
				this->SpawnBaseUnitForPlayer(player, WorldContextObject, FString(TEXT("Soldier")), playerStart);
			}
			else if (playerStart->ActorHasTag("Shooter"))
			{
				this->SpawnBaseUnitForPlayer(player, WorldContextObject, FString(TEXT("Shooter")), playerStart);
			}
			else if (playerStart->ActorHasTag("CityHall"))
			{
				this->SpawnBaseBuildingForPlayer(player, WorldContextObject, FString(TEXT("CityHall")), playerStart);
			}
			else if (playerStart->ActorHasTag("Hearth"))
			{
				this->SpawnBaseBuildingForPlayer(player, WorldContextObject, FString(TEXT("Hearth")), playerStart);
			}
			else
			{
				UE_LOG(TicTacLogMapGeneration, Warning,
					TEXT("UMapGenerator::SpawnAllTheThings: Couldn't spawn the actor for player start '%s', the tag was not known or present"),
					*GetNameSafe(playerStart));
			}
		}
		else
		{
			UE_LOG(TicTacLogMapGeneration, Log,
				TEXT("UMapGenerator::SpawnAllTheThings: Player start '%s' has neither 'Player1' nor 'Player2' tag. Cannot spawn anything on it."),
				*GetNameSafe(playerStart));
		}
	}
}

void UMapGenerator::FillArraysWithStartingPlayers()
{
	//iterator_diablo is used because for some reason all the actors are loaded when this function is called the second time only. FUCK UNREAL ENGINE

	if (iterator_diablo == 0)
	{
		iterator_diablo++;
	}
	else if (iterator_diablo == 1)
	{
		TArray<AActor *> AllPlayerStart;

		UGameplayStatics::GetAllActorsOfClass(currentLevelStreaming, APlayerStart::StaticClass(), AllPlayerStart);
		for (int32 i = 0; i < 7; ++i)
		{
			player1StartingActors.Add(AllPlayerStart[i]);
		}
		for (int32 i = 7; i < 14; ++i)
		{
			player2StartingActors.Add(AllPlayerStart[i]);
		}
		OnPlayerStartGathered.Broadcast(this);
	}
	else
	{
		UE_LOG(TicTacLogMapGeneration, Warning, TEXT("UMapGenerator::FillArraysWithStartingPlayers: ya une couille dans le pate dans la generation de map"));
	}
}