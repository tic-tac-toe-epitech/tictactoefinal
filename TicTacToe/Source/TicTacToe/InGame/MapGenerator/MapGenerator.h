// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MapGenerator.generated.h"

class APlayerState;
class UUnitSpawner;
class UStartingPlayerActors;
class ULevelStreaming;
class AInGameGameMode;
class AObjectNetworkSyncronizer;
class AInGamePlayerController;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEvent_UMapGenerator, UMapGenerator*, MapGenerator);

/**
 *
 */
UCLASS(Blueprintable, BlueprintType)
class TICTACTOE_API UMapGenerator : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FEvent_UMapGenerator OnPlayerStartGathered;
	UFUNCTION(BlueprintCallable)
	void GenerateMap(UObject* WorldContextObject, USceneComponent *rootComponent);
	UFUNCTION()
	void Init(UUnitSpawner * US);
	UFUNCTION()
	FString const GetUniqueInstanceMapName(int32 tileIterator);
	UFUNCTION()
	UStartingPlayerActors *GetArrayPlayersStartingActors();
	UFUNCTION()
	void SpawnPlayerStartAssets(UObject *WorldContextObject, UStartingPlayerActors *playerActors);
	UFUNCTION()
	void InitBaseAreasOwnership(UObject *WorldContextObject, UStartingPlayerActors *playerActors);
	UFUNCTION()
	const TArray<FName>& GetNameOfLevels(bool isExample) const;
	UFUNCTION()
	FTransform CreateTransformForSubLevel(int index);
	UFUNCTION()
	const TArray<FName>& GetNameOfCentralLevels(bool isExample) const;
	UFUNCTION()
	int32 GenerateMapWalls();
	UFUNCTION()
	void FillArraysWithStartingPlayers();
	UFUNCTION()
	void SpawnBaseUnitForPlayer(APlayerController *playerController, UObject* worldContextObject, FString nameOfActor, AActor *playerStart);
	UFUNCTION()
	void SpawnBaseBuildingForPlayer(APlayerController *playerController, UObject* worldContextObject, FString nameOfActor, AActor *playerStart);
	UFUNCTION()
	void SetBaseAreaOwnerToPlayer(APlayerState *playerState, AActor *BaseArea);
	UPROPERTY(EditAnywhere)
	TArray<FName> SideZonesSubLevelNames;
	UPROPERTY(EditAnywhere)
	TArray<FName> CentralZoneSubLevelNames;
private:
	UFUNCTION()
	void GenerateTile(UObject * worldContextObject, TArray<FName>& listOfLevels, FLatentActionInfo info, int32 index);

private:
	const float VERTEXVALUE = 100.f;
	const float TILESIZE = 100.f;
		int32 iterator_diablo = 0;
private:
	UPROPERTY()
	UUnitSpawner *US;
	UPROPERTY()
	ULevelStreaming *currentLevelStreaming;
	UPROPERTY()
	TArray<AActor *> player1StartingActors;
	UPROPERTY()
	TArray<AActor *> player2StartingActors;
	
private:
	UFUNCTION()
		void OnSynchronizerSpawnedByGameMode(AInGameGameMode* GameMode, AObjectNetworkSyncronizer* Synchronizer, AInGamePlayerController* OwningPlayer);
};
