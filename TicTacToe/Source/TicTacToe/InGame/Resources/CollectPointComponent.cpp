// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectPointComponent.h"

// Sets default values for this component's properties
UCollectPointComponent::UCollectPointComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	// ...
}

// Called when the game starts
void UCollectPointComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UCollectPointComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UCollectPointComponent::CanCollectResources(const TArray<EResourceType>& Resources)
{
	for (auto resource : Resources)
	{
		if (!CollectibleResources.Contains(resource))
		{
			return false;
		}
	}
	return true;
}