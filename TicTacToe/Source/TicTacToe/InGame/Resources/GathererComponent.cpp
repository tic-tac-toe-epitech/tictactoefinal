// Fill out your copyright notice in the Description page of Project Settings.

#include "GathererComponent.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Engine/EngineTypes.h"
#include "GameData/PlayerState/InGamePlayerState.h"

// Sets default values for this component's properties
UGathererComponent::UGathererComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	bIsTimerStarted = false;
}

bool UGathererComponent::BindToResourceNode(AResourcePoint *ResourcePoint)
{
	UWorld *World = GetWorld();
	if (ResourcePoint->TryAddCollector(GetOwner()) == true
		&& World != nullptr
		&& !ftimerhandle.IsValid())
	{
		CurrentResourcePointCollecting = ResourcePoint;
		ResourcePoint->OnPointEmpty.AddDynamic(this, &UGathererComponent::UnbindResourceNode);
		this->TypeOfResourceGathered = ResourcePoint->ResourcePointType;
		AInGamePlayerState *playerstate = Cast<AInGamePlayerState>(this->GetOwner()->GetOwner());
		this->ResourcesManagerComponent = playerstate->GetResourcesManager();
		World->GetTimerManager().SetTimer(ftimerhandle, this, &UGathererComponent::CollectResource, 3.0f, true, 0.f);

		return true;
	}
	else if (ftimerhandle.IsValid()
		&& World->GetTimerManager().IsTimerPaused(ftimerhandle))
	{
		//Le timer etait en pause le temps de faire l'aller retour
		World->GetTimerManager().UnPauseTimer(ftimerhandle);
		return true;
	}
	else
	{
		return false;
	}
}

void UGathererComponent::CollectResource()
{
	//Setter le type de resource port�
	this->TypeOfResourceCarried = this->TypeOfResourceGathered;

	//Trouver combien de resources je dois ramasser
	int32 maxResourceToGatherThisTick;
	if (ResourcesMax - CurrentResources == 0)
	{
		PreOnGathererFullBroadcast();
	}
	else if (ResourcesMax - CurrentResources < AmountGatheredPerTick)
	{
		maxResourceToGatherThisTick = ResourcesMax - CurrentResources;
	}
	else
	{
		maxResourceToGatherThisTick = AmountGatheredPerTick;
	}

	//Essayer de prendre ces resources du point
	auto resourcePointQuantity = CurrentResourcePointCollecting->GetResourcePointQuantity();
	if (resourcePointQuantity == 0)
	{
		PreOnGathererFullBroadcast();
	}
	else
	{
		CurrentResources += CurrentResourcePointCollecting->CollectFromResourcePoint(maxResourceToGatherThisTick);
	}

	//Si je suis full, je rentre, si ya plus de resources, je rentre
	if (CurrentResources == ResourcesMax ||
		CurrentResourcePointCollecting->GetResourcePointQuantity() == 0)
	{
		PreOnGathererFullBroadcast();
	}
}

void UGathererComponent::PreOnGathererFullBroadcast()
{
	GetWorld()->GetTimerManager().PauseTimer(ftimerhandle);
	OnGathererFull.Broadcast();
}

void UGathererComponent::UnbindResourceNode()
{
	this->CurrentResourcePointCollecting->OnPointEmpty.RemoveDynamic(this, &UGathererComponent::UnbindResourceNode);
	this->CurrentResourcePointCollecting->TryRemoveCollector(Cast<AUnit>(this->GetOwner()));
	ftimerhandle.Invalidate();
}

int32 UGathererComponent::GetCurrentResources() const
{
	return this->CurrentResources;
}

EResourceType UGathererComponent::GetTypeOfResourceCarried() const
{
	return this->TypeOfResourceCarried;
}

void UGathererComponent::ResetWorkerResources()
{
	this->TypeOfResourceCarried = EResourceType::Undefinied;
	this->TypeOfResourceGathered = EResourceType::Undefinied;
	this->DropCarriedResources();
}

void UGathererComponent::DropCarriedResources()
{
	this->CurrentResources = 0;
}

bool UGathererComponent::IsResourcePointEmpty() const
{
	if (this->CurrentResourcePointCollecting != nullptr)
	{
		return this->CurrentResourcePointCollecting->GetResourcePointQuantity() == 0;
	}

	return true;
}

// Called when the game starts
void UGathererComponent::BeginPlay()
{
	Super::BeginPlay();
	ResourcesMax = 10;
	CurrentResources = 0;
	TypeOfResourceCarried = EResourceType::Undefinied;
	AmountGatheredPerTick = 5;
	bFlagToReturnToPoint = false;
	// ...

}


// Called every frame
void UGathererComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

