#pragma once

#include "ResourceInclude.generated.h"

UENUM(BlueprintType)
enum class EResourceType : uint8
{
	Undefinied,
	Gold,
	Wood,
	Steel
};
