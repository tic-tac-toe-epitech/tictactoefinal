// Fill out your copyright notice in the Description page of Project Settings.


#include "ResourcePoint.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AResourcePoint::AResourcePoint()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	bReplicates = true;
	bAlwaysRelevant = true;
	bReplicateMovement = true;
	nbVillagerMax = 5;
	CurrentResourceQuantity = 15;
	ResourceMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ressource Mesh Component"));
	ResourceMesh->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));
	ResourceMesh->CastShadow = false;
}

// Called when the game starts or when spawned
void AResourcePoint::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AResourcePoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

EResourceType AResourcePoint::GetResourcePointType() const {
	return this->ResourcePointType;
}

bool AResourcePoint::TryAddCollector(AActor *newCollector)
{
	auto castedNewCollector = Cast<AUnit>(newCollector);
	if (castedNewCollector == nullptr)
	{
		return false;
	}
	if (arrayOfCollectors.Num() < nbVillagerMax)
	{
		arrayOfCollectors.Add(castedNewCollector);
		//TODO:  Start collecting
		return true;
	}
	else
	{
		return false;
	}
}

bool AResourcePoint::TryRemoveCollector(AUnit * collector)
{
	if (collector != nullptr)
	{
		//Remove more or zero element, something is wrong
		if (arrayOfCollectors.Remove(collector) != 1)
			return false;
	}
	else
	{
		return false;
	}
	return true;
}

int32 AResourcePoint::GetResourcePointQuantity() const
{
	return this->CurrentResourceQuantity;
}

int32 AResourcePoint::CollectFromResourcePoint(int32 quantityToCollect)
{
	if (CurrentResourceQuantity - quantityToCollect < 0)
	{
		CurrentResourceQuantity = 0;
		OnPointEmpty.Broadcast();
		return CurrentResourceQuantity;
	}
	else
	{
		return quantityToCollect;
	}
}