// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "GameFramework/Actor.h"
#include "InGame/Unit/Unit.h"
//#include "InGame/Resources/GathererComponent.h"
#include "InGame/ResourcesManager/ResourcesManagerComponent.h"
#include "ResourcePoint.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEvent_AResourcePoint);

class UStaticMeshComponent;
//enum EResourceType;

UCLASS()
class TICTACTOE_API AResourcePoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AResourcePoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent *ResourceMesh;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
		bool TryAddCollector(AActor *newCollector);
	UFUNCTION()
		bool TryRemoveCollector(AUnit *collector);
	UPROPERTY(EditAnywhere)
		EResourceType ResourcePointType;
	UFUNCTION(BlueprintCallable)
		EResourceType GetResourcePointType() const;

	UFUNCTION()
		int32 GetResourcePointQuantity() const;
	// If a different value is returned from the signature, than node is empty
	UFUNCTION()
		int32 CollectFromResourcePoint(int32 quantityToCollect);
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_AResourcePoint OnPointEmpty;
private:
	UPROPERTY()
	int32 nbVillagerMax; //TODO: Move to config 5

	UPROPERTY()
	int32 nbCurrentVillagers; // 0

	UPROPERTY()
	int32 CurrentResourceQuantity; //5000

	UPROPERTY()
	TArray<AUnit *> arrayOfCollectors;
};
