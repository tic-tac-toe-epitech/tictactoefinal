// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InGame/ResourcesManager/ResourcesManagerComponent.h"
#include "InGame/Resources/ResourcePoint.h"
#include "GathererComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEvent_UGathererComponent);

struct FTimerHandle;

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TICTACTOE_API UGathererComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGathererComponent();

	//A bound ResourceNode is a node where the villager will come and go.
	//This will be unbound once the node is empty or a new order comes
	UFUNCTION(BlueprintCallable)
		bool BindToResourceNode(AResourcePoint *ResourcePoint);
	UFUNCTION(BlueprintCallable)
		void UnbindResourceNode();
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_UGathererComponent OnGathererFull;
	UFUNCTION(BlueprintCallable)
		int32 GetCurrentResources() const;
	UFUNCTION(BlueprintCallable)
		EResourceType GetTypeOfResourceCarried() const;

	UFUNCTION(BlueprintCallable)
		void DropCarriedResources();
	UFUNCTION(BlueprintCallable)
		bool IsResourcePointEmpty() const;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY()
		int32 ResourcesMax;
	UPROPERTY()
		int32 AmountGatheredPerTick;
	UPROPERTY()
		int32 CurrentResources;
	UPROPERTY()
		EResourceType TypeOfResourceCarried;
	UPROPERTY()
		EResourceType TypeOfResourceGathered;
	UPROPERTY()
		AResourcePoint *CurrentResourcePointCollecting;
	UPROPERTY()
		UResourcesManagerComponent *ResourcesManagerComponent;
	UPROPERTY()
		bool bFlagToReturnToPoint;
	UFUNCTION()
		void PreOnGathererFullBroadcast();
public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UFUNCTION()
		void CollectResource();
	UFUNCTION()
		void ResetWorkerResources();
	UPROPERTY()
		FTimerHandle ftimerhandle;
	UPROPERTY()
		bool bIsTimerStarted;

};
