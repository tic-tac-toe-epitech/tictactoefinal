// Fill out your copyright notice in the Description page of Project Settings.


#include "Building.h"
#include "TicTacToe.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "InGame/Selectable/SelectionDecalComponent/SelectionDecalComponent.h"
#include "Libraries/DiplomacyLibrary.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "Engine/StaticMesh.h"
#include "GameData/GameState/InGameGameState.h"
#include "GenericPlatform/GenericPlatformMath.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/GameStateBase.h"
#include "Kismet/GameplayStatics.h"
#include "Containers/UnrealString.h"
#include "InGame/Action/ActionsManagerComponent.h"
#include "UnrealNetwork.h"

// STATICS

TArray<ABuilding*> ABuilding::GetBuildingsOfType(TArray<ABuilding*> BuildingsToFiler, FGameplayTag Type)
{
	TArray<ABuilding*> ret;

	if (!Type.MatchesTag(FGameplayTag::RequestGameplayTag("FactionEntity.Building")))
	{
		UE_LOG(TicTacLogPlayerCivilization,
			Warning,
			TEXT("ABuilding::GetBuildingsOfType : Given type does not match tag 'FactionEntity.Building'. Given type : '%s'."),
			*Type.ToString());
	}
	else
	{
		for (auto building : BuildingsToFiler)
		{
			if (building->GetBuildingType().MatchesTag(Type))
			{
				ret.AddUnique(building);
			}
		}
	}
	return ret;
}

// END STATICS


// Sets default values
ABuilding::ABuilding() : Super()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bAlwaysRelevant = true;

	BuildingMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Building Mesh"));
	RootComponent = BuildingMeshComponent;
	BuildingMeshComponent->CastShadow = false;

	SquareDecalComponent = CreateDefaultSubobject<USelectionDecalComponent>(TEXT("Selection Decal Component"));
	SquareDecalComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));

	ConstructionProgressBar = CreateDefaultSubobject<UWidgetComponent>(TEXT("ConstructionProgressBar"));
	ConstructionProgressBar->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));

	ActionsManagerComponent = CreateDefaultSubobject<UActionsManagerComponent>(TEXT("Actions Manager"));
}

bool ABuilding::ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	// Subobject replication sample. Replace ReplicatedSubobject by your subobject and repeat these steps for any other objects that need to be replicated.
	//if (ReplicatedSubobject != nullptr)
	//{
	//	WroteSomething |= Channel->ReplicateSubobject(ReplicatedSubobject, *Bunch, *RepFlags);
	//}

	return WroteSomething;
}

void ABuilding::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Property replication sample. Replace ReplicatedProperty by your property and repeat this line for any properties that need to be replicated.
	DOREPLIFETIME(ABuilding, BuildingData);
}

void ABuilding::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	AInGameGameState* gameState = Cast<AInGameGameState>(UGameplayStatics::GetGameState(this));
	if (gameState)
	{
		BuildingData = FBuildingsStruct::GetBuildingData(gameState->GetBuildingsData(), BuildingType.ToString());
	}
}

void ABuilding::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (BuildingMeshComponent->GetStaticMesh())
	{
		FVector meshSize = BuildingMeshComponent->GetStaticMesh()->GetBounds().GetBox().GetExtent();
		float sideSize = meshSize.Y < meshSize.X ? meshSize.Y : meshSize.X;
		SquareDecalComponent->SetDecalSize(sideSize, false);
	}
}

// Called when the game starts or when spawned
void ABuilding::BeginPlay()
{
	Super::BeginPlay();

	AInGamePlayerState* ownerAsInGamePlayerState = Cast<AInGamePlayerState>(GetOwner());
	if (ownerAsInGamePlayerState)
	{
		ownerAsInGamePlayerState->AddBuilding(this);
	}
	if (!HasAuthority())
	{
		SquareDecalComponent->SetIsSelected(Selected, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
	}

	switch (UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this))
	{
	case EDiplomacy::OWNED:
		this->UpdateBuildingMaterial(EConstructionStatus::ConstructionStoped);
		break;
	case EDiplomacy::ENEMY:
		this->ConstructionProgressBar->SetVisibility(false);
		this->UpdateBuildingMaterial(EConstructionStatus::Constructed);
		break;
	default: break;
	}

	if (this->_ConstructionStatus == EConstructionStatus::Constructed)
		this->ConstructionProgressBar->SetVisibility(false);
}

void ABuilding::OnConstructionStatusUpdate_Implementation() {
	UE_LOG(LogController, Log,
		TEXT("ABuilding::OnConstructionStatusUpdate_Implementation."));
}

EConstructionStatus ABuilding::GetConstructionStatus()
{
	return this->_ConstructionStatus;
}

void ABuilding::OnRep_Owner()
{
	Super::OnRep_Owner();
	if (SquareDecalComponent)
	{
		SquareDecalComponent->SetIsSelected(Selected, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
	}
	AInGamePlayerState* ownerAsInGamePlayerState = Cast<AInGamePlayerState>(GetOwner());
	if (ownerAsInGamePlayerState)
	{
		ownerAsInGamePlayerState->AddBuilding(this);
	}
}

// Called every frame
void ABuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

#pragma region Gameplay Tags interface

void ABuilding::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
	TagContainer.AddTag(BuildingType);
	TagContainer.AppendTags(GameplayTagContainer);
}

#pragma endregion Gameplay Tags interface

#pragma region Selectable interface

void ABuilding::Select_Implementation()
{
	Selected = true;

	SquareDecalComponent->SetIsSelected(true, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void ABuilding::Unselect_Implementation()
{
	Selected = false;

	SquareDecalComponent->SetIsSelected(false, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void ABuilding::Hover_Implementation()
{
	Hovered = true;

	SquareDecalComponent->SetIsHovered(true, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void ABuilding::Unhover_Implementation()
{
	Hovered = false;

	SquareDecalComponent->SetIsHovered(false, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

#pragma endregion Selectable interface

void ABuilding::OnConstructionTimerEnded() {
	this->_ConstructionStatus = EConstructionStatus::Constructed;
	this->UpdateConstructionStatusOnClient();
	this->EndConstruction();
	ActionsManagerComponent->init();
}

void ABuilding::SetConstructionTimer(float time) {
	this->GetWorldTimerManager().ClearTimer(this->_EndConstructionTimer);
	this->GetWorldTimerManager().SetTimer(this->_EndConstructionTimer, this, &ABuilding::OnConstructionTimerEnded, time, false);
}


void	ABuilding::UpdateBuildingMaterial(EConstructionStatus ConstructionStatus) {
	if (!BaseMaterials.Num())
	{
		BaseMaterials = BuildingMeshComponent->GetMaterials();
	}
	switch (ConstructionStatus)
	{
	case EConstructionStatus::Constructed:
	{
		for (auto i = 0; i < BaseMaterials.Num(); i++)
		{
			BuildingMeshComponent->SetMaterial(i, BaseMaterials[i]);
		}
		break;
	}
	case EConstructionStatus::OnConstruction:
	case EConstructionStatus::ConstructionStoped:
	{
		for (auto i = 0; i < BuildingMeshComponent->GetMaterials().Num(); i++)
		{
			BuildingMeshComponent->SetMaterial(i, OnConstructionMaterial);
		}
		break;
	}
	}
}

float ABuilding::GetConstructionTime() {
	// Protect division by zero
	if (this->_NBConstructionWorkers <= 0) {
		return this->_TotalConstructionTime;
	}
	// Divide totalConstructionTime by 2 on each worker
	return FMath::Clamp<float>((this->_TotalConstructionTime / FGenericPlatformMath::Pow(2, this->_NBConstructionWorkers - 1)), 3.0f, this->_TotalConstructionTime);
}

float ABuilding::GetRemainingConstructionTime() {
	float elapsedTime = 0;

	// Get already elapsed time
	if (this->_ConstructionStartTime != -1 && this->_ConstructionEndTime != -1) {
		elapsedTime = this->_ConstructionEndTime - this->_ConstructionStartTime;
	}
	// Remove elapsed time
	return FMath::Clamp<float>(this->_ConstructionTime - elapsedTime, 3.0f, this->_TotalConstructionTime);
}

void ABuilding::InitConstruction(float totalConstructionTime, int nbWorker) {
	this->_NBConstructionWorkers = nbWorker;
	this->_TotalConstructionTime = totalConstructionTime;
	this->_ConstructionTime = totalConstructionTime;
	this->_RemainingConstructionTime = totalConstructionTime;
	this->_ConstructionStatus = EConstructionStatus::ConstructionStoped;
	this->UpdateConstructionStatusOnClient();
}

void ABuilding::StartConstruction() {
	if (this->_ConstructionStatus == EConstructionStatus::OnConstruction)
	{
		return;
	}
	this->_ConstructionTime = GetConstructionTime();
	this->_RemainingConstructionTime = GetRemainingConstructionTime();

	if (this->_NBConstructionWorkers <= 0) {
		this->_ConstructionStatus = EConstructionStatus::ConstructionStoped;
		this->_ConstructionEndTime = -1;
	}
	else if (this->_RemainingConstructionTime > 0) {
		this->_ConstructionStartTime = this->GetWorld()->GetGameState()->GetServerWorldTimeSeconds();
		this->_ConstructionEndTime = this->_ConstructionStartTime + this->_RemainingConstructionTime;
		UE_LOG(TicTacLogOrderManagement, Log, TEXT("ABuilding(%s)::UpdateConstructionStatus Remaining construction time=%f"),
			*GetNameSafe(this),
			this->_RemainingConstructionTime);
		this->_ConstructionStatus = EConstructionStatus::OnConstruction;
		this->SetConstructionTimer(this->_RemainingConstructionTime);
	}
	else
	{
		this->_ConstructionStatus = EConstructionStatus::Constructed;
	}
	this->UpdateConstructionStatusOnClient();
}

void ABuilding::EndConstruction()
{
	this->OnStopConstruction.Broadcast();
	// Works without it
	//for (size_t i = 0; i < arrayOfBuilders.Num(); i++)
	//{
	//	auto fun = arrayOfBuilders[i]->GetComponentByClass(BuilderComponentVar);
	//	auto fun2 = Cast<UBuilderComponent>(fun);
	//	fun2->UnbindBuilding();
	//}
}

void ABuilding::StopConstruction() {
	// Clear timer
	this->GetWorldTimerManager().ClearTimer(this->_EndConstructionTimer);
	// Save elapsed time
	if (this->_ConstructionStartTime != -1 && this->_ConstructionEndTime != -1) {
		this->_ConstructionEndTime = this->GetWorld()->GetGameState()->GetServerWorldTimeSeconds();
	}
	this->_ConstructionTime = GetConstructionTime();
	this->_RemainingConstructionTime = GetRemainingConstructionTime();
	this->_ConstructionStatus = EConstructionStatus::ConstructionStoped;
	this->UpdateConstructionStatusOnClient();
}

void ABuilding::UpdateConstructionTime(float totalConstructionTime) {
	this->_TotalConstructionTime = totalConstructionTime;
	this->_ConstructionTime = GetConstructionTime();
	this->_RemainingConstructionTime = GetRemainingConstructionTime();
	this->_ConstructionStartTime = this->GetWorld()->GetGameState()->GetServerWorldTimeSeconds();
	this->_ConstructionEndTime = this->_ConstructionStartTime + this->_RemainingConstructionTime;

	if (this->_ConstructionTime > 0) {
		this->_ConstructionStatus = EConstructionStatus::OnConstruction;
		this->SetConstructionTimer(this->_RemainingConstructionTime);
	}
	else
	{
		this->_ConstructionStatus = EConstructionStatus::Constructed;
	}
	this->UpdateConstructionStatusOnClient();
}

void ABuilding::AddConstructionWorker(int nbWorker) {
	this->_NBConstructionWorkers += nbWorker;
	this->UpdateConstructionTime(this->_TotalConstructionTime);
}

void ABuilding::DelConstructionWorker(int nbWorker) {
	this->_NBConstructionWorkers -= nbWorker;

	if (this->_NBConstructionWorkers <= 0) {
		this->_NBConstructionWorkers = 0;
		this->StopConstruction();
	}
	else {
		this->UpdateConstructionTime(this->_TotalConstructionTime);
	}
}

void ABuilding::UpdateConstructionStatus_Implementation(EConstructionStatus ConstructionStatus, float ConstructionEndTime, float RemainingTime, float TotalTime, int NBWorker) {
	this->_NBConstructionWorkers = NBWorker;
	this->_ConstructionStatus = ConstructionStatus;
	this->_ConstructionEndTime = ConstructionEndTime;
	this->_ConstructionTime = TotalTime;
	this->_RemainingConstructionTime = RemainingTime;

	this->LogConstructionStatusUpdate(*FString::Printf(TEXT("Client %s"), *UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetName()));

	if (UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this) == EDiplomacy::OWNED) {
		this->UpdateBuildingMaterial(this->_ConstructionStatus);
	}
	switch (ConstructionStatus)
	{
	case EConstructionStatus::ConstructionStoped:
	case EConstructionStatus::OnConstruction:
		this->ConstructionProgressBar->SetVisibility(true);
		break;
	case EConstructionStatus::Constructed:
		this->ConstructionProgressBar->SetVisibility(false);
		break;
	default:
		break;
	}
	this->OnConstructionStatusUpdate();
}

void ABuilding::LogConstructionStatusUpdate(FString Platform) {
	FString	status;

	switch (this->_ConstructionStatus)
	{
	case EConstructionStatus::OnConstruction:
		status = "On Construction";
		break;
	case EConstructionStatus::ConstructionStoped:
		status = "On Stopped";
		break;
	case EConstructionStatus::Constructed:
		status = "Constructed";
		break;
	default:
		break;
	}
	UE_LOG(TicTacLogOrderManagement, Log, TEXT("ABuilding(%s)::UpdateConstructionStatus Status=%s StartTime=%f EndTime=%f RemainingTime=%f TotalTime=%f (%s)"),
		*GetNameSafe(this),
		*status,
		this->_ConstructionStartTime,
		this->_ConstructionEndTime,
		this->_RemainingConstructionTime,
		this->_ConstructionTime,
		*Platform);
}

void ABuilding::UpdateConstructionStatusOnClient() {
	this->LogConstructionStatusUpdate(TEXT("Server"));
	this->UpdateConstructionStatus(this->_ConstructionStatus, this->_ConstructionEndTime, this->_RemainingConstructionTime, this->_ConstructionTime, this->_NBConstructionWorkers);
	this->OnConstructionStatusUpdate();
}

bool ABuilding::TryAddBuilder(AActor *builder)
{
	auto castedNewBuilder = Cast<AUnit>(builder);
	if (builder == nullptr)
	{
		return false;
	}
	if (arrayOfBuilders.Num())
	{
		arrayOfBuilders.Add(castedNewBuilder);
		//TODO:  Start collecting
		return true;
	}
	else
	{
		return false;
	}
}

bool ABuilding::TryRemoveBuilder(AActor * builder)
{
	auto castedNewBuilder = Cast<AUnit>(builder);
	if (castedNewBuilder == nullptr)
	{
		return false;
	}
	this->arrayOfBuilders.Remove(castedNewBuilder);
	this->DelConstructionWorker(1);
	return true;
}
