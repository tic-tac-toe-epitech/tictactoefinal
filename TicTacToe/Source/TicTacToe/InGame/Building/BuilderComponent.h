// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuilderComponent.generated.h"


UENUM(BlueprintType, Blueprintable)
enum class EBuilderState : uint8
{
	None,
	PlacingGhost,
	MovingToBuilding,
	Building,
};



UCLASS(meta = (BlueprintSpawnableComponent))
class TICTACTOE_API UBuilderComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UBuilderComponent();
	UFUNCTION()
		EBuilderState const & GetBuilderState() const;
	UFUNCTION(BlueprintCallable)
		void SetBuilderState(EBuilderState const & state);
	UFUNCTION(BlueprintCallable)
		bool BindBuilderToBuilding(ABuilding * building);
	UFUNCTION(BlueprintCallable)
		void UnbindBuilding();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY()
		EBuilderState builderState;
	UPROPERTY()
		ABuilding *building;
	UPROPERTY()
		ABuilding *currentConstructedBuilding;
};
