// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildableArea.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"

// Sets default values
ABuildableArea::ABuildableArea() : Super()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bAlwaysRelevant = true;
	bOnlyRelevantToOwner = false;


	AreaMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Area Building Mesh"));
	RootComponent = AreaMeshComponent;

}

// Called when the game starts or when spawned
void ABuildableArea::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABuildableArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


#pragma region Gameplay Tags interface

void ABuildableArea::GetOwnedGameplayTags(FGameplayTagContainer & TagContainer) const
{
	TagContainer.AddTag(BuildableAreaPositionTag);
	TagContainer.AppendTags(GameplayTagContainer);
}

#pragma endregion Gameplay Tags interface