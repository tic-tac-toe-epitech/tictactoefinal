// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InGame/Unit/Unit.h"
#include "InGame/Selectable/SelectableInterface.h"
#include "GameplayTagsModule.h"
#include "GameplayTags.h"
#include "InGame/Building/BuilderComponent.h"
#include "GameData/Configuration/Building/BuildingConfig.h"
#include "Misc/DateTime.h"
#include "Components/WidgetComponent.h"
#include "Building.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEvent_ABuilding);

class UStaticMeshComponent;
class USelectionDecalComponent;
class UBoxComponent;
class UActionsManagerComponent;
class UMaterialInterface;


UENUM(BlueprintType, Blueprintable)
enum class EConstructionStatus : uint8
{
	OnConstruction,
	ConstructionStoped,
	Constructed,
};


UCLASS()
class TICTACTOE_API ABuilding : public AActor, public ISelectableInterface, public IGameplayTagAssetInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuilding();

	virtual bool ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

	UFUNCTION(BlueprintPure)
		static TArray<ABuilding*> GetBuildingsOfType(TArray<ABuilding*> BuildingsToFilter, FGameplayTag Type);

	// Update client construction status
	UFUNCTION(Client, Reliable, BlueprintCallable)
	void UpdateConstructionStatus(EConstructionStatus ConstructionStatus, float ConstructionEndTime, float RemainingTime, float TotalTime, int NBWorker);
	void UpdateConstructionStatus_Implementation(EConstructionStatus ConstructionStatus, float ConstructionEndTime, float RemainingTime, float TotalTime, int NBWorker);

	// Called when the constrction status has been updated on client and server.
	UFUNCTION(BlueprintNativeEvent)
	void OnConstructionStatusUpdate();
	void OnConstructionStatusUpdate_Implementation();

	UFUNCTION()
	EConstructionStatus GetConstructionStatus();

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_ABuilding OnStopConstruction;

	UPROPERTY()
		TSubclassOf<UBuilderComponent> BuilderComponentVar;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnRep_Owner() override;

	// Total time used to construct the building without worker
	UPROPERTY(BlueprintReadOnly)
		float		_TotalConstructionTime = 0;
	// Time used to construct the building with workers
	UPROPERTY(BlueprintReadOnly)
		float		_ConstructionTime = 0;
	// Remaining to complete the construction
	UPROPERTY(BlueprintReadOnly)
		float		_RemainingConstructionTime = 0;
	// Number of worker currently used
	UPROPERTY(BlueprintReadOnly)
		int		_NBConstructionWorkers = 0;
	// End construction time based on GetServerWorldTimeSeconds
	UPROPERTY(BlueprintReadOnly)
		float	_ConstructionEndTime = -1;
	// Start construction time based on GetServerWorldTimeSeconds
	UPROPERTY(BlueprintReadOnly)
		float	_ConstructionStartTime = -1;
	// Construction status
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		EConstructionStatus	_ConstructionStatus = EConstructionStatus::ConstructionStoped;
	// End construction timer
	UPROPERTY(BlueprintReadOnly)
		FTimerHandle	_EndConstructionTimer;

public:	
	virtual void PreInitializeComponents() override;
	virtual void PostInitializeComponents() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// *** Server Construction ***
	// Init construction parameters
	UFUNCTION(BlueprintCallable)
		void InitConstruction(float totalConstructionTime, int nbWorker);
	// Start construction
	UFUNCTION(BlueprintCallable)
		void StartConstruction();
	UFUNCTION()
	void EndConstruction();
	// Stop construction
	UFUNCTION(BlueprintCallable)
		void StopConstruction();
	// Update the total time required to build.
	UFUNCTION(BlueprintCallable)
		void UpdateConstructionTime(float totalConstructionTime);
	// Add construction worker
	UFUNCTION(BlueprintCallable)
		void AddConstructionWorker(int nbWorker = 1);
	// Remove constrction worker
	UFUNCTION(BlueprintCallable)
		void DelConstructionWorker(int nbWorker = 1);
	UFUNCTION()
		bool TryAddBuilder(AActor * builder);
	UFUNCTION()
		bool TryRemoveBuilder(AActor * builder);

private:
	void LogConstructionStatusUpdate(FString Platform);

	// *** Server Construction ***
	// Callback used by timer on time elapsed
	void OnConstructionTimerEnded();
	// Set the construction timer with the new time
	void SetConstructionTimer(float time);
	// Get constrction time with n worker
	float GetConstructionTime();
	// Get remaining constrction time
	float GetRemainingConstructionTime();
	//
	void UpdateConstructionStatusOnClient();



	// *** Client Construction ***
	// Update building material
	void	UpdateBuildingMaterial(EConstructionStatus ConstructionStatus);

	UPROPERTY()
		TArray<UMaterialInterface*> BaseMaterials;

#pragma region Config

private:
	UPROPERTY(Replicated)
	FBuildingStruct BuildingData;

public:
	UFUNCTION(BlueprintPure)
		const FBuildingStruct& GetBuildingData() { return BuildingData; }

#pragma endregion Config

#pragma region Components

protected:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BuildingMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UActionsManagerComponent* ActionsManagerComponent;

	UPROPERTY(EditAnywhere)
		USelectionDecalComponent* SquareDecalComponent;

	UPROPERTY(EditAnywhere)
		UMaterial* ConstructedMaterial;

	UPROPERTY(EditAnywhere)
		UMaterial* OnConstructionMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWidgetComponent*	ConstructionProgressBar;

#pragma endregion Components

#pragma region Selectable Interface

private:
	bool Selected;
	bool Hovered;

public:
	virtual void Select_Implementation() override;

	virtual void Unselect_Implementation() override;

	virtual void Hover_Implementation() override;

	virtual void Unhover_Implementation() override;

#pragma endregion Selectable Interface

#pragma region GameplayTag Asset Interface

protected:
	UPROPERTY(EditAnywhere)
		FGameplayTag BuildingType;

	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;

	UPROPERTY(EditAnywhere)
		FGameplayTagContainer GameplayTagContainer;

public:
	UFUNCTION(BlueprintPure)
		FGameplayTag GetBuildingType() { return BuildingType; }

	UPROPERTY(EditAnywhere)
		TArray<AUnit *> arrayOfBuilders;

#pragma endregion GameplayTag Asset Interface
};
