// Fill out your copyright notice in the Description page of Project Settings.


#include "RallyPoint.h"

// Sets default values
ARallyPoint::ARallyPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	SetReplicates(true);
}

// Called when the game starts or when spawned
void ARallyPoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARallyPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector ARallyPoint::GetRallyLocation()
{
	return GetActorLocation();
}

