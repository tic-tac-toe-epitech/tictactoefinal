// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameplayTagsModule.h"
#include "GameplayTags.h"
#include "Containers/Map.h"
#include "Templates/SharedPointer.h"
#include "Engine/StaticMesh.h"
#include "GhostBuilding.generated.h"

class UStaticMeshComponent;
class UBoxComponent;

UCLASS()
class TICTACTOE_API AGhostBuilding : public AActor, public IGameplayTagAssetInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGhostBuilding();

public:	
	virtual void PostInitializeComponents() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SetBuildable(bool isBuildable);

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	int		Collisioncounter = 0;
	bool    CurrentMaterialIsBuildable = 0;

	void SetBuildableMaterial(bool isBuildable);

#pragma region Components

protected:
	UPROPERTY(BlueprintReadOnly)
	bool	IsBuildable = 0;

	UPROPERTY(BlueprintReadOnly)
	bool	IsOverlapping = 0;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BuildingMeshComponent;

	UPROPERTY(EditAnywhere)
	UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere)
	UMaterial* GhostValidMaterial;

	UPROPERTY(EditAnywhere)
	UMaterial* GhostInvalidMaterial;


#pragma endregion Components

#pragma region GameplayTag Asset Interface

protected:
	UPROPERTY(EditAnywhere)
	FGameplayTag BuildingType;

	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;

	UPROPERTY(EditAnywhere)
	FGameplayTagContainer GameplayTagContainer;

public:
	UFUNCTION(BlueprintPure)
	FGameplayTag GetBuildingType() { return BuildingType; }

#pragma endregion GameplayTag Asset Interface
};
