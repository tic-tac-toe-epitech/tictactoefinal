// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameplayTagsModule.h"
#include "GameplayTags.h"

#include "BuildableArea.generated.h"


UENUM(BlueprintType, Blueprintable)
enum class EBuildableAreaType : uint8
{
	Square,
	Base,
	BaseCorner,
	Outpost,
};

UCLASS()
class TICTACTOE_API ABuildableArea : public AActor, public IGameplayTagAssetInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildableArea();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Construction status
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EBuildableAreaType	_BuildableAreaType;

#pragma region Components

protected:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* AreaMeshComponent;

#pragma endregion Components

#pragma region GameplayTag Asset Interface

protected:
	UPROPERTY(EditAnywhere)
	FGameplayTag BuildableAreaPositionTag;

	virtual void GetOwnedGameplayTags(FGameplayTagContainer & TagContainer) const override;

	UPROPERTY(EditAnywhere)
	FGameplayTagContainer GameplayTagContainer;

public:
	UFUNCTION(BlueprintPure)
	FGameplayTag GetBuildableAreaPositionTag() { return BuildableAreaPositionTag; }

#pragma endregion GameplayTag Asset Interface

};
