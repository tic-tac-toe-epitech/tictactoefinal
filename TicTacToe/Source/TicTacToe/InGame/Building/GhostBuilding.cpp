// Fill out your copyright notice in the Description page of Project Settings.


#include "GhostBuilding.h"
#include "TicTacToe.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "InGame/Selectable/SelectionDecalComponent/SelectionDecalComponent.h"
#include "Libraries/DiplomacyLibrary.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "Engine/StaticMesh.h"

// Sets default values
AGhostBuilding::AGhostBuilding() : Super()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	BuildingMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ghost Building Mesh"));
	BuildingMeshComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));
	RootComponent = BuildingMeshComponent;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	BoxComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));


	GhostValidMaterial = CreateDefaultSubobject<UMaterial>(TEXT("GhostValidMaterial"));
	GhostInvalidMaterial = CreateDefaultSubobject<UMaterial>(TEXT("GhostInvalidMaterial"));
}

void AGhostBuilding::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		IsOverlapping = true;
		Collisioncounter++;
	}
}

void AGhostBuilding::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		Collisioncounter--;
		if (Collisioncounter == 0) {
			IsOverlapping = false;
		}
	}
}

void AGhostBuilding::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AGhostBuilding::OnOverlapBegin);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &AGhostBuilding::OnOverlapEnd);
	SetBuildableMaterial(CurrentMaterialIsBuildable);
}

// Called every frame
void AGhostBuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsOverlapping && CurrentMaterialIsBuildable) {
		this->SetBuildableMaterial(false);
	}
	else if (!IsOverlapping && IsBuildable != CurrentMaterialIsBuildable) {
		this->SetBuildableMaterial(IsBuildable);
	}
}

#pragma region Gameplay Tags interface

void AGhostBuilding::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
	TagContainer.AddTag(BuildingType);
	TagContainer.AppendTags(GameplayTagContainer);
}

#pragma endregion Gameplay Tags interface

void AGhostBuilding::SetBuildable(bool isBuildable) {
	this->IsBuildable = isBuildable;
}

void AGhostBuilding::SetBuildableMaterial(bool isBuildable) {
	this->CurrentMaterialIsBuildable = isBuildable;
	if (isBuildable) {
		for (auto i = 0; i < BuildingMeshComponent->GetMaterials().Num(); i++)
		{
			BuildingMeshComponent->SetMaterial(i, GhostValidMaterial);
		}
	}
	else {
		for (auto i = 0; i < BuildingMeshComponent->GetMaterials().Num(); i++)
		{
			BuildingMeshComponent->SetMaterial(i, GhostInvalidMaterial);
		}
	}
}
