// Fill out your copyright notice in the Description page of Project Settings.


#include "BuilderComponent.h"
#include "InGame/Building/Building.h"

// Sets default values for this component's properties
UBuilderComponent::UBuilderComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	builderState = EBuilderState::None;
	currentConstructedBuilding = nullptr;
}


// Called when the game starts
void UBuilderComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UBuilderComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

EBuilderState const & UBuilderComponent::GetBuilderState() const
{
	return this->builderState;
}

void UBuilderComponent::SetBuilderState(EBuilderState const & state)
{
	this->builderState = state;
}

bool UBuilderComponent::BindBuilderToBuilding(ABuilding *building)
{
	UWorld *World = GetWorld();
	if (building->TryAddBuilder(GetOwner()) == true
		&& World != nullptr)
	{
		this->builderState = EBuilderState::Building;
		building->OnStopConstruction.AddDynamic(this, &UBuilderComponent::UnbindBuilding);
		currentConstructedBuilding = building;
		//this->TypeOfResourceGathered = ResourcePoint->ResourcePointType;
		//AInGamePlayerState *playerstate = Cast<AInGamePlayerState>(this->GetOwner()->GetOwner());

		return true;
	}
	else
	{
		return false;
	}
}

void UBuilderComponent::UnbindBuilding()
{
	//this->CurrentResourcePointCollecting->OnPointEmpty.RemoveDynamic(this, &UGathererComponent::UnbindResourceNode);
	if (currentConstructedBuilding != nullptr)
	{
		this->currentConstructedBuilding->TryRemoveBuilder(Cast<AUnit>(this->GetOwner()));
		currentConstructedBuilding = nullptr;
	}
	this->builderState = EBuilderState::None;
}