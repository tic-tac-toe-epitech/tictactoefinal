#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "UObject/SoftObjectPtr.h"
#include "GhostBuilding.h"
#include "Building.h"
#include "BuildingTableRow.generated.h"

/*
** DataTable Row used to map Tag to buildings classes.
*/
USTRUCT(BlueprintType)
struct FBuildingTableRow: public FTableRowBase
{
	GENERATED_BODY()

		/*
		** Ghost Building Class
		*/
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AGhostBuilding> GhostBuildingClass;
		//class AGhostBuilding*		GhostBuildingClass;

		/*
		** Building Class
		*/
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ABuilding> BuildingClass;

	//class ABuilding*			BuildingClass;
};